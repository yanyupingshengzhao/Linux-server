# 第7章 Linux服务器程序规范

除了网络通信外, 服务器程序通常还必须考虑许多其他的细节问题.这些问题涉及广且零碎, 而且基本上是模板式的, 所以我们称之为服务器程序规范.

1. Linux服务器程序以后台程序呈现, 即守护进程(daemon), 无控制终端, 也不会意外接收用户输入, 其父进程通常是init进程.
2. Linux服务器程序通常有一套日志系统, 有的高级服务器还能输出日志到专门的UDP服务器.大部分后台程序都在/var/log目录下拥有自己的日志目录.
3. Linux服务器程序以root身份启动, 非root身份运行.
4. Linux服务器程序通常可配置, 可使用命令行或者配置文件, 配置文件通常放在/etc目录下.
5. Linux服务器程序会在启动的时候生成一个PID文件, 并存入/var/run目录中, 以记录该后台进程的PID.
6. Linux服务器程序通常需要考虑系统资源和限制, 以预测自身能承受多大负荷, 比如进程可用文件描述符总和和内存总量.

## 7.1日志

### 7.1.1 Linux日志系统

Linux提供一个守护进程来处理系统日志`syslogd`, 目前升级为`rsyslogd`.

`rsyslogd`守护进程既能接收用户进程输出日志, 又能接收内核日志. 用户进程通过调用`syslog`函数生成系统日志. 内核日志通过调用`printk`系统调用打印至内核的环状缓存(ring buffer)中, 环状缓存的内容直接映射到`/proc/kmsg`文件中.

用户日志收集原理为`syslog`函数将日志输出到UNIX本地域socket类型(AF_UNIX)的文件`/dev/log`中, `rsyslogd`守护进程则监听该文件以获取用户进程的输出.

内核日志收集原理为printk函数将日志缓存在环状缓存上, 集中写于文件`/proc/kmsg`中. rsyslogd守护进程则监听该文件以获取用户进程的输出.

默认情况下, 调试信息保存至`/var/log/dbug`文件, 普通信息保存于`/var/log/message`文件;

内核消息保存于`/var/log/kern.log`文件中.

日志进程主配置文件保存于`/etc/rsyslog/conf`.

- 内核日志输入路径
- 是否接收udp日志及其监听端口
- 是否接收tcp日志及其监听端口
- 日志文件的权限
- 包含哪些子配置文件(例如/etc/rsyslog.d/*.conf), 指定各类日志的目标存储文件

![image-20230203180010588](./charter07.assets/Linux日志系统.png)

### 7.1.2 syslog函数

应用程序使用syslog函数与rsyslogd守护进程通信.其定义如下:

```c
#include <syslog.h>
#define LOG_EMERG	0	//系统不可用
#define LOG_ALERT	1	//报警, 需要立即采取行动
#define LOG_CRIT	2	//非常严重的情况
#define LOG_ERROR	3	//错误
#define LOG_WARNING	4	//警告
#define LOG_NOTICE	5	//通知
#define LOG_INFO	6	//信息
#define LOG_DEBUG	7	//调试

void syslog(int priority, const char* message, ...);
/*
描述
	函数采用可变参数来结构化输出, 设施值的默认值是LOG_USER.
参数
	priority 参数是所谓的设施值和日志级别的按位或
	message, 可变参数, 可结构化输出
*/


void openlog(const char* ident, int logopt, int facility);
/*
描述
	进一步结构化日志内容, 该函数可以改变syslog的默认输出方式
参数
	ident指定字符串将被添加到日志消息的日期和时间之后, 它通常被设置为程序的名字
	logopt 对后续syslog的行为进行配置, 它可取下列值的按位或
        #define LOG_PID		0X01	//在日志消息中包含程序PID
        #define LOG_CONS	0X02	//如果消息不能记录到日志文件, 则打印至终端
        #define LOG_ODELAY	0X04	//延迟使用日志功能直到第一次调用syslog
        #define LOG_NDELAY	0X08	//不延迟打开日志功能
	
	facility 可用来修改syslog函数中默认设施值
*/


int setlogmask(int maskpri);
/*
描述
	此外, 日志过滤也很重要, 程序开发阶段可能需要输出很多调试信息, 而发布后我们又需要将这些调试信息关闭. 解决这个问题的方法并不是在程序发布后删除调试信息, 而是简单的设置日志掩码, 使日志级别大于日志掩码的日志信息被系统忽略
	使用setlogmask忽略日志内容
参数
	maskpri 指定日志掩码值, 该函数始终会成功, 他返回调用进程先前的日志掩码值 
*/


void closelog();
/*
描述
	最后, 使用日志输出后, 需要关闭日志功能

*/
```

## 7.2 用户信息

### 7.2.1 UID EUID GID EGID

大部分服务器程序, 需以root身份启动, 但不能以root身份运行.

下列函数可获取和设置进程的真实用户ID(UID), 有效用户ID(EUDID), 真实组ID(GID), 和有效组ID(EGID)

```c
#include <sys/types.h>
uid_t getuid();		// 获取用户真实用户ID
uid_t geteuid();	// 获取用户有效用户ID
gid_t getgid();		// 获取用户组ID
gid_t getegid();	// 后去用户有效组ID

int setuid(uid_t uid);
int seteuid(uid_t euid);
int setgid(gid_t gid);
int setegid(gid_t egid);

/*
一个进程拥有两个用户ID: UID EUID
EUID:方便资源访问, 使运行程序的用户拥有该程序有效用户的权限.
su程序为例, 任何用户都可以使用它来修改自己的账户信息, 但修改账户时的su程序不得不访问/etc/passwd文件, 而访问该文件是需要root权限.那么以普通用户信息启动的su如何访问/etc/passwd文件的呢?
窍门就在EUID, 用ls命令可以查看到, su程序的所有者是root, 并且它被设置了set-user-id标志, 这个标志表明, 任何普通用户运行su程序时, 其有效用户就是该程序的所有者root, 那么根据有效用户的含义, 任何运行su程序的普通用户都能够访问/etc/passwd文件.
有效用户为root的程序称为特权进程(privileged progress). EGID和EUID类似, 给运行目标程序的组用户提供有效组特权.
-rwsr-xr-x 1 root root 55672 Feb 21  2022 su
su的EUID为root, rws即所有者的权限
-rw-r--r-- 1 root root 1514 Dec  7 16:55 /etc/passwd


修改euid步骤
	1. sudo chmod root:root test_uid 	//修改目标文件的所有者为root
	2. sudo chmod +s test_uid			//设置目标文件的set-user-id标志
*/
```

### 7.2.2切换用户

```c
//演示以root身份启动的进程切换为以普通用户身份运行进程.
//启动身份和运行身份是两个概念

static bool switch_to_user( uid_t user_id, gid_t gp_id )
{
	//保证目标用户不是root
    if ( ( user_id == 0 ) && ( gp_id == 0 ) )
    {
        return false;
    }

    gid_t gid = getgid();
    uid_t uid = getuid();
    //确保当前用户是合法用户: root 或者 目标用户
    if ( ( ( gid != 0 ) || ( uid != 0 ) ) && ( ( gid != gp_id ) || ( uid != user_id ) ) )
    {
        return false;
    }
	//不是root程序, 则已经是目标用户了
    if ( uid != 0 )
    {
        return true;
    }
	// 切换目标用户
    if ( ( setgid( gp_id ) < 0 ) || ( setuid( user_id ) < 0 ) )
    {
        return false;
    }

    return true;
}
```

## 7.3进程间关系

### 7.3.1进程组

每个进程(PID)都会隶属于一个进程组(PGID), 进程组是集合, 其基本元素是子进程和自己.

```c
#include <unistd.h>
pid_t getpgid(pid_t pid);
/*
描述
	每个进程隶属于一个进程组, 进程组有首领进程, 首领进程的进程组ID是自己的进程PID
参数
	pid 进程id
返回值
	成功返回pid, 失败返回-1+errno

*/

int setpgid(pid_t pid, pit_t pgid);
/*
描述
	将PID为pid的进程的PGID设为pgid.
参数
	pid 当前需设置进程pid
	pgid 设置子进程或自己的进程组id为pgid
        特殊情况
            若pid == pigd, 则pid指定的进程将被被设为进程组首领
            若pid ==0, 则表明设置当前进程的PGID为pgid
            若pgid == 0, 则使用pid作为目标PGID
此外
	一个进程只能设置自己的或者子进程的pgid

*/
```

### 7.3.2 会话

一些关联的进程组将形成一个会话session, 会话是一个集合, 其基本元素是进程组.

"会话只能有一个控制终端, 即控制IO操作." 看起来不太对

```c
#include <unistd.h>
pid_t setsid(void);
pid_t getsid(pid_t pid)
/*
描述
	该函数不能由进程首领进程调用,否则将产生一个错误
	对于非首领进程, 调用该函数会有如下效果
	1. 创建新会话
	2. 调用进程称为会话的首领, 此时该进程是新会话的唯一成员
	3. 新建一个进程组, 其PGID就是调用进程的PID, 调用进程成为该进程组的进程首领
	4. 调用进程将甩开终端(如果有)
	
	Linux进程并未提供所谓的会话ID(SID)的概念, 但是Linux系统认为`会话ID`等于会话首领所在的进程组的PGID, 即新的会话的首领sid = pgid, 而pgid = pid, 最终三者相等.
	但是Linux提供了获取会话ID的方式
	
参数
	void
返回值
	成功返回新的进程组的PGID
	失败-1+errno
*/

pid_t getsid(pid_t pid);
```

### 7.3.3 用ps命令查看进程关系

```
ps -o pid,ppid,pgid,sid,comm | less
```

## 7.4系统资源限制

Linux上运行的程序都会受到资源限制的影响, 比如物理设备限制(CPU数量, 内存数量), 系统策略(CPU时间等), 以及具体的限制(比如文件名最大长度).

Linux系统资源限制可以通过如下函数读取和设置.

```c
#include <sys/resource.h>
int getrlimit(int resource, struct rlimit* rlim);
int setrlimit(int resource, const struct rlimit* rlim);

struct rlimit
{
	rlim_t rlim_cur;
	rlim_t rlim_max;
};
/*
rlim_t 一个整数类型, 用来描述资源级别
rlim_cur 该成员指定资源的软限制, 即一个建议性的, 最好不要超越的限制, 如果超越的话, 系统可能会向进程发送信号终止其运行. 
rlim_max 该成员指定资源的硬限制, 硬限制一般是软限制的上限.
两种修改方式
	1. 通过ulimit命令修改当前shell环境启动的资源限制, 这种限制只能对通过该shell的启动的后续程序有效.
	2. 通过修改配置文件来改变系统的软限制和硬限制.
*/
```

![image-20230226214445804](./charter07.assets/资源限制.png)

## 7.5 改变工作目录和根目录

有些服务器还需要改变工作目录和根目录, 工作目录类似系统的`home`目录, 而根目录是`/`, 例如web服务器一般在/var/www目录, web服务器的根目录并非系统的根目录, 而是站点的根目录.

```c
#include <unistd.h>
char* getcwd(char* buf, size_t size);
/*
描述
	获取当前工作目录路径.
参数
	buf 指向内存用于存储进程当前工作目录的绝对路径名, 其大小由size参数指定
	
返回值
	若buf!=NULL, 且绝对路径char*数组(+'\0')长度大于size, 则getcwd返回NULL, +errno(ERANGE)
    若buf==NULL, getcwd在内部使用malloc创建内存, 需手动释放.
*/
int chdir(const char* path);
/*
描述
	切换工作目录
参数
	path 切换到path所表达的目录
*/

int chroot(const char* path);
/*
描述
	切换进程根目录, 切换之后还需要使用chdir("/")来将工作目录切换到新的根目录, 但是这样之后, 程序可能无法访问/dev等文件, 不过chroot之后, 进程原先打开的文件描述符依旧生效, 最后注意只有特权进程才可以改变根目录.
参数
	path 切换根目录
*/

```

## 7.6服务程序后台化

最后, 将服务器程序后台化.

守护进程编写步骤

```c
bool daemonize()
{
	//1. 创建子进程, 关闭父进程, 这样可以使程序在后台运行
	pit_t pid = fork();
	if(pid < 0)
	{
		return false;
	}
    else if ( pid > 0 )
    {
        exit( 0 );
    }
	/*
	2. 设置文件权限掩码, 当进程创建新文件时(open(2)), 文件的权限将是mode & 0777
	*/
    umask( 0 );

    //3. 创建新会话, 设置本进程为进程组的首领
    pid_t sid = setsid();
    if ( sid < 0 )
    {
        return false;
    }
	//4. 切换工作目录
    if ( ( chdir( "/" ) ) < 0 )
    {
        /* Log the failure */
        return false;
    }
	//5. 关闭标准输入设备, 标准输出设备和标准错误输出设备
    close( STDIN_FILENO );
    close( STDOUT_FILENO );
    close( STDERR_FILENO );
	//6.关闭其他已经打开的文件描述符
    ...;
	
    //7. 将标准输入 标准输出 标准错误输出都重定向都/dev/null文件
    open( "/dev/null", O_RDONLY );
    open( "/dev/null", O_RDWR );
    open( "/dev/null", O_RDWR );
    return true;
}
/*
umask 指定创建的默认文件权限
chmod 修改已创建的文件权限

umask 的大小为三个八进制数(前缀是0, 是因为八进制)
假设 umask=value, 则新建文件的默认权限是(0777-value) = 所有者:所属组:其他组
umask=0245, 则新建文件的默认权限是(0777-0245) = 0532 = 101:011:010 = -r-x:-wx:-w-
*/
```



```c
//Linux提供了完成同样功能的库函数
#include <unistd.h>
int daemon(int nochdir, int noclose);
/*
描述
	创建守护进程
参数
	nochdir 指定是否改变工作目录, 传0, 则工作目录将被设置为"/", 否则继续使用当前工作目录
	noclose 是否重定向三个标准到/dev/null, 否则仍旧使用原来的设备
返回值
	成功0, 失败-1+errno
*/
```


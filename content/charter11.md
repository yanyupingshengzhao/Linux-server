# 第11章 定时器

网络程序需要处理的第三类事件就是定时事件, 比如定期检测客户连接的活动状态.服务器程序通常管理众多的定时事件,因此有效的组织这些定时事件, 使之能够在预期时间内被触发且不影响服务器的主要逻辑, 对于服务器的性能有者至关重要的影响.

为此, 我们将每个定时事件分别封装为定时器, 并使用某种容器类数据结构管理, 例如如链表, 排序链表和时间轮, 将所有的定时器串联起来, 以实现对定时事件统一管理.本章主要讨论两种高效的管理定时器容器

1. 时间轮
2. 时间堆

在讨论定时器器容器之前, 先介绍定时的方法.定时是指在一段时间之内触发某段代码的机制, 我们可以在这段代码中一次处理所有到期的定时器.换言之, 定时机制是定时器得以被处理的原动力.

Linux提供了三种定时方法:

1. socket选项SO_RCVTIMEO和SO_SNDTIMEO, 设置socket连接参数后, 函数返回+errno判断是否时间已到.
2. SIGALRM信号, alarm和setitimer函数设置实时闹钟
3. IO复用系统调用的超时参数, 参数指定, 不过需要自己计算已经流逝的时间.


# 第13章 多进程编程

进程是Linux操作系统环境的基础, 它控制者系统上几乎所有的互动.

从程序员角度探讨Linux多进程编程:

1. 复制进程映像的fork系统调用和替换进程映像的exec系列系统调用.
2. 僵尸进程以及如何避免僵尸进程.
3. 进程间通信(Inter-Process Communicaion, IPC)的最简单方式: 管道.
4. 3种System V进程间通信方式: 信号量, 消息队列和共享内存.他们是由AT&T System V2版本引入UNIX, 所以被统称为System V IPC.
5. 在进程间传递文件描述符的通用方法: 通过UNIX本地域socket传递特殊的辅助数据(辅助数据, 5.8).

## 13.1 fork系统调用

```c
#include <sys/types.h>
#include <unistd.h>
pid_t fork(void);

/*
描述
	创建新进程, 函数每次调用返回两次, 在父进程返回子进程PID
	在子进程返回0, 返回值是后续代码判断当前进程是子进程还是父进程的依据
	fork函数复制当前进程, 在内核种创建一个新的进程表项.
	新的表项的属性和子进程有相同, 有不同.
	相同
		复制 堆指针 栈指针 标志寄存器值
		复制 代码段 数据段(堆数据 栈数据 静态数据)
		数据的复制采用策略是cow, 只有在父进程或者子进程对数据对数据执行写操作时发生.(缺页中断->操作系统分配内存->复制父进程数据)
		复制 父进程中打开的文件描述符默认在子进程中打开, 且文件描述符引用计数+1
		复制 父进程的用户根目录 当前工作目录等变量的引用计数+1
		
	不同
		PPID被设为原进程PID 信号位图被清除(原进程的信号处理函数不再对新进程起作用)
	
返回值
	成功 父进程 pid, 子进程 0
	失败 -1+errno
		

*/





```



## 13.2 exec系列系统调用

```c
#include <unistd.h>
extern char** environ;

int execl(const char* path, const char* arg,...);
int execlp(const char* file, const char* arg,...);
int execle(const char* path, const char* arg,..., char* const envp[]);
int execv(const char* path, char* const argv[]);
int execl(const char* file, char* const argv[]);
int execl(const char* path, char* const argv[],char* const envp[]);
/*
描述
	exec调用后, 源程序已经被exec的参数指定的程序完全替换(代码段和数据段)
	exec函数不会关闭原进程打开的文件描述符, 除非该文件描述符被设置了类似SOCK_CLOEXEC属性
	
参数
	path 指定可执行文件的完整路径
	file 文件名, 具体文件在环境变量PATH中寻找
	argv 接收参数数组, 传递给新程序的main函数
	envp 设置新程序环境变量, 如果不设置, 则新程序将由全局变量environ指定环境变量
返回
	成功 无返回值
	失败 -1+errno
*/

```



## 13.3处理僵尸进程

对于多进程程序而言, 父进程一般需要跟踪子进程的推出状态.因此, 当子进程结束运行时, 内核不会立即释放该进程的进程表表项, 以满足父进程后续对该子进程退出信息的查询(如果父进程还在运行). 在子进程结束运行之后, 父进程读取其退出状态之前, 我们称该子进程处于`僵尸态`. 

另外一种使子进程进入`僵尸态`的情况是: 父进程结束或者终止异常, 而子进程继续运行. 此时, 子进程的PPID将被操作系统设置为1, 即init进程.init进程接管了该子进程, 并等待它结束.在父进程退出之后,子进程退出之前, 该子进程处于僵尸态.

由此可见, 无论哪种情况, 如果父进程没有正确地处理子进程的返回信息, 子进程都将停留在僵尸态, 并占据着内核资源, 因为内核资源有限.

下面的这对函数在父进程中调用, 以等待子进程的结束, 并活得子进程返回的信息, 从而避免了僵尸进程的产生, 或者使子进程的僵尸态立即结束.

```c
#include <sys/types.h>
#include <sys/wait.h>
pid_t wait(int* stat_loc);


/*
描述
	wait函数将阻塞等待任意一个子进程运行结束为止, 返回运行结束的子进程PID,
	并将该子进程的推出状态存储域stat_loc参数指向的内存中.
	
参数
	stat_loc 返回结束状态

*/
pid_t waitpid(pid_t pid, int* stat_loc, int option);
/*
描述
	wait函数将等待某个子进程运行结束为止, 返回运行结束的子进程PID,
	并将该子进程的推出状态存储域stat_loc参数指向的内存中.
	
*/
/*
描述
	waitpid函数指定回收子进程pid, 可设置非阻塞等待.
参数
	pid pid=-1与wait相同, pid>0 特定子进程
	stat_loc 返回结束状态
	option 控制wiatpid行为
		WNOHANG	非阻塞	最常用
			若目标子进程未结束或者意外终止, waitpid立即返回0;
			若目标子进程正常退出, 则waitpid返回子进程PID
			否则返回-1+errno

*/
```

宏 解析子进程退出状态

![image-20230210132437421](./charter13.assets/WAIT.png)

何时使用waitpid

显然要在事件发生的情况下执行非阻塞调用才能提高程序的效率.

对于waitpid函数而言, 我们最好在某个子进程推出后再调用它.

而当子进程结束时, 它将给父进程发送SIGCHILD信号, 因此我们可以再父进程捕获SIGCHILD信号处理函数中调用waitpid函数以彻底结束一个子进程.

```c
static void handle_child(int sig)
{
	pid_t pid;
	int stat;
	while((pid =waitpid(-1, &stat, WNOHANG))>0)
	{
		//对结束子进程后续处理
	}
}
```



## 13.4管道

管道是父进程和子进程间通信的常用手段, 而且只能用在父子进程间.

管道能在父子进程间传递数据, 利用的是fork调用之后, 两个管道文件描述符(fd[0] fd[1])都能保持打开.

一对这样的文件描述符只能保证父子进程间一个方向的数据传输.父进程和子进程必须有一个关闭fd[0], 另一个关闭fd[1].

比如我们要实现从父进程到子进程写数据, 就应该按照如下图操作:

![image-20230210133713210](./charter13.assets/管道通信.png)

此外, socket编程接口还提供了一个创建全双工管道的系统调用: socketpair

## 13.5信号量

### 13.5.1信号量原语

将检查true/false和修改统一起来, 不会出现判断和修改分离, 这样的信号叫做二进制信号量.

Linux信号量集, 即信号量集, 不是单个信号量.

### 13.5.2 semget系统调用

```c
#include <sys/sem.h>
int semget(key_t key, int num_sems, int sem_flags);
/*
描述
	semget系统调用用于创建一个新的信号量集, 或者获取一个已经存在的信号量集
参数
	key 键值对, 用于标识一个全局唯一的信号量集, 就像文件名全局唯一地标识一个文件一样.要通过信号量通信的进程需要使用相同的键值来创建/获取信号量
	num_sems 指定要创建/获取的信号量集中信号量的数目.
		如果是创建, 则该值必须指定;
		如果是获取, 则可以设为0.
	sem_flags 参数指定一组标志.
		它的低端9个比特是该信号量的权限, 其格式和含义都与系统调用open的mode参数相同.
		此外还可以和IPC_CREAT标志按位或运算以创建新的信号量集. 此时信号量即使存在, 也不会产生错误.
		还可以联合IPC_CREAT+IPC_EXCL标志来确保创建一组新的, 唯一的信号量集.
			如果信号量集存在, 则semget返回错误并设置errno为EEXIST
		这种方式和open使用O_CREAT+OEXCL标志排他式地打开一个文件相似.
返回值
	成功返回一个正整数, 它式信号量集的标识符
	失败返回 -1 +errno
*/

//内核中的信号量集合结构体
//该结构用于描述IPC对象（信号量，　共享内存和消息队列）的权限
struct ipc_perm
{
    key_t key;		//键值
    uid_t uid;		//所有者有效用户ID
    gid_t gid;		//所有者有效组ID
    uid_t cuid;		//创建者有效用户ID
    gid_t cgid;		//创建者有效组ID
    mode_t mode;	//访问权限
    ...;			//其他字段省略
}

struct semid_ds
{
    struct ipc_perm sem_perm;		//信号量的操作权限
    unsigned long int sem_nsems;	//该信号量集中的信号量数目
    time_t sem_otime;				//最后一次调用semop的时间
    time_t sem_ctime;				//最后一次调用semctl的时间
    
    ...;							//其他字段省略
}


/*
semget对semid_ds结构体的初始化包括:

将 sem_perm.cuid 和 sem_perm.id 设置为调用进程的有效用户 ID;
将 sem_perm.cgid 和 sem_perm.gid 设置为调用进程的有效组 ID;
将 sem_perm.mode 的最低9位设置为 sem_f lags 参数的最低9位;
将 sem_nsems 设置为 num_sems;
将 sem_otime 设置为0;
将 sem_ctime 设置为当前的系统时间

*/
```



### 13.5.3 semop系统调用

sem operate, semop系统调用改变信号量的值, 即执行P V 操作, 是在信号量集上的操作.



```c
//再讨论semop之前, 我们需要先介绍与每个信号量关联的一些重要的内核变量:
unsigned short semval;		//信号量的值
unsigned short sezcnt;		//等待信号量值变为0的进程数量 zero
unsigned short sencnt;		//等待信号量值增加的进程数量
pid_t sempid;				//最后一次执行semop操作的进程ID

//semop对信号量的操作实际是对这些内核变量的操作
#include <sys/sem.h>
int semop(int sem_id, struct sembuf* sem_ops, size_t num_sem_ops);
/*
描述
	semop系统调用改变信号量的值, 即执行PV操作
参数
	sem_id 是semget调用返回的信号量集标识符, 用以指定被操作的目标信号量集
	sem_ops 指向一个sembuf结构体类型的数组, 
	sem_sem_ops 指定sem_ops数组的长度, 指定执行的操作个数,
		semop对数组sem_ops中的每个成员按照数组顺序依次执行操作, 并且该过程是原子操作, 以避免别的进程在同一时刻按照不同顺序对该信号集中的信号量执行semop操作导致的竞态条件
		
返回值
	成功0
	失败-1+errno, 失败的时候sem_ops数组中指定的所以操作都不被执行
	
*/

struct sembuf
{
    unsigned short int sem_num;	//信号量集中的信号量的编号, 0代表第一个
    short int sem_op;			//指定操作类型, 取正整数 0 负整数, 还受到sem_flg影响

    short int sem_flg;			//设置sem_op系统调用的行为, 类似非阻塞IO操作
}
/*
sem_flg 可选值IPC_NOWAIT, SEM_UNDO
    IPC_NOWAIT 无论信号量操作是否成功, semop调用操作都立即返回, 类似于非阻塞IO
    SEM_UNDO 当进程退出时取消正在执行的semop操作

sem_op 可取 正整数 0 负整数
	sem_op取正整数 则semop将被操作的信号量的值semval增加sem_op. 该操作要求调用进程对被操作的信号量集拥有写权限. 此时若设置了SEM_UNDO标志, 则系统将更新进程的semadj变量(用于跟踪进程对信号量的修改情况)
	sem_op=0, 则表示这是一个"等待0"(wait-for-zero)操作.
*/
```



### 13.5.4 semctl系统调用

```c
#include <sys/sem.h>
int semctl(int sem_id, int sem_num, int command, ...);
/*
描述
	semctl系统调用允许调用者对信号量进行直接控制
参数
	sem_id参数是由semget调用返回的信号量集标识符, 用以指定被操作的信号量集
	sem_num参数指定被操作信号量在信号量集中的编号.
	command指定要执行的命令, 有的命令需要调用者传递第4个参数.第4个参数类型由用户自己定义, 但sys/sem.h头文件给出了它的推荐格式
*/

//第4个参数推荐格式
union semun
{
    int val;				//用于SETVAL命令
    struct semid_ds* buf;	//用于IPC_STAT和IPC_SET命令
    unsigned short* array;	//用于GETALL和SETALL命令
    struct seminfo* __buf;	//用于IPC_INFO命令
}
struct seminfo
{
    int semmap;	//Linux内核没有使用
    int semmni;	//系统最多可以拥有的信号量集数目
    int semmns;	//系统最多可以拥有的信号量数目
    int semmnu;	//Linux内核没有使用
    int semmsl;	//一个信号量集最多允许包含的信号量数目
    int semopm;	//semop依次最多能执行的sem_op操作数目
    int semume;	//Linux内核没有使用
    int semusz;	//sem_undo结构体的大小
    int semvmx;	//最大允许的信号量值
    int semaem;	//最多允许的UNDO次数(零SEM_UNDO标志的semop操作次数)
}
```

![image-20230210223802018](./charter13.assets/semctl支持的命令.png)

### 13.5.5特殊ipc_private

semget的调用者可以给其key参数传递一个特殊的键值`IPC_PRIVATE`(其值为0), 这阿姨那个无论该信号量是否已经存在, semget都将创建一个新的信号量.使用该键值创建的信号, 并非像它的名字声称的那样进程私有的.其他进程, 尤其是子进程, 也有方法来访问这个信号量.所以semget的man手册的BUGS部分说, 使用名字`IPC_PRIVATE`有些误导(历史原因), 应该称为`IPC_NEW`.比如下面的代码就在父子进程间使用`IPC_PRIVATE`信号量来同步.



同时, `IPC_PRIVATE`在共享内存和消息队列中含义相同.

## 13.6共享内存

最高效的IPC机制, 因为不涉及进程间的任何数据传输.

### 13.6.1 shmget系统调用

```c
#include <sys/shm.h>
int shmget(key_t key, size_t size, int shmflg);
/*
描述
	shmget系统调用创建一段新的共享内存, 或者获取一段已经存在的共享内存.
参数
	key 键值对, 用来标识一段全局唯一的共享内存
	size 指定共享内存的大小, 单位字节.如果获取内存size可为0
	shmflg参数使用和含义于semget参数一致, 不过shmget支持两个额外标志
		SHM_HUGETLB, 类似mmap的MAP_HUGETLE标志, 系统将使用"大页面"来为共享内存分配空间
		SHM_NORESERVE, 类似mmap的MAP_NORESERVE标志, 不为共享内存保存交换分区(swap空间).这样当物理内存不足的时候, 对该共享内存执行写操作将触发SIGSEGV信号.
		
返回值
	成功返回一个正整数, 即共享内存的标识符
	失败返回 -1+errno

*/

/*
shmget用于创建共享内存, 则这段共享内存的所有字节都被初始化为0, 与之关联的内核数据结构shmid_ds将被创建并初始化.


*/

struct shmid_ds
{
    struct ipc_perm shm_perm;	//共享内存的操作权限
    size_t shm_segsz;			//共享内存大小, 单位是字节
    __time_t shm_atime;			//对这段内存最后一次调用shmat的时间
    __time_t shm_dtime;			//对这段内存最后一次调用shmdt的时间
    __time_t shm_ctime;			//对这段内存最后一次调用shmctl的时间
    __pid_t shm_cpid;			//创建者的PID
    __pid_t shm_lpid;			//最后一次执行shmat或shmdt操作的进程PID
    shmatt_t shm_nattach;		//目前关联到此共享内存的进程数量
    ...;
}

/*
shmget对shmid_ds结构体的初始化包括
	将 shm_perm.cuid 和 shm_perm.uid 设置为调用进程的有效用户ID将
    将 shm_perm.cgid 和 shm_perm.gid 设置为调用进程的有效组ID
    将 shm_perm.mode 的最低9位设置为shmflg参数的最低9位
    将 shm_segsz 设置位size
    将 shm_lpid, shm_nattach, shm_atime, shm_dtime 设置为0
	将 shm_ctime设置为当前时间


*/

```



### 13.6.2 shmat系统调用

共享内存创建/获取之后, 我们不能立即访问它, 而是需要先将它关联到进程空间中. 使用完共享内存之后, 需要从进程地址空间分离.

```c
#include <sys/shm.h>
void* shmat(int shm_id, const void* shm_addr, int shmgflg);
int shmdt(const void* shm_addr);
/*
描述
	shmat 
参数
	shm_id shmget返回的key
	shm_addr参数指定将共享内存关联到进程的哪块地址空间, 最终的效果还受到shmflg参数的可选标志SHM_RND的影响.
		shm_addr=NULL, 则被关联的地址由操作系统选择, 这是推荐做法,也是代码可移植性确保
		shm_addr=非空, 并且SHM_RND标志未设置, 则共享内存被关联到addr指定地址处.
		shm_addr=非空, 并且SHM_RND设置, 则被关联的地址是[shm_addr-(shm_addr%SHMLBA)]...
	shmflg 除了SHM_RND还支持
		SHM_RDONLY, 进程只能读.未指定读写均可(创建者必须创建时指定读写权限)
		SHM_REMAP, 若地址shmaddr已被惯量到一段共享内存上, 则重新关联.
		SHM_EXEC, 对共享内存的执行权限, 对于共享内存而言, 执行权限实际上和读权限一样

返回值
	成功地址, 失败(void*)-1+errno

此外
	shmat成功时, 将修改内核数据结构shmid_ds的部分字段
		将shm_nattach+1
		将shm_lpid设置为调用进程PID
		将shm_atime设置为当前时间
*/

/*
描述
	shmdt 将关联到shm_addr处的共享内存从进程中分离
返回值
	成功 0, 失败-1+errno
此外
	shmdt成功时, 会修改内核数据结构shmid_ds的部分字段
		将shm_nattach-1
		将shm_lpid设置为调用进程的PID
		将shm_dtime设置为当前时间

*/

```

### 13.6.3 shmdt系统调用

```c
#include <sys/shm.h>
int shmctl(int shm_id, int command, struct shmid_ds* buf);
/*
描述
	shmctl系统调用用于控制共享内存的某些属性
参数
	shm_id 由shmget获取的共享内存标识符
	command 执行命令
*/
```

![image-20230211140205775](./charter13.assets/shmctl.png)

### 13.6.4共享内存的posix方法



### 13.6.5共享内存实例

## 13.7消息队列

消息队列是在两个进程之间传递二进制数据块的一种简单有效的方式.每个数据块都有个特定的类型, 接收方可以根据类型来有选择地接收数据, 而不一定像管道和命名管道那样必须以先进先出的方式来接收数据.

### 13.7.1 msgget系统调用

```c
#include <sys/msg.h>
int msgget(key_t key, int msgflg);
/*
描述
	msgget 创建或者获取一个消息队列
参数
	key 键值对, 用来标识一个全局唯一的消息队列
	msgflg参数的使用和semget系统调用的sem_flags参数相同
返回值
	成功key
	失败-1+errno
*/

struct msqid_ds
{
    struct ipc_perm msg_perm;	//消息队列操作权限
    time_t msg_stime;			//最后一次调用msgsnd的时间
    time_t msg_rtime;			//最后一次调用msgrcd的时间
    time_t msg_ctime;			//最后一次修改的时间
    unsigned long __msg_cbytes;	//消息队列中已有的字节数
    msgqnum_t msg_qnum;			//消息队列中已有的消息数
    msglen_t msg_qbytes;		//消息队列允许的最大字节数
    pid_t msg_lspid;			//最后执行msgsnd的进程PID
    pid_t msg_lrpid;			//最后执行msgrcv的进程PID
}

/*
描述
*/
```

### 13.7.2 msgnd系统调用

```C
#include <sys/msg.h>
int msgsnd(int msqid, const void* msg_ptr, size_t msg_sz, int msgflg);
/*
描述
	msgsnd向消息队列msqid添加一条数据
参数
	nsqid 消息队列key
	msg_ptr队列
	msg_sz消息msg_ptr数据部分长度, 可为0, 表示无消息数据
	msgflg通常仅支持IPC_NOWAIT表示, 即以非组的方式发送消息.
		默认情况, 若队列满了, 则msgsnd阻塞.
		若指定了IPC_NOWAIT标志, msgsnd立即返回并设置errno为EAGAIN
		
		同时处于阻塞状态的msgsnd调用可被两种异常所中断
			消息队列被删除, 此时msgsnd立即返回并设置errno为EINRM
			程序接收到信号, 此时msgsnd调用立即返回并设置errno为WINTR
			
返回值
	成功 0, 失败-1+errno
此外
	msgsnd成功, 将会修改内核数据结构msqid_ds部分字段
		将msg_qnum+1
		将msg_lspid设置为调用进程ID
		将msg_stime设置为当前的时间
*/
```



### 13.7.3 msgrcv系统调用

```c
#include <sys/msg.h>
int msgrcv(int msqid, void* msg_ptr, size_t msg_sz, long int msgtype, int msgflg);
/*
描述
	msgrcv系统调用从消息队列获取消息, 该函数还可以是阻塞函数.
参数
	msqid 键值对
	msg_ptr存储接收消息
	msg_sz 指定消息数据部分长度
	msgtype指定接收消息类型
		msgtype=0 读取消息队列中的第一个消息
		msgtype>0 读取消息队列第一个类型为msgtype的消息
		msgtype<0 读取消息队列第一个类型值比msgtype的绝对值小的消息
	msgflg 控制msgrcv函数的行为, 它可以是如下一些标志的按位或
		IPC_NOWAIT 如果消息队列无消息, 直接返回并设置errno为ENOMSG
		IPC_EXPECT 如果msgtype大于0, 则接收消息队列第一个非msgtype类型的消息
		MSG_NOERROR 如果消息数据部分的长度超过了msg_sz, 就将它截断.
返回值
	成功0
	失败-1+errno
此外	
	同时处于阻塞状态的msgsnd调用可被两种异常所中断
		消息队列被删除, 此时msgsnd立即返回并设置errno为EINRM
		程序接收到信号, 此时msgsnd调用立即返回并设置errno为WINTR
	msgrcv成功时还会修改内核数据结构msqid_ds部分字段:
		将 msg_qnum-1
		将 msg_lrpid设置为调用进程ID
		将 msg_rtime设置为当前的时间
*/
```



### 13.7.4msgctl系统调用

```c
#include <sys/msg.h>
int msgctl(int msqid, int conmmand, struct msqid_ds* buf);
/*
描述
	msgctl系统调用控制消息队列的某些属性
参数
	msqid 键值对
	conmmand 命令
	buf 接收内核信息或者设置内核数据结构参数
*/
```

![image-20230211163844994](./charter13.assets/msgctl.png)

## 13.8 ipc命令

## 13.9在进程间传递文件描述符

在fork调用之后, 父进程中打开的文件描述符在子进程中依然打开, 所以文件描述符可以很方便的从父进程传递到子进程.值得注意的是, 传递一个文件描述符并不是传递一个文件描述符的值, 而是要在接收进程中创建一个新的文件描述符, 并且该文件描述符和发送进程中被传递的文件描述符和发送进程中被传递的文件描述符执行内核中相同的文件表项.

如何在两个毫不相干的进程之间传递文件描述符, 利用UNIX域socket在进程间传递特殊的辅助数据, 以实现文件描述符的传递.

```c
#include <sys/socket.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

static const int CONTROL_LEN = CMSG_LEN( sizeof(int) );
//fd UNIX域socket
void send_fd( int fd, int fd_to_send )
{
    struct iovec iov[1];
    struct msghdr msg;
    char buf[0];

    iov[0].iov_base = buf;
    iov[0].iov_len = 1;
    msg.msg_name    = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov     = iov;
    msg.msg_iovlen = 1;

    cmsghdr cm;
    cm.cmsg_len = CONTROL_LEN;
    cm.cmsg_level = SOL_SOCKET;
    cm.cmsg_type = SCM_RIGHTS;
    *(int *)CMSG_DATA( &cm ) = fd_to_send;
    msg.msg_control = &cm;	//设置辅助数据
    msg.msg_controllen = CONTROL_LEN;

    sendmsg( fd, &msg, 0 );
}

int recv_fd( int fd )
{
    struct iovec iov[1];
    struct msghdr msg;
    char buf[0];

    iov[0].iov_base = buf;
    iov[0].iov_len = 1;
    msg.msg_name    = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov     = iov;
    msg.msg_iovlen = 1;

    cmsghdr cm;
    msg.msg_control = &cm;
    msg.msg_controllen = CONTROL_LEN;

    recvmsg( fd, &msg, 0 );

    int fd_to_read = *(int *)CMSG_DATA( &cm );
    return fd_to_read;
}

int main()
{
    int pipefd[2];
    int fd_to_pass = 0;

    int ret = socketpair( PF_UNIX, SOCK_DGRAM, 0, pipefd );
    assert( ret != -1 );

    pid_t pid = fork();
    assert( pid >= 0 );

    if ( pid == 0 )
    {
        close( pipefd[0] );
        fd_to_pass = open( "test.txt", O_RDWR, 0666 );
        send_fd( pipefd[1], ( fd_to_pass > 0 ) ? fd_to_pass : 0 );
        close( fd_to_pass );
        exit( 0 );
    }

    close( pipefd[1] );
    fd_to_pass = recv_fd( pipefd[0] );
    char buf[1024];
    memset( buf, '\0', 1024 );
    read( fd_to_pass, buf, 1024 );
    printf( "I got fd %d and data %s\n", fd_to_pass, buf );
    close( fd_to_pass );
}
```


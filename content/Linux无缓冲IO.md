open函数

```c
#include <fcntl.h>
#include <sys/stat.h>
int open(const char *pathname, int flags);
int open(const char *pathname, int flags, mode_t mode);
/*
描述
	打开或者新建一个文件,int fd = open("test.log", O_RDWR | OCREAT, 0755)；
参数
	pathname参数是要打开或创建的文件名,和fopen一样, pathname既可以是相对路径也可以是绝对路径。
	flag 必选项:以下三个常数中必须指定一个, 且仅允许指定一个。
		O_RDONLY 只读打开
		O_WRONLY 只写打开
		O_RDWR 可读可写打开
		以下按位与
			O_APPEND表示追加。如果文件已有内容, 这次打开文件所写的数据附加到文件的末尾而不覆盖原来的内容。
			O_CREAT若此文件不存在则创建它。使用此选项时需要提供第三个参数mode, 表示该文件的访问权限。文件最终权限：mode & ~umask
			O_EXCL 如果同时指定了O_CREAT,并且文件已存在,则出错返回。
			O_TRUNC 如果文件已存在, 将其长度截断为为0字节。
			O_NONBLOCK 对于设备文件, 以O_NONBLOCK方式打开可以做非阻塞I/O(NonblockI/O),非阻塞I/O。
返回值
	成功返回最小且未被占用的file descriptor
	失败 -1+errno 
*/
```

close函数

```c
int close(int fd);
/*
描述
	关闭文件
参数
	fd 文件描述符
返回值
	成功 0
	失败 -1+errno
此外
	进程关闭会自动关闭所有文件描述符
	
*/
```

read函数

```c
ssize_t read(int fd, void *buf, size_t count);
/*
描述
	从打开的设备或文件中读取数据
参数
	fd: 文件描述符
	buf: 读上来的数据保存在缓冲区buf中
	count: buf缓冲区存放的最大字节数
返回值
	>0：读取到的字节数
	=0：文件读取完毕
	-1： 出错，并设置errno
*/
```

write函数

```c
ssize_t write(int fd, const void *buf, size_t count);
/*
描述
	向打开的设备或文件中写数据
参数
	fd: 文件描述符
	buf: 缓冲区，要写入文件或设备的数据
	count: buf中数据的长度
返回值
	成功：返回写入的字节数
	错误：返回-1并设置errno

*/
```

lseek函数

```c
off_t lseek(int fd, off_t offset, int whence);

/*
描述
	所有打开的文件都有一个当前文件偏移量(current file offset),以下简称为cfo. cfo通常是一个非负整数, 用于表明文件开始处到文件当前位置的字节数. 读写操作通常开始于 cfo, 并且使 cfo 增大, 增量为读写的字节数. 文件被打开时, cfo 会被初始化为 0, 除非使用了 O_APPEND.使用 lseek 函数可以改变文件的 cfo.
参数
	fd: 文件描述符
	offset：文件偏移量， offset 的含义取决于参数 whence。
	whence
		SEEK_SET：文件开头位置
		SEEK_CUR：当前位置
		SEEK_END：文件结尾位置
		
返回值
	成功：返回新的偏移量

*/


```

fcntl函数

```c
#include <fcntl.h>
int fcntl(int fd, int cmd, ... /* arg */ );

/*
描述
	改变已经打开的文件的属性.
	
参数
	fd: 文件描述符
	cmd
		若cmd为F_DUPFD, 复制文件描述符, 与dup相同
		若cmd为F_GETFL, 获取文件描述符的flag属性值
		若cmd为 F_SETFL, 设置文件描述符的flag属性
	arg 可变参数
返回值
	返回值取决于cmd
	成功
		若cmd为F_DUPFD, 返回一个新的文件描述符
		若cmd为F_GETFL, 返回文件描述符的flags值
		cmd为 F_SETFL, 返回0
	失败：返回-1, 并设置errno值
    
*/

//fcntl函数常用的操作
//复制一个新的文件描述符:
int newfd = fcntl(fd, F_DUPFD, 0);

//获取文件的属性标志
int flag = fcntl(fd, F_GETFL, 0);
    
//设置文件状态标志
flag = flag | O_APPEND;
fcntl(fd, F_SETFL, flag);

//获得和设置文件描述符的flag属性: 
int flag = fcntl(fd, F_GETFL, 0);
flag |= O_APPEND;//末尾追加
fcntl(fd, F_SETFL, flag);

//获取和设置文件描述符为非阻塞
int flags = fcntl(fd[0], F_GETFL, 0); 
flag |= O_NONBLOCK;//非阻塞
fcntl(fd[0], F_SETFL, flags);
```

stat/lstat函数

```c

int stat(const char *pathname, struct stat *buf);
int lstat(const char *pathname, struct stat *buf);

/*
描述
	获取文件属性
参数
	

返回值
	成功返回 0
	失败返回 -1

stat函数和lstat函数的区别
	stat函数和lstat函数的区别
	对于连接文件,调用lstat函数获取的是链接文件本身的属性信息; 而stat函数获取的是链接文件指向的文件的属性信息.
*/

struct stat 
{
	    dev_t          st_dev;        //文件的设备编号
	    ino_t           st_ino;        //节点
	    mode_t         st_mode;      //文件的类型和存取的权限
	    nlink_t         st_nlink;     //连到该文件的硬连接数目，刚建立的文件值为1
	    uid_t           st_uid;       //用户ID
	    gid_t           st_gid;       //组ID
	    dev_t          st_rdev;      //(设备类型)若此文件为设备文件，则为其设备编号
	    off_t          st_size;      //文件字节数(文件大小)
	    blksize_t       st_blksize;   //块大小(文件系统的I/O 缓冲区大小)
	    blkcnt_t        st_blocks;    //块数
	    time_t         st_atime;     //最后一次访问时间
	    time_t         st_mtime;     //最后一次修改时间
	    time_t         st_ctime;     //最后一次改变时间(指属性)
};
```


# 第5章 Linux网络编程基础API

本章主要探讨, Linux网络编程基础API与内核中TCP/IP协议族值间的关系.

1. socket地址API, 即(ip, port).
2. socket基础API, sys/socket.h, 
   - 创建socket 
   - 命名socket 
   - 监听socket 
   - 接收连接 
   - 发起连接 
   - 读取数据 
   - 获取地址信息 
   - 检测带外标记
   - 以及读取和设置socket选项.
3. 网络信息API, netdb.h
   - 实现主机名和IP地址之间的转换
   - 服务名称端口之间的转换

## 5.1 socket地址API

首先引入两个概念, 主机字节序和网络字节序

### 5.1.1 主机字节序和网络字节序

小端字节序(little endian): 整数的高位字节存储在内存高地址

```
数据: 123456789
地址: 876543210
```

大端字节序(big endian): 整数的高位字节存储在内存低地址

```
数据: 123456789
地址: 012345678
```

规定:

- 网络字节序采用大端存储

- 主机字节序一般采用小端存储

注意

- 同一台主机上的网络字节序在不同进程也需要字节序列匹配

API

```c
#include <netinet/in.h>
unsigned long int htonl(unsigned long int hostlong);
unsigned short int htons(unsigned short int hostshort);
unsigned long int ntohl(unsigned long int netlong);
unsigned short int ntohs(unsigned long int netshort);

short 2B
short int 2B
int 4B
long 4B
long int 4B
long long int 8B
```

- 长整型转ip, 短整型转port

### 5.1.2 通用socket地址

socket地址结构体`sockaddr`:

```c
#include <bits/socket.h>
struct sockaddr
{
	sa_family_t sa_family;
	char sa_data[14];
};
```

#### sa_family_t sa_family

地址族类型`sa_family_t`通常与协议类型`protocol family, domian`对应

| 协议族   | 地址族  | 描述             |
| -------- | ------- | ---------------- |
| PF_UNIX  | AF_UNIX | UINX本地域协议族 |
| PF_INET  | AF_INET | TCP/IPv4协议族   |
| PF_INET6 | AF_INT6 | TCP/IPv6协议族   |

注意

- 宏`PF_*`和`AF_*`都定义在bits/socket.h, 二者等价, 可混用

#### char sa_data[14];

sa_data成员用于存放socket地址值, 不同的PF_对应的`sa_data`结构体类型不同, 含义和长度也各不同.

| 协议族   | 地址值含义与长度                                             |
| -------- | ------------------------------------------------------------ |
| PF_UNIX  | 文件的路径名, 长度可达108字节                                |
| PF_INET  | 16bit端口号和32bit IPv4地址, 共6Byte                         |
| PF_INET6 | 16bit端口号, 32bit 流标志, 128bit IPv6地址, 32bit范围ID, 共26Byte |



显然sa_data无法容纳多数协议族地址值, 因此, Linux引入新的通用socket地址结构体:

```c
#include <bits/socket.h>
struct sockaddr_storage
{
	sa_family_t sa_family;
	unsigned long int __ss_align;
	char __ss_padding[128-sizeof(__ss_align)];
};
/*
这个结构体不仅提供了足够大的空间用于存放地址值, 而且是内存对齐的(由于__ss_align成员作用)
/*
```

### 5.1.3 专用socket地址

上面提到的两个socket地址结构体显然不好用, 比如设置和获取IP地址和端口号就必须执行复杂的位操作,  所以Linux为各个协议族提供了专门的socket地址结构体.

#### UNIX本地域socket地址结构体

```c
#include <sys/un.h>
struct sockaddr_un
{
	sa_family_t sin_family;//地址族:AF_UNIX
	char sun_path[108];//文件路径名
	
};
```

#### IPv4 socket地址结构体

```c
struct sockaddr_in
{
	sa_family_t sin_family;//地址族, AF_INET
	u_int16_t sin_port;//端口号, 要用网络字节序表示
	struct in_addr sin_addr;//IPv4地址结构体
};
struct in_addr
{
	u_int32_t s_addr;//IPv4地址, 要用网络字节序表示
};

```

#### IPv6 socket地址结构体

```c
struct sockaddr_in6
{
	sa_family_t sin6_family;//地址族, AF_INET6
	u_int16_t sin6_port;//端口号, 要用网络字节序表示
    u_int32_t sin6_flowinfo;//流信息, 应设置为0
	struct in6_addr sin6_addr;//IPv6地址结构体
    u_int32_t sin6_scope_id;//scope ID 尚处于试验阶段
};
struct in6_addr
{
	unsigned char sa_addr[16];//IPv6地址, 要用网络字节序表示
};

```

#### 使用

所有专用socket地址(以及sockaddr_storage)类型的变量在实际使用时, 都需要转化为socket地址类型`sockaddr`(强制转换即可), 因为所有的socket编程接口使用的地址参数类型都是`sockaddr`.

### 5.1.4 IP地址转换函数

点分十进制地址--提高可读性.

十六进制ipv6地址--提高可读性.

编程使用--将其转化为整数方能使用.

记录日志--点分十进制地址.

故引入地址转换函数, 便于实现IP地址表达类型转换.

#### 适用于IPv4地址转换函数

```c
#incldue <arpa/inet.h>

in_addr_t inet_addr(const char* strptr);
/*
描述
	将点分十进制ip地址转化为网络字节序整数表示返回
参数
	strptr 点分十进制IP字符串
返回值
	成功返回网络字节序地址
	失败返回INADDR_NONE

*/

int inet_aton*(const char* cp, struct in_addr* inp);
/*
描述
	将点分十进制ip地址转化为网络字节序整数ip地址表示,结果存于inp
参数
	strptr 点分十进制IP字符串
返回值
	成功返回1
	失败返回0
*/

char* inet_ntoa(struct in_addr in);
/*
描述
	将网络字节序整数ip地址转化为点分十进制ip地址表示
参数
	in 将网络字节序整数ip地址
返回值
	成功返回 点分十进制表示的Ipv4地址
	失败返回NULL
此外
	补充: 该函数为不可重入, 指函数返回值为指针, 该指针指向函数内部一个静态变量, 再次执行函数, 该静态变量被覆盖.
*/
```



#### 适用于IPv4和IPv6的地址转换

```c
#include <arpa/inet.h>

//函数中p和n分别代表表达（presentation)和数值（numeric)
int inet_pton(int af, const char* src, void* dst);
/*
描述
	将Ipv4/Ipv6地址从点分十进制(Ipv4)/十六进制字符串(Ipv6)转化为网络字节序表示的IP地址, 并将结果存于dst
参数
	af 协议类型
	src 点分十进制ip(或者十六进制ipv6)地址指针
	dst 存放转换结果, 
		Ipv4 dst类型 in_addr
		Ipv6 dst类型 in6_addr
返回值
	成功返回1, 失败返回0, 并设置errno
*/

const char* inet_ntop(int af, const void* src, char* dst, socklen_t size);
/*
描述
	将二进制地址转换为字符串
参数
	af 协议类型
	src 点分十进制ip(或者十六进制ipv6)地址指针
	dst 存放转换结果
		Ipv4 dst类型 in_addr
		Ipv6 dst类型 in6_addr
	size INET_ADDRSTRLEN/INET6ADDRSTRLEN, 对应Ipv的in_addr的大小
返回值
	成功目标存储单元地址,
	失败返回NULL并设置errno
*/
#include <netinet/in.h>
#define INET_ADDRSTRLEN 16
#define INET6ADDRSTRLEN 46
```

## 5.2 socket基础函数

### 5.2.1创建socket

#### API

```c
#include <sys/types.h>
#include <sys/socket.h>
int socket(int domain, int type, int protocol);

/*
domian 
    PF_INET 
    PF_INET6 
    PF_UNIX
type 
	SOCK_STREAM
	SOCK_UGRAM
protocol
	SOCK_NONBLOCK	非阻塞socket
	SOCK_CLOEXEC	fork子进程后, 在子进程中关闭该socket
	protocol在前两个参数构成的协议集合下, 再选择一个具体协议, 几乎所有情况下, 都使用0, 表示默认协议.
返回值
	成功返回socket文件描述符
	失败返回-1, 并设置errno



*/
```

### 5.2.2命名socket

创建socket, 我们指定了地址族, 但是并未指定使用该地址族中的哪一个socket地址, 将一个socket与socket地址绑定的过程称为socket命名.

在服务器中,我们通常要命名socket, 因为只有明明后客户端才能知道该如何连接它.

在客户端中, 通常不需要命名, 操作系统会自动分配socket地址.

#### API

命名socket地址系统调用

```c
#include <sys/types.h>
#include <sys/socket.h>

int bind(int sockfd, const struct sockaddr* my_addr, socklen_t addrlen);
/*
bind
	将sockfd与my_addr绑定
addrlen
	指出socket地址长度
返回值
	成功返回0
	失败返回-1, 并设置errno
	errno
		EACCES, 被绑定地址是受保护地址, 进超级用户能够访问.例如端口0-1023
		EADDRINUSE, 绑定正在使用的IP地址,例如绑定处于TIME_WAIT状态的socket

*/
```

### 5.2.3监听socket

socket被命名之后不能立即连接, 还需要创建监听队列以存放待处理的客户连接.

#### API

```c
#include <sys/socket.h>

int listen(int sockfd, int backlog);

/*
sockfd
	监听端口
	
backlog
	监听队列长度, 若监听队列长度超过backlog, 服务器不再受理新的客户连接, 客户端收到ECONNREFUSED错误信息
	内核版本2.2之前 backlog为半连接状态(SYN_RCVD)和完全全连接状态(ESTABLISHED)总和
	内核2.2后, backlog为完全全连接状态的连接个数
	半连接状态socket上限 /proc/sys/net/ipv4/tcp_max_syn_backlog内核参数定义.
	backlog典型值为5

*/
```



测试backlog限制效果

```c
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

static bool stop = false;
static void handle_term( int sig )
{
    stop = true;
}

int main( int argc, char* argv[] )
{
    signal( SIGTERM, handle_term );

    if( argc <= 3 )
    {
        printf( "usage: %s ip_address port_number backlog\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );
    int backlog = atoi( argv[3] );

    int sock = socket( PF_INET, SOCK_STREAM, 0 );
    assert( sock >= 0 );

    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int ret = bind( sock, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );

    ret = listen( sock, backlog );
    assert( ret != -1 );

    while ( ! stop )
    {
        sleep( 1 );
    }

    close( sock );
    return 0;
}
```

执行程序

```
./testliseten ip地址A 端口B backlog个数

#多次执行
tenet ip地址A 端口B

netstat -nt | grep 端口B
```

![image-20230105133521259](./charter05.assets/testlisten.png)

测试结果表明, 监听tcp个数为`backlog+1`.

### 5.2.4接收连接

从listen监听队列中接收一个连接

#### API

```c
#include <sys/types.h>
#include <sys/socket.h>
int accept(int sockfd, struct sockaddr* addr, socklen_t* addrlen);
/*
sockfd 
	listen()监听的socket
addr
	接收远端socket地址
addrlen
	socket地址长度由addrlen给出

返回值
	成功返回一个新的socket, 该socket唯一的标识这个连接
	失败返回-1, 并且设置errno
	

*/
```



测试accpet()从监听队列取出异常连接(位于listen监听队列, 但是该socket已经关闭)时, socket所处状态

```c
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main( int argc, char* argv[] )
{
    if( argc <= 2 )
    {
        printf( "usage: %s ip_address port_number\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );

    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int sock = socket( PF_INET, SOCK_STREAM, 0 );
    assert( sock >= 0 );

    int ret = bind( sock, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );

    ret = listen( sock, 5 );
    assert( ret != -1 );

    //暂停20s等待客户端连接和相关操作(掉线)完成
    sleep(20);
    
    struct sockaddr_in client;
    socklen_t client_addrlength = sizeof( client );
    int connfd = accept( sock, ( struct sockaddr* )&client, &client_addrlength );
    if ( connfd < 0 )
    {
        printf( "errno is: %d\n", errno );
    }
    else
    {
        // 接受连接成功则打印出客户端的IP地址和端口号
        char remote[INET_ADDRSTRLEN ];
        printf( "connected with ip: %s and port: %d\n", 
            inet_ntop( AF_INET, &client.sin_addr, remote, INET_ADDRSTRLEN ), ntohs( client.sin_port ) );
        close( connfd );
    }

    close( sock );
    return 0;
}
```

结论:

1.  服务器运行, listen()之后, sleep(), 在sleep期间, 发起连接.会发现连接正常, 连接状态为ESTABLISHED, 说明accept只是从listen监听队列中取出连接, 而不是建立连接, 因此而不论连接处于何种状态, 因此三次握手发生在listen之后, accept之前.
2. 同样, 作为对比, 我们在建立(accept)连接后, 再退出客户端, 此时处于连接处于CLOSE_WAIT状态.

### 5.2.5 发起连接

listen调用是机器被动接受连接, 那么connect则是主动发起连接请求.

#### API

```c
#include <sys/types.h>
#include <sys/socket.h>
int connect(int sockfd, const struct sockaddr* serv_addr, socklen_t addrlen);
/*
描述
	主动发起连接请求.
参数
    sockfd
        socket()向系统申请一个套接字,作为参数
    serv_addr
        服务器监听的socket地址

    addrlen
        服务器监听socket地址长度
	
返回值
	成功返回0
	失败返回-1, 且设置errno
        ECONNREFUSED
        目标端口不存在, 连接被拒绝
        ETIMEOUT
            连接超时
	

*/
```

### 5.2.6 关闭连接

关闭连接, 实质为关闭连接对应的socket

#### API

```c
#include <unistd.h>
int close(int fd);
/*
描述
	关闭文件描述符
参数
    fd
        被关闭的fd
返回值
	成功返回0, 失败返回-1, 并设置errno
*/
```

注意:

`close`系统调用并不是立即关闭一个连接, 而是将fd的引用计数减1, 只有当fd的引用计数为0时, 才真正的关闭连接.

在多进程程序中, 一次`fork`调用默认将使父进程中打开的socket引用计数加1, 我们必须在父进程和子进程中均调用`close`才能将其关闭.

如果无论如何都要立即终止连接(不是减少引用计数), 可使用`shutdow`系统调用.

```c
#include <sys/socket.h>
int shutdown(int sockfd, int howto);

/*
sockfd
	待关闭sockfd
howto
	决定shutdown的行为
*/
```

| howto可选值 | 含义                                                         |
| ----------- | ------------------------------------------------------------ |
| SHUT_RD     | 关闭sockfd上的读一半, 应用程序不能再针对sockfd执行读操作, sockfd的接收缓冲区中的数据均被丢弃 |
| SHUT_WR     | 关闭sockfd上的写一半, sockfd的发送缓冲区中的数据会在真正关闭连接之前全部发送出去, 进程不可在对sockfd进行写操作, 即处于半关闭状态 |
| SHUT_RDWR   | 同时关闭sockfd上的读写                                       |



### 5.2.7 数据读写

#### TCP数据读写

对于文件的读写操作`read`和`write`同样适用于socket, 但是socket编程接口提供了几个专用读写系统调用, 他们增加了对于读写的控制.

##### TCP读写API

```c
#include <sys/types.h>
#include <sys/socket.h>
ssize_t recv(int sockfd, void* buf, size_t len, int flags);
/*
描述
	从sockfd接受数据, 并存于buf(大小为len字节)
	一次读取数据量肯小于发送的数据流, 因此可读取多次数据, 以便于读取完整数据.
参数
    sockfd
        读取sockfd上数据
    buf len
        指定读缓冲区的位置和大小
    flags
        参见后文,通常为0

返回值
	读取成功时, 返回读取数据字节数量, 当返回0时,读取完成或者通信对方已关闭连接
	读取失败时, 返回-1, 并设置errno
	
此外
	flags的设置, 只对send/recv的当前调用生效, 如需永久, 需使用setsockopt系统调用
*/
ssize_t send(int sockfd, const void* buf, size_t len, int flags);

```

flags参数

| 选项名        | 含义                                                         | send | recv |
| ------------- | ------------------------------------------------------------ | ---- | ---- |
| MSG_CONFIRM   | 指示数据链路层协议持续建通对方的回应, 直到得到答复, 它仅能用于SOCK_DGRAM和SOCK_RAW类型的socket | Y    | N    |
| MSG_DONTROUTE | 不查看路由表, 直接将数据发送给本地局域网内的主机, 这表示发送者确切的知道目标主机就在本地网络上 | Y    | N    |
| MSG_DONTWAIT  | 对于此次socket的操作将是非阻塞的                             | Y    | Y    |
| MSG_MORE      | 告诉内核应用程序还有更多数据要发送, 内核将等待新数据写入TCP发送缓冲区后一并发送,这样可以防止TCP发送过多小的报文段, 从而提高传输效率 | Y    | N    |
| MSG_WAITALL   | 读操作仅在读取到指定数量的字节后才返回                       | N    | Y    |
| MSG_PEEK      | 窥探该缓冲中的数据, 此次读操作不会导致这些数据被清除         | N    | Y    |
| MSG_OOB       | 发送或接收紧急数据                                           | Y    | Y    |
| MSG_NOSIGAL   | 往读端关闭管道或者socket连接中写入数据时不引发SIGPIPE信号    | Y    | N    |

例如:实现使用这些选项, 以MSG_OOB选项给应用程序提供发送和接收带外数据的方法

```c
//发送带外数据
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

int main( int argc, char* argv[] )
{
    if( argc <= 2 )
    {
        printf( "usage: %s ip_address port_number\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );

    struct sockaddr_in server_address;
    bzero( &server_address, sizeof( server_address ) );
    server_address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &server_address.sin_addr );
    server_address.sin_port = htons( port );

    int sockfd = socket( PF_INET, SOCK_STREAM, 0 );
    assert( sockfd >= 0 );
    if ( connect( sockfd, ( struct sockaddr* )&server_address, sizeof( server_address ) ) < 0 )
    {
        printf( "connection failed\n" );
    }
    else
    {
        printf( "send oob data out\n" );
        const char* oob_data = "abc";
        const char* normal_data = "123";
        send( sockfd, normal_data, strlen( normal_data ), 0 );
        // 设置发送flags
        send( sockfd, oob_data, strlen( oob_data ), MSG_OOB );
        send( sockfd, normal_data, strlen( normal_data ), 0 );
    }

    close( sockfd );
    return 0;
}
```

```c
//接收带外数据
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define BUF_SIZE 1024

int main( int argc, char* argv[] )
{
    if( argc <= 2 )
    {
        printf( "usage: %s ip_address port_number\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );

    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int sock = socket( PF_INET, SOCK_STREAM, 0 );
    assert( sock >= 0 );

    int ret = bind( sock, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );

    ret = listen( sock, 5 );
    assert( ret != -1 );

    struct sockaddr_in client;
    socklen_t client_addrlength = sizeof( client );
    int connfd = accept( sock, ( struct sockaddr* )&client, &client_addrlength );
    if ( connfd < 0 )
    {
        printf( "errno is: %d\n", errno );
    }
    else
    {
        char buffer[ BUF_SIZE ];

        memset( buffer, '\0', BUF_SIZE );
        ret = recv( connfd, buffer, BUF_SIZE-1, 0 );
        printf( "got %d bytes of normal data '%s'\n", ret, buffer );

        memset( buffer, '\0', BUF_SIZE );
        
        // 接受带外数据 Out Of Band
        ret = recv( connfd, buffer, BUF_SIZE-1, MSG_OOB );
        printf( "got %d bytes of oob data '%s'\n", ret, buffer );

        memset( buffer, '\0', BUF_SIZE );
        ret = recv( connfd, buffer, BUF_SIZE-1, 0 );
        printf( "got %d bytes of normal data '%s'\n", ret, buffer );

        close( connfd );
    }

    close( sock );
    return 0;
}

```



```bash
./testoobrecv localhost 2222	#在终端A
./testoobsend localhost 2222	#在终端B

sudo tcpdump -ntx -i eth0 port 2222

got 5 byte of normal data '123ab'
got 5 byte of oob data 'c'
got 5 byte of normal data '123'

testoobsend
实际上是
	先发normal数据: 	"123"
	再发oob数据:		"abc"
	最后发normal数据:	"123"
但是接受结果是, 我们只接收到了oob的最后一个字节的数据, 并且服务器对于正常数据的接受将被带外数据截断, 
即前一部分正常数据"123ab"和后续正常数据"123"是不能被一个recv调用全部读出的.
```



#### UDP数据读写

##### UDP读写API

```c
#include <sys/types.h>
#include <sys/socket.h>

ssize_t recvfrom(int sockfd, void* buf, size_t len, int flags, struct sockaddr* src_addr, socklen_t addrlen);
/*
描述
	recvfrom从sockfd读取数据, 将结果存于buf
参数
	sockfd 读取sockfd上的数据
	buf 接收缓冲区地址
	len 接收缓冲区大小
	flags 与send/recv相同
	src_addr 接收端的socket地址
	addrlen 接收端socket地址的大小, 用于区分Ipv4和Ipv6地址
返回值
	读取成功时, 返回读取数据字节数量, 当返回0时,读取完成或者通信对方已关闭连接
	读取失败时, 返回-1, 并设置errno
此外
	recvfrom可用于面向连接(STREAM)的socket数据读, 并且需将scr_addr和addrlen输入NULL, 因为双方已建立连接.
*/
ssize_t sendto(int sockfd, void* buf, size_t len, int flags, struct sockaddr* src_addr, socklen_t addrlen);
/*
描述
	sendto往sockfd发送数据
参数
	sockfd 读取sockfd上的数据
	buf 发送缓冲区地址
	len 发送缓冲区大小
	flags 与send/recv相同
	src_addr 接收端的socket地址
	addrlen 接收端socket地址的大小, 用于区分Ipv4和Ipv6地址
返回值
	发送成功时, 返回发送数据字节数量, 当返回0时,发送完成或者通信对方已关闭连接
	发送失败时, 返回-1, 并设置errno
此外
	sendto可用于面向连接(STREAM)的socket数据写, 并且需将scr_addr和addrlen输入NULL, 因为双方已建立连接.
*/

```

注意: 

1. 该API通用, TCP/UDP均可使用.
2. flags参数与上文send/recv通用
3. UDP需要指定src_addr, TCP则无需.

#### 通用数据读写

##### 通用读写API

```c
#include <sys/socket.h>
ssize_t recvmsg(int sockfd, struct msghdr* msg, int flags);
ssize_t sendmsg(int sockfd, struct msghdr* msg, int flags);

/*
参数 
	socket指定被操作目标
	msg为发送数据对象的数据结构
	flags sockfd属性控制
*/
struct msghdr
{
	void* msg_name;			//socket地址
	socklen_t msg_namelen;	//socket地址长度
	struct iovec* msg_iov;	//分散内存块的数组
	int msg_iovlen;			//分散内存块的数量
	void* msg_control;		//辅助数据的起始位置
	socklen_t msg_controllen;//辅助数据的大小
	int msg_flags;			//复制函数中flags参数, 并在调用过程中更新
}

struct iovec
{
	void* iovbase;//内存起始地址
	size_t iov_len;//内存长度
}

//我们称这个过程为scatter read gather write
//关于辅助数据, 将在13章来使用它, 实现进程间传递文件描述符.
//falgs参数与sen/recv中falgs并无二异
```

### 5.2.8 带外标记

实际带外数据, 有以下三个点处理:

1. 带外数据何时到达, 进程如何被通知,两种实现方式
   - IO复用产生的异常事件
   - SIGURG信号
2. 以及带外数据在数据流中的位置

#### API

```c
#include <sys/socket.h>
int sockatmark(int sockfd);
/*
描述
	socckatmark判断sockfd是否带外标记, 即下一个被读取到的数据是否带外数据
若返回1, 此时可以利用带MSG_OOB标志的recv调用, 来接收带外数据
若不是, 则返回0
参数
	sockfd 用于判断的文件描述符上下一个读取到的数据是否有带外数据
*/
```

### 5.2.9 地址信息函数

有些时候需要知道一个连接的本端和远端socket地址, 如下两个系统调用

```c
#include <sys/socket.h>

int getsockname(int sockfd, struct sockaddr* address, socklen_t* address_len);
/*
描述
	getsockname用于获取本端sockfd对应的socket地址, 并将将结果存于address和address_len中
参数
	sockfd 本端文件描述符
	address 用于返回本段地址信息
	address_len 用于返回本端地址信息的数据结构的长度, 用于区分Ipv4和Ipv6, 以及UNIX域地址
返回值
	成功返回0
	失败-1+errno
*/

int getpeername(int sockfd, struct sockaddr* address, socklen_t* address_len);
/*
描述
	getpeername用于获取对应远端sockfd对应的socket地址, 并将将结果存于address和address_len中
参数
	sockfd 本端文件描述符
	address 用于返回本段地址信息
	address_len 用于返回本端地址信息的数据结构的长度, 用于区分Ipv4和Ipv6, 以及UNIX域地址
返回值
	成功返回0
	失败-1+errno
*/
//address address_len 兼容性, 二者同时使用

```

### 5.2.10socket选项

如果说`fcntl`系统调用是控制文件描述符属性通用POSIX方法, 那么下面两个系统调用则是专门用来读取和设置socket文件描述符属性方法

```c
#include <sys/socket.h>

int getsockopt(int sockfd, int level, int opotion_name, void* option_value, socklen_t* restrict option_len);
int getsockopt(int sockfd, int level, int opotion_name, void* option_value, socklen_t option_len);

/*
描述
	专门用于设置socket文件描述符的属性方法
参数
    socket 指定的socket
    level 哪个协议选项, 如IPv4, IPv6, TCP等
    optionname 指定选项名字

	option_value option_len 联合使用, 不同的选项有不同的值
返回值
	成功时返回0 失败返回-1 且设置errno

*/
```

| level                      | option_name      | 数据类型    | 说明                                                         |
| -------------------------- | ---------------- | ----------- | ------------------------------------------------------------ |
| SOL_SOCKET(通用socket选项) | SO_DEBUG         | int         | 打开调试信息                                                 |
|                            | SO_REUSEADDR     | int         | 重用本地地址                                                 |
|                            | SO_TYPE          | int         | 获取socket类型                                               |
|                            | SO_ERROR         | int         | 获取并清除socket错误状态                                     |
|                            | SO_DONTROUTE     | int         | 不查看路由表直接将数据发送给本地局域网内主机.含义和send系统调用的MSG_DONTROUTE标志类似 |
|                            | SO_RCVBUF        | int         | TCP接收缓冲区大小                                            |
|                            | SO_SNDBUF        | int         | TCP发送缓冲区大小                                            |
|                            | SO_KEEPALIVE     | int         | 发送周期性保活报文以维持连接                                 |
|                            | SO_OOBINLINE     | int         | 接收到的带外数据将存留在普通数据的输入队列(在线存留), 此时我们不能使用带MSG_OOB标志的读操作来存取带外数据(而应该像普通数据那样读取带外数据) |
|                            | SO_LINGER        | linger      | 若有数据发送,则延迟关闭                                      |
|                            | SO_RCVLOWAT      | int         | TCP接收缓冲区低水位标志                                      |
|                            | SO_SNDLOWAT      | int         | TCP发送缓冲区低水位标志                                      |
|                            | SO_RCVTIMEO      | timeval     | 接收数据超时                                                 |
|                            | SO_SNDTIMEO      | timeval     | 发送数据超时                                                 |
| IPPROTO_IP                 | IP_TOS           | int         | 服务类型                                                     |
|                            | IP_TTL           | int         | 存活时间                                                     |
| IPPROTO_IPV6               | IPV6_NEXTHOP     | socaddr_in6 | 下一跳ip地址                                                 |
|                            | IPV6_RECVPKTINFO | int         | 接收分组信息                                                 |
|                            | IPV6_DONTFAG     | int         | 禁止分片                                                     |
|                            | IPV6_RECVTCLASS  | int         | 接收通信类型                                                 |
| IPROTO_TCP                 | TCP_MAXSEG       | int         | TCP最大报文段长度                                            |
|                            | TCP_NODELAY      | int         | 禁止Nagle算法                                                |

> 值得提出的是, 
>
> 对于服务器而言, 有部分socket选项只能在调用listen系统调用之前针对socket设置才有效, 这是因为连接socket只能由accept调用返回, 而accept从listen监听队列中接收的连接至少已经完成了TCP三次握手的前两个步骤(因为listen的监听队列中的连接至少[已经进入SYS_RECV状态), 这说明服务器已经往被接收连接上发出的TCP同步报文段.
>
> 但是有的socket设置选项应该在TCP同步报文段设置, 例如TCP最大报文段长度.
>
> 对于这种情况, Linux给开发人员的提供的解决方案是:
>
> 对监听socket设置这些选项, 那么accept返回的连接socket将自动继承这些选项, 这些选项包含:`SO_DEBUG`,`SO_DONTROUTE`,`SO_KEEPALIVE`, `SO_LINGER`, `SO_OOBINLINE`, `SO_RCVBUF`, `SO_RCVLOWAT`, `SO_SNDBUF`, `SO_SNDLOWAT`, `TCP_MAXSEG`, `TCP_NODELAY`.
>
> 而对于客户端:
>
> 这些socket设置选项应该在调用connect函数之前设置, 因为connect调用成功返回之后, TCP三次握手已经完成.

#### SO_REUSEADDR选项

服务器程序通过设置socket选项来强制使用被处于TIME_WAIT状态的连接占用的socket, 即快速重用处于TIME_WAIT状态的socket.

此外可以修改,内核参数`/proc/sys/net/ipv4/tcp_tw_recycle`, 从而使TCP连接根本不进入TIME_WAIT状态, 从而快速重用本地socket地址.

```c
int sock = socket(PF_INET, SOCK_STREAM, O);
assert(sock>=0);
int reuse = 1;
setsockopt(sock, SOl_socket, SO_REUSEADDR, &reuse, sizeof(reuse));

struct sockaddr_in address;
bzero(&address, sizeof(address));
address.sin_family = AF_INF;
inet_pton(AF_INET, ip, &address.sin_addr);
address.sin_port = hton(port);
int ret = bind(sock, (struct sockaddr* )&address, sizeof(address));
```

#### SO_RCV(SND)BUF

设置TCP接收(发送)缓冲区大小, 其默认值最小为256B(2048B), 当使用setsockopt设置时, 系统自动设置为其默认值的倍数, 且不小于所设置的值.(设置过小会自动设置为默认值), 系统这样做的目的是, 确保一个TCP连接有足够的空闲缓冲区来处理拥塞(就如同快速重传算法就期望TCP接收缓冲区至少容纳4个大小为SMSS的TCP报文段).

此外可以修改内核参数, `/pro/sys/net/ipv4/tvp_rmem`和`/proc/sys/net/ipv4/tcp_wmem`来强制TCP接收和发送缓冲区没有大小限制.

```c
//修改发送缓冲区
#include <sys/socket.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#define BUFFER_SIZE 512

int main( int argc, char* argv[] )
{
    if( argc <= 3 )
    {
        printf( "usage: %s ip_address port_number send_bufer_size\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );

    struct sockaddr_in server_address;
    bzero( &server_address, sizeof( server_address ) );
    server_address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &server_address.sin_addr );
    server_address.sin_port = htons( port );

    int sock = socket( PF_INET, SOCK_STREAM, 0 );
    assert( sock >= 0 );

    int sendbuf = atoi( argv[3] );
    int len = sizeof( sendbuf );
    setsockopt( sock, SOL_SOCKET, SO_SNDBUF, &sendbuf, sizeof( sendbuf ) );
    getsockopt( sock, SOL_SOCKET, SO_SNDBUF, &sendbuf, ( socklen_t* )&len );
    printf( "the tcp send buffer size after setting is %d\n", sendbuf );

    if ( connect( sock, ( struct sockaddr* )&server_address, sizeof( server_address ) ) != -1 )
    {
        char buffer[ BUFFER_SIZE ];
        memset( buffer, 'a', BUFFER_SIZE );
        send( sock, buffer, BUFFER_SIZE, 0 );
    }

    close( sock );
    return 0;
}
```

```c
//修改接收缓冲区
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define BUFFER_SIZE 1024

int main( int argc, char* argv[] )
{
    if( argc <= 3 )
    {
        printf( "usage: %s ip_address port_number receive_buffer_size\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );

    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int sock = socket( PF_INET, SOCK_STREAM, 0 );
    assert( sock >= 0 );
    int recvbuf = atoi( argv[3] );
    int len = sizeof( recvbuf );
    setsockopt( sock, SOL_SOCKET, SO_RCVBUF, &recvbuf, sizeof( recvbuf ) );
    getsockopt( sock, SOL_SOCKET, SO_RCVBUF, &recvbuf, ( socklen_t* )&len );
    printf( "the receive buffer size after settting is %d\n", recvbuf );

    int ret = bind( sock, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );

    ret = listen( sock, 5 );
    assert( ret != -1 );

    struct sockaddr_in client;
    socklen_t client_addrlength = sizeof( client );
    int connfd = accept( sock, ( struct sockaddr* )&client, &client_addrlength );
    if ( connfd < 0 )
    {
        printf( "errno is: %d\n", errno );
    }
    else
    {
        char buffer[ BUFFER_SIZE ];
        memset( buffer, '\0', BUFFER_SIZE );
        while( recv( connfd, buffer, BUFFER_SIZE-1, 0 ) > 0 ){}
        close( connfd );
    }

    close( sock );
    return 0;
}

```

#### SO_RCVLOWAT/SO_SNDLOWAT

`SO_RCVLOWAT/SO_SNDLOWAT`一般被IO复用系统调用用来判断socket是否可读可写.

当TCP接收缓冲区中可读数据大于其低水位标记时, IO复用系统调用将通知应用程序可以从对应的socket上读取数据.

当TCP发送缓冲区中的空闲大于低水位标志时, IO复用系统调用将通知应用程序可以往socket上写入数据.

默认情况下, TCP接收缓冲区标记和TCP发送缓冲区的低水位标志均为1.

#### SO_LINGER

`SO_LINGER`用于控制close系统调用在关闭TCP连接时的行为, 默认情况下, 当我们使用close系统调用来关闭一个socket时, close立即返回, TCP模块负责把该socket对应的TCP缓冲区中残留的数据发送给对方,

```c
#include <sys/socket.h>
struct linger
{
	int l_onoff;//开启(非0) 关闭(0)
	int l_linger;//滞留时间

}
```

根据linger结构体的两个成员变量不同的值, close系统调用有以下三种行为

1. l_onoff = 0, 此时SO_LINGER无效, close用默认行为关闭socket

2. l_onoff != 0, l_linger = 0, 此时close系统调用立即返回, TCP模块将丢弃被关闭的socket对应的TCP发送缓冲区遗留数据, 同时给对方发送复位报文段. 因此,这种情况给服务器提供了一种终止异常的方法.

3. l_onoff != 0, l_linger >0, 此时取决于两个条件:被关闭对应的socket中是否含有未发送数据和socket是否为阻塞的

   1. 对于阻塞socket, close将等待一段长为l_linger的时间, 直到TCP模块发送完所有残留数据并得到对方确认.如果这段时间内TCP模块没发送完所有暗流数据并得到对方的确认, 那么close返回-1, 并设置errno为EWOULDBLOCK
   2. 对于非阻塞socket, close将返回, 此时我们需要根据返回值和errno判断数据是否发送完毕.

   

## 5.3网络信息API

socket核心 IP+ port.

### gethostbyname/gethostbyaddr

```c
#include <netdb.h>
struct hostent* gethostbyname(const char* name);
/*
描述
	通过主机名或者域名查找ip地址
	/etc/hosts->DNS
参数
	name 主机名或者域名
返回值
	成功 返回hostent结构体指针
	失败 返回NULL
*/


struct hostent* gethostbyaddr(const void* addr, size_t len, int type);
/*
描述
	通过ip获取主机完整信息 
	/etc/hosts->DNS
参数
	addr IP地址结构
	len 指定sockaddr的长度
	type 为IP地址类型 AF_INET和AF_INET6
返回值
	成功 返回hostent结构体指针
	失败 返回NULL
*/
struct hostent
{
  char *h_name;			/* 主机名*/
  char **h_aliases;		/* Alias list. 服务别名列表, 可能有多个 */
  int h_addrtype;		/* Host address type.  地址族*/
  int h_length;			/* Length of address.  地址长度*/
  char **h_addr_list;		/* List of addresses from name server.  按网络字节序的IP地址列表*/
}

```

```c
//使用gethostbyname和gethostbyaddr
int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        printf("非法输入\n");
        exit(0);
    }
    char* name = argv[1];

    struct hostent *hostptr{};

    hostptr = gethostbyname(name);
    if (hostptr == nullptr)
    {
        printf("输入存在错误 或无法获取\n");
        exit(0);
    }

    printf("Official name of hostptr: %s\n", hostptr->h_name);

    char **pptr;
    char inet_addr[INET_ADDRSTRLEN];

    printf("Alias list:\n");
    for (pptr = hostptr->h_aliases; *pptr != nullptr; ++pptr)
    {
        printf("\t%s\n", *pptr);
    }

    switch (hostptr->h_addrtype)
    {
        case AF_INET:
        {
            printf("List of addresses from name server:\n");
            for (pptr = hostptr->h_addr_list; *pptr != nullptr; ++pptr)
            {
                printf("\t%s\n",
                        inet_ntop(hostptr->h_addrtype, *pptr, inet_addr, sizeof(inet_addr)));
            }
            break;
        }
        default:
        {
            printf("unknow address type\n");
            exit(0);
        }
    }
    return 0;
}
c
/*
./run baidu.com
Official name of hostptr: baidu.com
Alias list:
List of addresses from name server:
	39.156.69.79
	220.181.38.148
*/
```



### getservbyname/getservbyport

```c
#include <netdb.h>
struct servent* getservbyname(const char* name, const char* proto);
/*
描述
	根据名称获取某个服务的完整信息
返回值
    name 表示服务名称
    proto	传 tcp 表示获取tcp类型服务
            传 udp 表示获取udp类型服务
            传 null 表示获取所有类型
		
函数返回	指针
*/

// 根据端口号获取服务信息
struct servent* getservbyport(int port, const char* proto);

struct servent
{
	char* s_name; /* 服务名称*/
	char ** s_aliases; /* 服务的别名列表*/
	int s_port; /* 端口号*/
	char* s_proto; /* 服务类型, 通常为TCP或UDP*/
}
```

> 二者实际是通过读取/etc/service文件获取服务信息
>
> 这两个函数都是不可重入, 但是netdb.h也给出了可重入版本, 正如Linux可重入版本命名规则那样, 这些函数的函数名是在函数名尾部加上_r(re-entrant)

### getaddrinfo

```c
#include <netdb.h>

int getaddrinfo(const char* hostname, const char* service, const struct addrinfo* hints, struct addrinfo** result)
/*
描述
	内部使用的gethostbyname 和 getserverbyname
参数
    hostname 可以用于接收主机名, 也可以用来接收字符串表示的IP地址(IPv4采用点分十进制, IPv6采用十六进制字符串)
    service 可以用于接收服务名, 也可以接收字符串表示的十进制端口号
    hints参数 对getaddrinfo的输出进行更准确的控制, 可以设置为NULL, 允许反馈各种有用的结果
    result 指向一个链表, 用于存储getaddrinfo的反馈结果

*/
struct addrinfo
{
	int ai_flags;
	int ai_family;
	int ai_socktype; /* 服务类型, SOCK_STREAM或者SOCK_DGRAM*/
	int ai_protocol;
	socklen_t ai_addrlen;/* socket地址ai_addr的长度 */
	char* ai_canonname; /* 主机的别名*/
	struct sockaddr* ai_addr; /* 指向socket地址*/
	struct addrinfo* ai_next; /* 指向下一个sockinfo结构对象*/
}c
/*
ai_protocol	指具体的网络协议, 与socket系统调用第三个参数相同, 通常设置为0
ai_flags成员按下表中的标志按位或
*/
// 需要手动的释放堆内存
void freeaddrinfo(struct addrinfo* res);
```

| 选项       | 含义                                                         |
| ---------- | ------------------------------------------------------------ |
| AI_PASSIVE | 在hints此参数中设置,表示调用者是否会将取得的socket地址用于被动打开 |

内容过多, 暂时不写.

### getnameinfo

```c
#include <netdb.h>
// host 存储返回的主机名
// serv存储返回的服务名

int getnameinfo(const struct sockaddr* sockaddr, socklen_t addrlen, char* host, socklen_t hostlen, char* serv
	socklen_t servlen, int flags);

//将errno转为可读字符串
#include <netdb.h>
const char* gai_strerror(int errno )
```


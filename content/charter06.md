# 第6章 高级I/O函数

Linux有很多高级的I/O函数, 他们并不像Linux基础I/O函数(如open read)那么常用, 但是在一些特定情况下很实用, 将他们分为三类

1. 用于创建文件描述符的函数: pipe, dup/dup2
2. 用于读写数据的函数: readv/writev, sendfile, mmap/munmap, splice, tee
3. 用于控制io行为和属性的函数: fcntl

## 6.1用于创建文件描述符的函数

### 6.1.1 pipe

```c
#include <unistd.h>
int pipe(int fd[2]);

/*
参数
	fd[2] fd[0] 只限于读 fd[1] 只限于写
	二者分别构成管道两端, 如果不设置非阻塞fd, 那么read空管道会阻塞, write满管道阻塞
	同时, 如果管道的写端文件描述符fd[1]的引用计数减少至0, 即没有任何进程需要往管道中写入数据, 则针对该管道的读端文件描述符fd[0]的read操作将返回0, 即读到了文件结束标记(End Of File, EOF).
	反之, 如果管道的读端文件描述符fd[0]的引用计数减少至0, 即没有任何进程需要从管道读取数据, 则针对该管道的写端文件描述符fd[1]的write操作将失败, 并引发SIGPIPE信号.
	一荣俱荣, 一损俱损.
	
返回值
	失败返回-1, 并设置errno
	成功返回0
	
TCP字节流, 大小取决于对方的接收通知窗口, 和本端的拥塞窗口大小
管道字节流, 本身具有容量限制, 2^16B,65536B, 可以通过fcntl修改
*/

//此外, socket API有一个socketpair函数, 它也能够方便的创建双向管道.
#include <sys/types.h>
#include <sys/socket.h>
int socketpair(int domain, int type, int protocol, int fd[2]);
/*
参数
	前三个参数与socket()系统调用相同, 但domain只能使用UNIX本地域协议族AF_UNIX, 因为我们仅能在本地使用这个双向管道, 最后一个参数和pipe系统调用参数一样, 只不过sockpair创建的这对文件描述符都是既可以读又可写.
返回值
	成功返回0
	失败返回-1, 并设置errno
	
*/


```

### 6.1.2 dup dup2

重定向标准输入/标准输出.

```c
#include <unistd.h>
int dup(int fd);
int dup2(int fd1, int fd2);

/*
dup
	创建与原有文件描述符指向相同文件 管道或者网络连接
	并且dup返回的总是取系统当前可用最小的整数值.
dup2
	功能与dup相同, 但是dup2返回的新文件描述符不小于fd2
此外
	dup和dup2创建的文件描述符并不继承原文件描述符的属性, 比如close-on-exec和non-blocking
*/

#define STDIN_FILENO 0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2
```

```c
//CGI服务器Common Gateway Interface
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main( int argc, char* argv[] )
{
    if( argc <= 2 )
    {
        printf( "usage: %s ip_address port_number\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );

    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int sock = socket( PF_INET, SOCK_STREAM, 0 );
    assert( sock >= 0 );

    int ret = bind( sock, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );

    ret = listen( sock, 5 );
    assert( ret != -1 );

    struct sockaddr_in client;
    socklen_t client_addrlength = sizeof( client );
    int connfd = accept( sock, ( struct sockaddr* )&client, &client_addrlength );
    if ( connfd < 0 )
    {
        printf( "errno is: %d\n", errno );
    }
    else
    {
        //将标准输出关闭, 然后将当前连接的socket fd复制, 由于dup总是选取当前文件描述符中最小的, 因此, STDOUT_FILENO的句柄将被fd替代, 此时进程的标准输出被重定位至fd.
    	//返回系统最小可用, 标准输出是1c
        close( STDOUT_FILENO );
        dup( connfd );
        printf( "abcd\n" );
        close( connfd );
    }

    close( sock );
    return 0;
}

```

## 6.2用于读写数据的函数

### 6.2.1 readv writev

readv函数将数据从文件描述符读到分散的内存块, 即分散读;

writev函数将分散的内存块一并写入文件描述符中, 即集中写.

```c
#include <sys/uio.h>
ssize_t readv(int fd, const struct iovec* vector, int count);
ssize_t writev(int fd, const struct iovec* vector, int count);
/*
描述
	readv分散读, 
	writev集中写, 提前将需要写入的缓冲区准备好, 有几个缓冲区, 申请多大的vector数组, 最后集中写入.
参数
	fd 目标文件描述符
	vector 读入内存, 或者写入数据内存
	count vector长度
	
返回值
	成功0, 失败-1+errno
*/
```



```c
//http服务器集中写
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#define BUFFER_SIZE 1024
//定义两种HTTP状态码和状态信息
static const char* status_line[2] = { "200 OK", "500 Internal server error" };

int main( int argc, char* argv[] )
{
    if( argc <= 3 )
    {
        printf( "usage: %s ip_address port_number filename\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );
    
    //将目标文件作为三个参数传入
    const char* file_name = argv[3];

    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int sock = socket( PF_INET, SOCK_STREAM, 0 );
    assert( sock >= 0 );

    int ret = bind( sock, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );

    ret = listen( sock, 5 );
    assert( ret != -1 );

    struct sockaddr_in client;
    socklen_t client_addrlength = sizeof( client );
    int connfd = accept( sock, ( struct sockaddr* )&client, &client_addrlength );
    if ( connfd < 0 )
    {
        printf( "errno is: %d\n", errno );
    }
    else
    {
        //用于保存HTTP应答状态行, 头部字段和一个空行的缓冲区
        char header_buf[ BUFFER_SIZE ];
        memset( header_buf, '\0', BUFFER_SIZE );
        //用于存放目标文件的应用程序缓冲
        char* file_buf;
        //用于获取目标文件的属性, 比如是否为目录 文件大小等
        struct stat file_stat;
        //记录文件是否为有效文件
        bool valid = true;
        //缓冲区head_buf目前已经使用了多少字节的空间
        int len = 0;
        if( stat( file_name, &file_stat ) < 0 )//目标文件不存在
        {
            valid = false;
        }
        else
        {
            //目标文件是一个目录
            if( S_ISDIR( file_stat.st_mode ) )
            {
                valid = false;
            }
            //当前用户读取目标文件的权限
            else if( file_stat.st_mode & S_IROTH )
            {
                //动态分配缓冲区file_buf, 并指定大小为目标文件大小
                //file_stat.st_size+1, 然后将目标文件读入file_buf
                int fd = open( file_name, O_RDONLY );
                file_buf = new char [ file_stat.st_size + 1 ];
                memset( file_buf, '\0', file_stat.st_size + 1 );
                if ( read( fd, file_buf, file_stat.st_size ) < 0 )
                {
                    valid = false;
                }
            }
            else
            {
                valid = false;
            }
        }
        //如果目标文件有效, 则发送正常的HTTP应答
        if( valid )
        {
            //下面这部分内容将HTTP应答的状态行, "Content-Length"头部字段和一个空行依次加入header_buf
            ret = snprintf( header_buf, BUFFER_SIZE-1, "%s %s\r\n", "HTTP/1.1", status_line[0] );
            len += ret;
            ret = snprintf( header_buf + len, BUFFER_SIZE-1-len, 
                             "Content-Length: %d\r\n", file_stat.st_size );
            len += ret;
            ret = snprintf( header_buf + len, BUFFER_SIZE-1-len, "%s", "\r\n" );
            //利用writev将header_buf和file_buf的内容一并写出
            struct iovec iv[2];
            iv[ 0 ].iov_base = header_buf;
            iv[ 0 ].iov_len = strlen( header_buf );
            iv[ 1 ].iov_base = file_buf;
            iv[ 1 ].iov_len = file_stat.st_size;
            ret = writev( connfd, iv, 2 );
        }
        //如果目标文件无效, 则通知客户端服务器发生了"内部错误"
        else
        {
            ret = snprintf( header_buf, BUFFER_SIZE-1, "%s %s\r\n", "HTTP/1.1", status_line[1] );
            len += ret;
            ret = snprintf( header_buf + len, BUFFER_SIZE-1-len, "%s", "\r\n" );
            send( connfd, header_buf, strlen( header_buf ), 0 );
        }
        close( connfd );
        delete [] file_buf;
    }

    close( sock );
    return 0;
}

```

### 6.2.2sendfile

sendfile在两个文件描述符之间直接传递数据(完全在内核中操作), 从而避免了内核缓冲区和用户缓冲区之间的数据拷贝, 效率很高, 也叫零拷贝.

```c
#include <sys/sendfile.h>
ssize_t sendfile(int out_fd, int in_fd, off_t* offset, size_t count);
/*
描述
	sendfile实现out_fd->in_fd数据流转的零拷贝, 两个文件描述符数据流转完全是在内核中实现的.
参数
	out_fd 待读入内容的文件描述符, 必须是socket
	in_fd 待读出内容文件描述符, 必须支持类似mmap函数的文件描述符, 即指向真实文件, 不能是socket和管道
	offset 指定从读入文件流中的哪个位置开始读, 如果为空, 则使用读入文件流的默认起始位置
	count 指定out_fd和in_fd之间传输的字节数
	
此外
	其特点是, 无需分配用户空间缓冲, 也没有执行读取文件的操作, 而实现了文件的发送.
	可以说, sendfile是为网络编程而生的系统调用.
*/
```



```c
//sendfile传输文件
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/sendfile.h>

int main( int argc, char* argv[] )
{
    if( argc <= 3 )
    {
        printf( "usage: %s ip_address port_number filename\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );
    const char* file_name = argv[3];
	
    //申请需转发的文件描述符
    int filefd = open( file_name, O_RDONLY );
    assert( filefd > 0 );
    struct stat stat_buf;
    //提取文件信息
    fstat( filefd, &stat_buf );

    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int sock = socket( PF_INET, SOCK_STREAM, 0 );
    assert( sock >= 0 );

    int ret = bind( sock, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );

    ret = listen( sock, 5 );
    assert( ret != -1 );

    struct sockaddr_in client;
    socklen_t client_addrlength = sizeof( client );
    int connfd = accept( sock, ( struct sockaddr* )&client, &client_addrlength );
    if ( connfd < 0 )
    {
        printf( "errno is: %d\n", errno );
    }
    else
    {
        // 将文件filefd转发给socket连接connfd
        sendfile( connfd, filefd, NULL, stat_buf.st_size );
        close( connfd );
    }

    close( sock );
    return 0;
}

```

### 6.2.3mmap munmap

```c
#include <sys/mman.h>
void* mmap(void* start, size_t length, int port, int flags, int fd, off_t offset);
int munmap(void* start, size_t length);
/*
描述
	mmap函数用于申请一段内存空间, 这段内存可以作为进程间通信的共享内存, 也可以将文件直接映射到其中.
	munmap则用于释放mmap申请的空间.
参数
	start 内存起始地址
	length 内存段长度
	port 内存段访问权限
	flags 控制内存段内容被修改后程序的行为
	fd 被映射的文件的文件描述符, 通常通过open获取
	offset 设置从文件的何处开始映射(可以不读入整个文件)
	
返回值
	成功返回指向目标内存区域的指针
	失败返回MAP_FAILED((void*)-1), 并设置errno
*/

/*

返回值
	成功0, 失败-1+errno
*/
```

port按位或

| 位         | 含义             |
| ---------- | ---------------- |
| PROT_READ  | 内存段可读       |
| PROT_WRITE | 内存段可写       |
| PROT_EXEC  | 内存段可执行     |
| PROT_NONE  | 内存段不能被访问 |

flags按位或(其中MAP_SHARED和MAP_PRIVATE是互斥的, 不能同时指定)

| 常用值        | 含义                                                         |
| ------------- | ------------------------------------------------------------ |
| MAP_SHARED    | 在进程间共享这段内存, 对该内存段的修改将反映到被映射的文件中.它提供了进程间共享内存的posix方法 |
| MAP_PRIVATE   | 内存段为调用进程私有, 对该内存段的修改不会反映到被映射的文件中 |
| MAP_ANONYMOUS | 这段内存不是从文件映射而来的, 其内容被初始化为全0, 这种情况下, mmap函数的最后两个参数被忽略 |
| MAP_FIXED     | 内存段必须位于start参数指定的地址处, start必须是内存页面大小(4096字节)的整数倍 |
| MAP_HUGETLB   | 按照"大内存页面"来分配内存空间."大内存页面"的大小可通过/proc/meminfo文件来查看 |

### 6.2.4 splice

`移动数据`

```c
#include <fcntl.h>
ssize_t splice(int fd_in, loff_t* off_in, int fd_out, loff_t* off_out, size_t len, unsigned int flags);

/*
描述
	splice用于在两个文件描述符之间`移动数据`, 零拷贝操作.
参数
	fd_in 输入
		若fd_in为管道文件描述符, 那么off_in必须设为NULL
		若fd_in不为管道文件描述符, 若off_in不为NULL, 则偏移,否则从当前偏移位置开始读取数据
	off_in 非空指定何处偏移, 空则当前位置为偏移
	
	fd_out 输出
	off_out 非空指定何处偏移, 空则当前位置为偏移
	
	len 移动数据长度
	flags 控制数据如何移动

返回值
	成功, 移动长度字节数, 正整数和0
	失败, 返回-1, 设置errno
	
此外
	使用splice fd_in或者fd_out必须有一个是管道文件描述符
*/
```

flags参数

| 常用值          | 含义                                                         |
| --------------- | ------------------------------------------------------------ |
| SPLICE_MOVE     | 如果合适的话, 按整页内存移动数据. 这只是给内核一个提示, 不过, 它的实现存在bug, 所以子 内核2.6.21后, 它实际无任何效果. |
| SPLICE_NONBLOCK | 非阻塞splice操作, 但是实际效果还会受到文件描述符本身阻塞状态影响. |
| SPLICE_MORE     | 给内核一个提示, 后续的splice调用将读取更多数据               |
| SPLIC_F_GIFT    | 对splice没有效果                                             |

splice可能产生的errno

| 错误   | 含义                                                         |
| ------ | ------------------------------------------------------------ |
| EBADF  | 参数文件描述符有错                                           |
| EINVAL | 目标系统不支持splice, 或者目标文件以追加方式打开, 或者两个文件描述符都不是管道文件描述符, 或者某个offset参数被用于不支持随机访问的设备(比如字符设备). |
| ENOMEM | 内存不够                                                     |
| ESPIPE | 参数fd_in(或fd_out)是管道文件描述符, 而off_in(或off_out)不为NULL. |

```c
#include <sys/socket.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(int argc, char* argv[]) {
	if(argc <= 2) {
		printf("Wrong number of parameters!");
		return 1;
	}

	char* ip = argv[1];
	int port = atoi(argv[2]);

	int sockfd = socket(PF_INET, SOCK_STREAM, 0);
	assert(sockfd >= 0);

	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_port = htons(port);
	inet_pton(AF_INET, ip, &address.sin_addr);

	int ret = bind(sockfd, (struct sockaddr*)&address, sizeof(address));
	assert(ret != -1);

	ret = listen(sockfd, 5);
	assert(ret != -1);

	struct sockaddr_in client;
	socklen_t client_length = sizeof(client);
	int connfd = accept(sockfd, (struct sockaddr*)&client, &client_length);
	if(connfd < 0) {
		printf("error\n");
		return 1;
	} else {
		
		// 用管道连接起 socket的输入和输出 实现0拷贝 
		
		int pipefd[2];
		assert(ret != -1);
		ret = pipe(pipefd);
		ret = splice(connfd, NULL, pipefd[1], NULL, 32768, SPLICE_F_MORE | SPLICE_F_MOVE);
		assert(ret != -1);
		ret = splice(pipefd[0], NULL, connfd, NULL, 32768, SPLICE_F_MORE | SPLICE_F_MOVE);
		assert(ret != -1);
		close(connfd);
	}

	close(sockfd);
	return 0;
}
```



### 6.2.5 tee

`复制数据`

```c
#include <fcntl.h>
ssize_t tee(int fd_in, int fd_out, size_t len, unsigned int flags);

/*
描述
	tee与splice相似, 实现在两个管道之间`复制数据`, 即零拷贝操作, 但是tee的两个输入输出文件描述符必须为管道文件描述符.tee与splice相似, 实现在两个管道之间复制数据, 即零拷贝操作, 但是tee的两个输入输出文件描述符必须为管道文件描述符.
参数
	fd_in 输入管道
	fd_out 输出管道
	len 两个文件描述符之间复制的数据量
	flags 
返回值
	成功返回复制数据量
	失败-1+errno
此外
	fd_in fd_out 必须均为管道fd

*/
```

```c
//利用tee和splice实现Linux下实现tee程序的基本功能, 即同时输出数据到终端和文件的功能
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
/*
STDIN_FILENO(写入的信息) -> pipefd_stdout[1](写入)		定向	splice
pipefd_stdout[0](读) -> pipefd_file[1](写)		  	复制	tee

pipefd_file[0](读) -> filefd(写) 				定向		splice
pipefd_stdout[0](读) -> STDOUT_FILENO(写)		定向		splice

*/



int main( int argc, char* argv[] )
{
	if ( argc != 2 )
	{
		printf( "usage: %s <file>\n", argv[0] );
		return 1;
	}
	int filefd = open( argv[1], O_CREAT | O_WRONLY | O_TRUNC, 0666 );
	assert( filefd > 0 );

    // 输出到标准输出
	int pipefd_stdout[2];
        int ret = pipe( pipefd_stdout );
	assert( ret != -1 );

    // 输出到文件
	int pipefd_file[2];
        ret = pipe( pipefd_file );
	assert( ret != -1 );
	
    // 将标准输入内容输入管道 pipefd_stdout
	ret = splice( STDIN_FILENO, NULL, pipefd_stdout[1], NULL, 32768, SPLICE_F_MORE | SPLICE_F_MOVE );
	assert( ret != -1 );
    // 将管道 pipefd_stdout的输出复制到管道pipefd_file的输入端
	ret = tee( pipefd_stdout[0], pipefd_file[1], 32768, SPLICE_F_NONBLOCK ); 
	assert( ret != -1 );
    // 将管道pipefd_file的输出定向到文件描述符filefd上, 从而将标准输入的内容写入文件
	ret = splice( pipefd_file[0], NULL, filefd, NULL, 32768, SPLICE_F_MORE | SPLICE_F_MOVE );
	assert( ret != -1 );
    // 将管道pipefd_stdout的输出定向到文件描述符标准输出上, 其内容和写入文件的内容一致
	ret = splice( pipefd_stdout[0], NULL, STDOUT_FILENO, NULL, 32768, SPLICE_F_MORE | SPLICE_F_MOVE );
	assert( ret != -1 );

	close( filefd );
        close( pipefd_stdout[0] );
        close( pipefd_stdout[1] );
        close( pipefd_file[0] );
        close( pipefd_file[1] );
	return 0;
}

```



## 6.3用于控制io行为和属性的函数

控制文件描述符各种属性.

```c
#include <fcntl.h>
int fcntl(int fd, int cmd, ...);
```

![image-20230226163858975](./charter06.assets/fcntl.png)

```c
// 将文件描述符设置为非阻塞
int setnonblocking(int fd)
{
	int old_option = fcntl(fd, F_GETFL);
	int new_option = old_option | O_NONBLOCK;
	fcntl(fd, F_SETFL, new_option);
	return old_option;

}
```

此外，SIGIO和SIGURG这两个信号与其他Linux信号不同，它们必须与某个文件描述符相关联方可使用：当被关联的文件描述符可读或可写时，系统将触发SIGIO信号；当被关联的文件描述符（而且必须是一个socket）上有带外数据可读时，系统将触发SIGURG信号。将信号和文件描述符关联的方法，就是使用fcntl函数为目标文件描述符指定宿主进程或进程组，那么被指定的宿主进程或进程组将捕获这两个信号。使用SIGIO时，还需要利用fcntl设置其`O_ASYNC`标志（异步I/O标志，不过SIGIO信号模型并非真正意义上的异步I／O模型，见第8章）。关于信号SIGURG的更多内容，我们将在第10章讨论。

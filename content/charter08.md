# 第8章 高性能服务器程序框架

服务器一般原理:

1. IO处理单元, 介绍IO处理单元的四种模型和两种高效事件处理模式
2. 逻辑单元.本章介绍逻辑单元的两种高效并发模式, 以及高效的逻辑处理方式--有限状态机
3. 存储单元, 服务器程序的可选模块, 而且其内容与网络编程本身无关

- 服务器性能提高建议

## 8.1 服务器模型

### 8.1.1 C/S模型

中心服务器负责资源发布, 转发.

客户端负责内容请求资源.

![image-20230205112427522](./charter08.assets/cs.png)

优点: 非常适合资源相对集中, 实现简单, 把持流量入口.

缺点:服务器为通信中心, 访问量过大或者故障, 影响全局.

### 8.1.2 P2P模型

点对点通信, 去中心化, 让网络上所有主机回归对等地位, 使得每台机器在消耗服务的同时, 又为其他人提供服务, 即每台机器既为服务器, 又为客户端.

![image-20230205112858850](./charter08.assets/p2p.png)

优点:资源充分利用, 自由共享, 去中心化, 不受监管

缺点:主机之间难以相互发现, 因此一般还需要设置专门的发现服务器, 当用户之间的请求过多, 网络负载将加重.

## 8.2服务器编程框架

服务器种类繁多, 但是基本框架相同, 不同之处在于逻辑处理.

![image-20230205135219728](./charter08.assets/服务器基本编程框架.png)

| 模块         | 单个服务器程序             | 服务器机群                   |
| ------------ | -------------------------- | ---------------------------- |
| IO处理单元   | 处理客户连接, 读写网络数据 | 作为接入服务器, 实现负载均衡 |
| 逻辑单元     | 业务进程或者线程           | 逻辑服务器                   |
| 网络存储单元 | 本地数据库, 文件或者缓冲   | 数据库服务器                 |
| 请求队列     | 各个单元之间的通信方式     | 各服务器之间的永久tcp连接    |

1. IO处理单元, 是服务器客户连接的模块, 它通常完成以下工作: 
   1. 对于单个服务器来说, 等待并接收新的客户连接, 接收客户数据, 将服务器响应数据返回给客户端.但是, 数据的说法不一定在IO处理单元中执行, 
   2. 对于一个服务器机群来说, IO处理单元是专门的接入服务器, 它实现负载均衡, 所有逻辑服务器种选取负荷最小的的一台来为新客户服务

2. 一个逻辑单元通常是一个进程或者线程.
   1. 对于单个服务器程序而言, 它分析并处理客户数据, 然后将结果传递给IO处理单元, 或者直接发送给客户端(具体使用哪种方式去取决于事件处理方式 ). 
   2. 对服务器机群而言, 一个逻辑单元本身就是一台逻辑服务器, 服务器通常有多个逻辑单元,, 以实现多个客户任务的并行处理.

3. 网络存储单元可以是数据库, 缓冲和文件, 甚至是一台独立服务器, 但他不是必须的.
4. 请求队列, 是各个单元之间的通信方式的抽象. IO处理单元接收到客户请求时, 需要以某种方式来通知一个逻辑单元来处理请求.同样, 多个逻辑单元同时访问一个存储单元时, 也需要采用某种机制来协调处理竞态条件. 请求队列通常被实现为`池`的一部分, 我们后面将讨论池.对于服务器机群而言, 请求队列是各台服务器之间预先建立的, 静态的, 永久的TCP连接. 对于这种TCP连接既能提高服务器之间交换数据的效率, 因为它避免了动态建立TCP导致的额外系统开销.

## 8.3 IO模型

IO阻塞模型

1. 阻塞IO: 请求事件若未及时触发, 则挂起, 等待事件触发.
2. 非阻塞IO: 请求事件若未及时触发, 则返回, 返回值和出错情况, 但errno不同.
   1. accept, send, recv, 若事件未发生时, errno通常设置为`EAGAIN`(再来一次)或者`EWOULDBLOCK`(期望阻塞)后者`EINPROGRESS`(在处理中).

很显然, 我们只有在事件发生的情况下操作非阻塞IO, 才能提高程序的效率. 因此, 非阻塞IO必须和其他的IO通知机制一起使用, 比如IO服用和SIGIO.

同步IO: IO的读写操作发生在IO事件触发之后, 很显然, 阻塞IO, IO复用和信号驱动IO都是同步模型.

POSIX关于同步IO和异步IO:

1. 同步IO模型: 要求用户代码自行执行IO操作(将数据从内核缓冲区读入用户缓冲区, 或将数据从用户缓冲区写入内核缓冲区), 即同步IO向应用程序通知的是IO`就绪事件`.
2.   同步IO模型: 要求内核来执行IO操作(数据在内核缓冲区和用户缓冲区之间的移动是由内核在后台完成的), 即异步IO向应用程序通知的是IO`完成事件`.

| IO模型    | 读写操作和阻塞阶段                                           |
| --------- | ------------------------------------------------------------ |
| 阻塞IO    | 程序阻塞于读写函数                                           |
| IO复用    | 程序阻塞于IO复用系统调用, 但可以同时监听多个IO事件.对于IO本身的读写操作是非阻塞的 |
| SIGIO信号 | 信号触发读写就绪事件, 用户程序执行读写操作.                  |
| 异步IO    | 内核执行读写操作, 并触发读写完成事件.程序没有阻塞阶段        |



## 8.4 两种高效事件处理模式

服务器通常处理三类事件

| 事件类型 | 含义 |
| -------- | ---- |
| IO事件   |      |
| 信号事件 |      |
| 定时事件 |      |

1. 同步IO模型通常实现Reactor模式.
2. 异步IO模型通常实现Proactor模式.

### Reactor模式

`Reactor模式`它要求`主线程`(IO处理单元, 下同)只负责监听文件描述符上是否有事件发生, 有的话立即将该事件通知工作线程(逻辑端元, 下同). 除此之外, 主线程不做任何实质工作, 读写数据, 接收新连接, 以及处理客户请求均在工作线程种完成.

因此, `Reactor模型显然是同步IO模型`, 即只关于`IO就绪事件`.

使用同步IO模型实现的Reactor模式的工作流程:

1. 主线程往epoll内核事件表那种注册socket上的读就绪事件
2. 主线程调用epoll_wait等待socket上有数据可读
3. 当socket上有数据可读时, epoll_wait通知主线程, 主线程则将socket可读事件放入请求队列.
4. 睡眠在请求队列的某个工作线程被唤醒, 它从socket读取数据, ->
5. 该工作线程处理客户请求, 工作线程然后往epoll内核事件表中注册该socket上的写就绪事件.
6. 主线程调用epoll_wait等待socket可写
7. 当socket上可写时, epoll_wait通知主线程, 主线程则将socket可写事件放入请求队列.
8. 睡眠在请求队列上的某个工作线程被唤醒, 它往socket上写入服务器处理客户请求的结果.

![image-20230205144133856](./charter08.assets/reactor.png)

工作线程从请求队列中取出事件后, 将根据事件的类型来决定如何处理它:

对于可读事件, 执行读数据和处理请求操作;

对于可写事件, 执行写数据操作.

没必要区分读写操作.

### Proactor模式

Proactor模式, 将所有的IO操作交给主线程和内核来处理, 工作线程仅仅负责业务逻辑.

使用异步IO模型(aio_read和aio_write为例)实现Proactor模式的工作流程:

1. 主线程调用aio_read函数向内核注册socket上的读完成事件, 并告诉内核用户读缓冲区的位置, 以及读操作完成时如何通知应用程序(以信号为例, sigevent)
2. 主线程继续处理其他逻辑
3. 当socket上的数据被读入用户缓冲区之后, 内核将向应用程序发送一个信号, 以通知应用程序数据已经可用.
4. 应用程序预先定义好的信号处理函数选择一个工作线程来处理客户请求.
5. 工作线程处理完客户请求之后,工作线程调用aio_write函数向内核注册socket上的写完成事件, 并告诉内核用户写缓冲区的位置, 以及写操作完成时如何通知应用程序(仍以信号为例)
6. 主线程继续处理其他逻辑
7. 当用户缓冲区的数据被写入socket之后,内核将向应用程序发送一个信号, 以通知应用程序数据已经发送完毕.
8. 用用程序预先定义好的信号处理函数选择一个工作线程来做善后处理, 比如决定是否关闭socket

![image-20230205150311623](./charter08.assets/proactor.png)

连接socket上的读写事件是通过aio_read/aio_write向内核注册, 因此内核将通过信号来向应用程序报告连接socket上的读写事件.所以, 主线程中的epoll_wait调用仅能用来检测监听socket上的连接请求事件, 而不能检测连接socket上的读写事件.

## 8.5 两种有效的并发模式

并发编程的目的是让程序同时执行多个任务. 如果程序是计算密集型的, 并发编程并没有优势, 反而由于任务切换时效率减低. 

但如果程序是IO密集型的, 比如经常读写文件, 访问数据库等, 则相反.

由于IO操作的速度远没有CPU计算快, 所以让程序阻塞于IO操作将浪费大量的CPU时间.因此, 如果有多个执行线程, 则当前被IO阻塞的执行线程可主动放弃CPU, 并将执行权限转移到其他线程, 这样一来CPU利用率显著提高.

对于服务器框架而言, 并发模式是指IO处理单元和多个逻辑处理单元之间协调完成任务的算法的方法.

服务器主要有两种并发编程模式:

- 半同步/半异步half-sync half-async模式
- 领导者/追随者leader/folloers模式

### 8.5.1半同步/半异步

此处的同步和异步和IO模型中的同步异步是不同的概念.

io模型中, 同步和异步是内核向应用程序通知是何种IO事件, 以及IO事件是否在内核种处理.

并发模式中, 同步和异步是指代码序列的执行顺序, 同步顺序执行, 异步是指程序的执行需要系统事件来驱动, 例如中断, 信号等.

![image-20230205155425933](./charter08.assets/并发模式-同步异步.png)

同步

- 优点: 逻辑简单, 执行效率高, 实时性强

- 缺点:不适合大量并发


异步相反

对于像服务器这种, 我们既要有较好的实时性, 又要有高并发, 我们可以采用同步线程和异步线程实现, 即半同步/半异步.

半同步/半异步: 同步线程用于处理客户逻辑, 即逻辑单元; 异步线程用于处理IO事件, 相当于IO处理单元.

流程:

1. 异步线程监听到客户请求后, 将其封装成请求对象插入请求队列.
2. 请求队列将通知某个工作在同步模式的工作线程来读取并处理该请求对象.

工作线程的选取则取决于请求队列的设计, 例如最简单额轮流选取工作线程Round Robin算法, 也可以使用条件变量或信号量随机选取.



#### 半同步/半反应堆模式 

结合事件处理模式和并发模式, 半同步/半反应堆模式= Reactor + 半同步/半异步

![image-20230224220926933](./charter08.assets/半同步-半反应堆模式.png)

- 异步线程: 只有一个, 由主线程充当, 它负责监听所有socket上的事件
- 同步线程: 由睡眠在请求队列的工作线程充当, 当有任务时他们将通过竞争来获取任务的接管权.

缺点

1. 主线程和工作线程共享请求队列, 主线程添加任务或者工作线程取出任务都需要对请求队列加锁.
2. 每个工作线程同一时间只能处理一个客户请求, 如果客户请求过多, 而工作线程较少, 则请求队列任务堆积, 客户端响应过慢.若增加工作线程则导致工作线程切换耗费大量CPU事件.

#### 相对高效的半同步/半异步模式

![image-20230224222906226](./charter08.assets/相对高效的半同半异步模式.png)

异步线程: 主线程充当, 主线程只监听socket, 连接socket由工作线程管理. 当有新的连接到来, 主线程就接受之并将新的连接socket派发给某个工作线程, 此后新socket上的任何IO操作都有被选中的工作线程来处理, 直到客户关闭.主线程检测到管道上有数据的最简单方式就是往它和工作线程之间的管道里写数据. 工作线程检测到管道上有数据可读时, 就分析是否是一个新的客户连接请求到来, 如果是, 则把该新socket上的读写时间注册到自己的epoll内核事件表中.

每个线程(主线程和工作线程)都维持自己的事件循环, 各自独立的监听不同的世家. 因此, 在这种高效的半同步/半异步模式中, 每个线程都工作在异步模式, 所以它并非严格意义上的半同步/半异步模式. (15章该模式的CGI服务器代码)

### 8.5.2领导者追随者模式

## 8.6有限状态机

本节讨论的是服务器的逻辑单元内部的一种高效编程模式: 有限状态机(finite state machine). 

```c
//状态独立的有限状态机
STATE_MACHINE(Package _pack)
{
	PackageType _type = _pack.GetType();
	switch(_type)
	{
		case type_A:
            process_package_A(_pack);
            break;
        case type_B:c
        	process_package_A(_pack);
            break;
	}

}
```

```c
//状态转移的有限状态自动机
STATE_MACHINE(Package _pack)
{
	State cur_State = type_A;
	while(cur_State != type_C)
	{
		PackageType _type = _pack.GetType();
        switch(_type)
        {
            case type_A:
                process_package_A(_pack);
                cur_State = type_B;
                break;
            case type_B:
                process_package_A(_pack);
                cur_State = type_C;
                break;
        }
	}
	PackageType _type = _pack.GetType();
	switch(_type)
	{
		case type_A:
            process_package_A(_pack);
            break;
        case type_B:
        	process_package_A(_pack);
            break;
	}

}
```



http协议头解析

```c
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>

#define BUFFER_SIZE 4096
enum CHECK_STATE { CHECK_STATE_REQUESTLINE = 0, CHECK_STATE_HEADER, CHECK_STATE_CONTENT };
enum LINE_STATUS { LINE_OK = 0, LINE_BAD, LINE_OPEN };
enum HTTP_CODE { NO_REQUEST, GET_REQUEST, BAD_REQUEST, FORBIDDEN_REQUEST, INTERNAL_ERROR, CLOSED_CONNECTION };
static const char* szret[] = { "I get a correct result\n", "Something wrong\n" };

LINE_STATUS parse_line( char* buffer, int& checked_index, int& read_index )
{
    char temp;
    for ( ; checked_index < read_index; ++checked_index )
    {
        temp = buffer[ checked_index ];
        if ( temp == '\r' )
        {
            if ( ( checked_index + 1 ) == read_index )
            {
                return LINE_OPEN;
            }
            else if ( buffer[ checked_index + 1 ] == '\n' )
            {
                buffer[ checked_index++ ] = '\0';
                buffer[ checked_index++ ] = '\0';
                return LINE_OK;
            }
            return LINE_BAD;
        }
        else if( temp == '\n' )
        {
            if( ( checked_index > 1 ) &&  buffer[ checked_index - 1 ] == '\r' )
            {
                buffer[ checked_index-1 ] = '\0';
                buffer[ checked_index++ ] = '\0';
                return LINE_OK;
            }
            return LINE_BAD;
        }
    }
    return LINE_OPEN;
}

HTTP_CODE parse_requestline( char* szTemp, CHECK_STATE& checkstate )
{
    char* szURL = strpbrk( szTemp, " \t" );
    if ( ! szURL )
    {
        return BAD_REQUEST;
    }
    *szURL++ = '\0';

    char* szMethod = szTemp;
    if ( strcasecmp( szMethod, "GET" ) == 0 )
    {
        printf( "The request method is GET\n" );
    }
    else
    {
        return BAD_REQUEST;
    }

    szURL += strspn( szURL, " \t" );
    char* szVersion = strpbrk( szURL, " \t" );
    if ( ! szVersion )
    {
        return BAD_REQUEST;
    }
    *szVersion++ = '\0';
    szVersion += strspn( szVersion, " \t" );
    if ( strcasecmp( szVersion, "HTTP/1.1" ) != 0 )
    {
        return BAD_REQUEST;
    }

    if ( strncasecmp( szURL, "http://", 7 ) == 0 )
    {
        szURL += 7;
        szURL = strchr( szURL, '/' );
    }

    if ( ! szURL || szURL[ 0 ] != '/' )
    {
        return BAD_REQUEST;
    }

    //URLDecode( szURL );
    printf( "The request URL is: %s\n", szURL );
    checkstate = CHECK_STATE_HEADER;
    return NO_REQUEST;
}

HTTP_CODE parse_headers( char* szTemp )
{
    if ( szTemp[ 0 ] == '\0' )
    {
        return GET_REQUEST;
    }
    else if ( strncasecmp( szTemp, "Host:", 5 ) == 0 )
    {
        szTemp += 5;
        szTemp += strspn( szTemp, " \t" );
        printf( "the request host is: %s\n", szTemp );
    }
    else
    {
        printf( "I can not handle this header\n" );
    }

    return NO_REQUEST;
}

HTTP_CODE parse_content( char* buffer, int& checked_index, CHECK_STATE& checkstate, int& read_index, int& start_line )
{
    LINE_STATUS linestatus = LINE_OK;
    HTTP_CODE retcode = NO_REQUEST;
    while( ( linestatus = parse_line( buffer, checked_index, read_index ) ) == LINE_OK )
    {
        char* szTemp = buffer + start_line;
        start_line = checked_index;
        switch ( checkstate )
        {
            case CHECK_STATE_REQUESTLINE:
            {
                retcode = parse_requestline( szTemp, checkstate );
                if ( retcode == BAD_REQUEST )
                {
                    return BAD_REQUEST;
                }
                break;
            }
            case CHECK_STATE_HEADER:
            {
                retcode = parse_headers( szTemp );
                if ( retcode == BAD_REQUEST )
                {
                    return BAD_REQUEST;
                }
                else if ( retcode == GET_REQUEST )
                {
                    return GET_REQUEST;
                }
                break;
            }
            default:
            {
                return INTERNAL_ERROR;
            }
        }
    }
    if( linestatus == LINE_OPEN )
    {
        return NO_REQUEST;
    }
    else
    {
        return BAD_REQUEST;
    }
}

int main( int argc, char* argv[] )
{
    if( argc <= 2 )
    {
        printf( "usage: %s ip_address port_number\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );
    
    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );
    
    int listenfd = socket( PF_INET, SOCK_STREAM, 0 );
    assert( listenfd >= 0 );
    
    int ret = bind( listenfd, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );
    
    ret = listen( listenfd, 5 );
    assert( ret != -1 );
    
    struct sockaddr_in client_address;
    socklen_t client_addrlength = sizeof( client_address );
    int fd = accept( listenfd, ( struct sockaddr* )&client_address, &client_addrlength );
    if( fd < 0 )
    {
        printf( "errno is: %d\n", errno );
    }
    else
    {
        char buffer[ BUFFER_SIZE ];
        memset( buffer, '\0', BUFFER_SIZE );
        int data_read = 0;
        int read_index = 0;
        int checked_index = 0;
        int start_line = 0;
        CHECK_STATE checkstate = CHECK_STATE_REQUESTLINE;
        while( 1 )
        {
            data_read = recv( fd, buffer + read_index, BUFFER_SIZE - read_index, 0 );
            if ( data_read == -1 )
            {
                printf( "reading failed\n" );
                break;
            }
            else if ( data_read == 0 )
            {
                printf( "remote client has closed the connection\n" );
                break;
            }
    
            read_index += data_read;
            HTTP_CODE result = parse_content( buffer, checked_index, checkstate, read_index, start_line );
            if( result == NO_REQUEST )
            {
                continue;
            }
            else if( result == GET_REQUEST )
            {
                send( fd, szret[0], strlen( szret[0] ), 0 );
                break;
            }
            else
            {
                send( fd, szret[1], strlen( szret[1] ), 0 );
                break;
            }
        }
        close( fd );
    }
    
    close( listenfd );
    return 0;
}
```

## 8.7 提高服务器性能的其他建议

前面的高效处理事件模式, 并发模式, 以及高效的处理逻辑端元处理方式, 有利于从`整体提高性能`.

而下面则从`局部提高服务器性能`.

### 8.7.1池

池: 将系统硬件资源虚拟化, 即作为资源的集合, 这组静态资源在服务器完全创建好之前分配并初始化, 当服务器正式运行时, 直接从池中获取, 无需动态分配.

从最终效果来看, 池相当于服务管理系统资源的应用层设施, 它避免了服务器对内核的频繁访问.

根据资源的类型, 池划分为内存池, 进程池, 线程池, 连接池.

### 8.7.2数据复制

高性能服务器应该避免不必要的数据复制, 尤其是数据复制发生在用户代码和内核之间的时候, 如果内核可以直接处理从socket后者文件读入的数据, 则应用程序没必要将这些数据从内核缓冲区复制到应用程序缓冲区种.

例如: 发送文件, 用户代码只检测文件是否存在, 之后用senfile直接经过内核发送.

此外, 用户代码内部的数据复制也应该避免.

### 8.7.3上下文切换和锁

上下文切换问题, 即进程切换或者线程切换导致的系统开销.

如果线程数量小于CPU数目, 那么上下文切换就不是问题了.

此外, 资源加锁, 粒度要适当.
# 第16章 服务器调制 调试和测试

## 16.1 最大文件描述符数

文件描述符是服务器程序最宝贵的资源, 几乎所有的系统调用都是和文件描述符打交道.

而系统分配给应用程序的文件描述符是有限制的, 所以我们必须总是关闭那些已经不再使用的文件描述符, 以释放它们占用的资源. 比如作为守护进程运行的服务器程序应该总是关闭标准输入, 标准输出和标准错误者三个文件描述符.

```bash
# bash中设置只对当前会话有效
# 查看用户级文件描述符限制的方法
ulimit -n
# 设置用户级文件描述符为max-file-number
ulinmit -SHn max-file-number

```

```bash
# 用户级
# 可在配置文件中修改 /etc/security/limits.conf
# 在该文件添加如下两项
hard nofile max-file-number	#硬限制
soft nofile max-file-number	#软限制
```

```bash
# 系统级

# 系统级临时修改
sysctl -w fs.file-max=max-file-number

# 系统级永久修改 /etc/sysctl.conf添加如下一项
fs.file-max=max-file-number

# 生效
sysctl -p
```



## 16.2 调整内核参数

### 16.2.1 /proc/sys/fs目录下的部分文件

`/proc/sys/fs`目录下的内核参数都与文件系统相关。对于服务器程序来说，其中最重要的是如下两个参数：

1. `/proc/sys/fs/file—max`，系统级文件描述符数限制。直接修改这个参数和 16.1节讨论的修改方法有相同的效果（不过这是临时修改）。一般修改`/proc/sys/fs/file—max`后，应用程序需要把`/proc/sys/fs/inode—max`设置为新`/proc/sys/fs/file—max`值的3～4倍，否则可能导致i节点数不够用。
2. `/proc/sys/fs/epoll/max_user_watches`，一个用户能够往epoll内核事件表中注册的事件的总量。它是指该用户打开的所有epoll实例总共能监听的事件数目，而不是单个epoll 实例能监听的事件数目。往 epoll 内核事件表中注册一个事件，在 32 位系统上大概消耗90字节的内核空间，在64位系统上则消耗160字节的内核空间。所以，这个内核参数限制了 epoll 使用的内核内存总量。

### 16.2.2 /proc/sys/net目录下的部分文件

内核中网络模块的相关参数都位于`/proc/sys/net`目录下，其中和TCP/IP协议相关的参数主要位于如下三个子目录中：core、ipv4和ipv6。在前面的章节中，我们已经介绍过这些子目录下的很多参数的含义，现在再总结一下和服务器性能相关的部分参数。

1. `/proc/sys/net/core/somaxconn`，指定listen监听队列里，能够建立完整连接从而进入 `ESTABLISHED`状态的socket的最大数目。读者不妨修改该参数并重新运行代码清单5—3，看看其影响。
2. `/proc/sys/net/ipv4/tcp＿max＿syn＿backlog`，指定listen 监听队列里，能够转移至 ESTAB- LISHED或者SYN＿RCVD状态的socket的最大数目。
3. `/proc/sys/net/ipv4/tcp＿wmem`，它包含3个值，分别指定一个socket的TCP写缓冲区 的最小值、默认值和最大值。
4. `/proc/sys/net/ipv4/tcp＿rmem`，它包含3个值，分别指定一个socket的TCP读缓冲区的 最小值、默认值和最大值。在代码清单3—6中，我们正是通过修改这个参数来改变接收通告窗口大小的。
5. `/proc/sys/net/ipv4/tcp＿syncookics`，指定是否打开TCP同步标签（syncookie）。同步标 签通过启动cookic来防止一个监听socket因不停地重复接收来自同一个地址的连接请求（同步报文段），而导致listen监听队列溢出（所谓的SYN风暴）。

除了通过直接修改文件的方式来修改这些系统参数外，我们也可以使用sysctl命令来修改它们。这两种修改方式都是临时的。永久的修改方法是在`/etc/sysctl.conf`文件中加入相应网络参数及其数值，并执行sysctl—p使之生效，就像修改系统最大允许打开的文件描述符数那样。

## 16.3 gdb调试

### 16.3.1 gdb调试多进程

```bash
# 基本方法
# 调试网络程序
./program ip prot
ps -ef|grep program
gdb
# 将进程附加到gdb上
attach pid
....
b filename:num
c
bt

# 设置调试器选项follow-fork-mode	child/parent
# 允许执行fork调用后是否继续调试父进程还是调试子进程

gdb ./program
set follow-fork-mode child
b filename:numline
r ip port
```



### 16.3.2 gdb调试多线程

```
# 分别在主线程和子线程代码打断点
# 之后切换线程
thread num

# 一个较好的方法: 将子线程数目n->1, 观察程序逻辑是否正确
# 将子线程数据从1->n, 以为调试进程或者线程的同步是否正确
```

一个较好的方法: 

1. 将子线程数目n->1, 以观察程序逻辑是否正确
2. 将子线程数据1->n, 以调试进程或者线程的同步是否正确

## 16.4 压力测试

压力测试程序由很多种实现方式, 比如IO复用, 多线程, 多进程并发编程方式, 以及将这些方式结合, 不过IO复用方式施压程度是最高的.

如下是一个epoll实现的服务器通用压力测试程序:

```c
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

/* 每个客户连接不停的向服务器发送这个请求 */
static const char *request = "GET http://localhost/index.html HTTP/1.1\r\
                              nConnection: keep-alive\r\n\r\nxxxxxxxxxxxxxxxxxxxxxx";

int setnonblokcing(int fd)
{
    int old_option = fcntl(fd, F_GETFL);
    int new_option = old_option | O_NONBLOCK;
    fcntl(fd, F_SETFL, new_option);
    return old_option;
}

void addfd(int epoll_fd, int fd)
{
    epoll_event event;
    event.data.fd = fd;
    event.events = EPOLLOUT | EPOLLET | EPOLLERR;
    epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &event);
    setnonblokcing(fd);
}

/* 向服务器写入len字节的数据 */
bool write_nbytes(int sockfd, const char *buffer, int len)
{
    int bytes_write = 0;
    printf("write out %d bytes to socket %d\n", len, sockfd);
    while (1)
    {
        bytes_write = send(sockfd, buffer, len, 0);
        if (bytes_write == -1)
        {
            return false;
        }
        else if (bytes_write == 0)
        {
            return false;
        }

        len -= bytes_write;
        buffer = buffer + bytes_write;
        if (len <= 0)
        {
            return true;
        }
    }
}

/* 从服务器读取数据 */
bool read_once(int sockfd, char *buffer, int len)
{
    int bytes_read = 0;
    memset(buffer, '\0', len);
    bytes_read = recv(sockfd, buffer, len, 0);
    if (bytes_read == -1)
    {
        return false;
    }
    else if (bytes_read == 0)
    {
        return false;
    }

    printf("read in %d bytes from socket %d with content: %s\n", bytes_read, sockfd, buffer);

    return true;
}

/* 向服务器发起num个TCP连接，我们可以通过改变num来调整测试压力 */
void start_conn(int epoll_fd, int num, const char *ip, int port)
{
    struct sockaddr_in address;
    bzero(&address, sizeof(address));
    address.sin_family = AF_INET;
    inet_pton(AF_INET, ip, &address.sin_addr);
    address.sin_port = htons(port);

    for (int i = 0; i < num; ++i)
    {
        sleep(1);
        int sockfd = socket(PF_INET, SOCK_STREAM, 0);
        printf("create %d sock\n", i);
        if (sockfd < 0);
        {
            continue;
        }
        if (connect(sockfd, (struct sockaddr *)&address, sizeof(address)) == 0)
        {
            printf("build connection %d\n", i);
            addfd(epoll_fd, sockfd);
        }
    }
}

void close_conn(int epoll_fd, int sockfd)
{
    epoll_ctl(epoll_fd, EPOLL_CTL_DEL, sockfd, 0);
    close(sockfd);
}

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        printf("usage: %s ip_address, port_number client_num\n", basename(argv[0]));
        return 1;
    }

    int epoll_fd = epoll_create(100);
    start_conn(epoll_fd, atoi(argv[3]), argv[1], atoi(argv[2]));
    epoll_event events[10000];
    char buffer[2048];

    while (1)
    {
        int fds = epoll_wait(epoll_fd, events, 10000, 2000);
        for (int i = 0; i < fds; ++i)
        {
            int sockfd = events[i].data.fd;
            if (events[i].events & EPOLLIN)
            {
                if (!read_once(sockfd, buffer, 2048))
                {
                    close_conn(epoll_fd, sockfd);
                }

                struct epoll_event event;
                event.events = EPOLLOUT | EPOLLET | EPOLLERR;
                event.data.fd = sockfd;
                epoll_ctl(epoll_fd, EPOLL_CTL_MOD, sockfd, &event);
            }
            else if (events[i].events & EPOLLOUT)
            {
                if (!write_nbytes(sockfd, request, strlen(request)))
                {
                    close_conn(epoll_fd, sockfd);
                }

                struct epoll_event event;
                event.events = EPOLLIN | EPOLLET | EPOLLERR;
                event.data.fd = sockfd;
                epoll_ctl(epoll_fd, EPOLL_CTL_MOD, sockfd, &event);
            }
            else if (events[i].events & EPOLLERR)
            {
                close_conn(epoll_fd, sockfd);
            }
        }
    }
}

```


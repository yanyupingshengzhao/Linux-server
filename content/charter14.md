# 第14章 多线程编程

Linux有两个版本的线程库: LinuxThreads和NPTL(Native POSIX Thread Library)

本章讨论的线程相关内容都属于POSIX线程标准, 但不局限于NPTL实现

1. 创建线程和结束线程
2. 读取和设置不同线程属性
3. POSIX线程同步方式: POSIX信号量, 互斥锁和条件变量

最后讨论在Linux环境下, 库函数, 进程, 信号与多线程程序之间的相互影响.

## 14.1 Linux线程概述

### 14.1.1线程模型

线程是程序中完成一个独立任务的完整执行序列, 即一个可调度实体.根据运行环境和调度者的身份, 线程可分为内核线程和用户线程.

内核线程:在有的系统上也称为LWP(Light Weight Process), 运行在内核空间, 由内核调度.

用户线程:运行在用户空间, 由线程库来调度.

当进程的一个内核线程获得CPU使用权时, 它就加载并运行一个用户线程.可见, 内核线程相当于用户线程运行的容器.

一个进程可以拥有M个内核线程和N个用户线程, 其中M<=N. 并且在一个系统的所有进程中, M和N的比值都是固定的. 按照M:N的取值, 线程的实现方式可分为三种模式: 完全用户空间实现, 完全内核调度和双层调度.

#### 完全用户空间

##### 概念

完全用户空间实现: 无需内核支持, 因此内核对此无感知, 线程库负责运行管理所有线程, 比如优先级, 时间片等.线程库利用longjmp来切换线程的执行, 使他们看起来像是并发.但实际上, 内核仍然是把整个进程作为最小的单位来调度的.换句话说, 一个进程的所有执行线程共享该进程的时间片, 它们对外表现出相对优先级, 因此, 对这种实现方式, N=1, M不限.

##### 优点: 

1. 创建和调度线程无须内核干预, 因此速度相当快.
2. 不占用内核资源, 即使一个进程创建了很多线程对于系统性能也不会造成明显影响.

##### 缺点:

1. 一个进程的多个线程无法运行在不同的CPU上, 因此内核是按照其最小调度单位来分配CPU的.
2. 此外, 线程的优先级只对同一个进程的线程有效, 比较不同进程中的线程优先级无意义.

#### 完全内核调度

##### 概念

完全由内核调度的模式创建, 调度线程的任务都交给了内核, 运行在用户空间的线程库无需执行管理任务, 这与完全在用户空间实现的线程恰恰相反.因此这种实现, M:N=1:1.

##### 优点:

1. 一个进程的多个线程可以运行在不同的CPU上
2. 不同进程的线程优先级比较有意义

##### 缺点:

1. 创建和调度线程须内核干预
2. 占用内核资源

不过现代Linux内核对于线程的支持已大大增强.

#### 双层调度模式

##### 概念

内核调度M个内核线程, 线程库调度N个用户线程.这种线程模式, 结合前两者优点: 不但不会消耗过多的内核资源, 而且线程切换速度也较块, 同时可以充分利用多处理器的优势.





### 14.1.2Linux线程库

Linux上最有名的线程库是LinuxThreads和NPTL, 它们都采用1:1的方式实现.由于LinuxThreads在开发的时候, Linux内核对于线程的支持还非常有限, 所以其稳定性和POSIX兼容性都远不及NPTL.

现代Linux默认使用线程库是NPTL.用户可以通过如下命令查看当前系统上所使用的线程库:

`getconf GNU_LIBPTHREAD_VERSION`.

LinuxThreads线程库的内核线程是用clone系统调用创建的进程模拟. clone系统调用和fork系统调用作用类似: 创建调用进程的子进程. 不过我们可以为CLONE_THREAD标志, 这种情况下它创建的子进程与调用用进程共享相同的虚拟地址空间, 文件描述符和信号处理函数, 这些都是线程的特点. 不过, 用进程来模拟线程会导致很多语义问题, 比如:

1. 每个线程都拥有不同的PID, 因此不符合POSIX规范.
2. Linux信号处理本来就是基于进程的, 但现在一个进程内部的所有线程都能而且必须处理信号.
3. 用户ID, 组ID对一个进程中的不同线程来说可能是不一样的.
4. 程序产生的核心转储文件不会包含所有的线程的信息, 而只保险产生该核心转储文件的线程信息.
5. 由于每个线程都是一个进程, 因此系统允许的最大进程数也就是最大线程数.

LinuxThreads线程库一个有名的特性是所谓的管理线程.它是进程中专门用于管理其他共线程的线程.其作用包含:

1. 系统发送给进程的终止信号先由管理线程接收, 管理线程再给其他工作线程发送同样的信号.
2. 当终止工作线程或者工作线程退出, 则管理线程将阻止它, 直到其他所有的工作线程都结束之后才唤醒它.
3. 回收每个线程堆栈使用的内存.

管理线程的引入, 增加了额外的系统开销, 并且它只能运行在一个CPU上, 所以LinuxTHreads线程库也不能充分利用多处理器系统的优势.

要解决LinuxThreads线程库的一系列问题, 不仅要改进线程库, 最主要的是需要内核提供更完善的线程支持. 因此, 自Linux内核2.6版本开始, 提供了真正的内核线程.新的NPTL线程库也就应运而生.相比于LinuxThreads, NPTL的主要优势如下:

1. 内核线程不再是一个进程, 因此避免了很多进程模拟内核线程导致的语义问题.
2. 摒弃了管理线程, 终止线程, 回收线程堆栈等工作都可以由内核来完成.
3. 由于不存在管理线程, 所以一个进程的线程可以运行在不同的CPU上, 从而充分利用了多处理器系统的优势.
4. 线程的同步由内核来完成, 隶属于不同进程的线程之间也能共享互斥锁, 因此可以实现跨进程的线程同步.

  

## 14.2 创建线程和结束线程

```c
#include <pthread.h>
int pthread_create( pthread_t* thread, const pthread_attr_t* attr, void* (*start_routine)(void*), void* arg);
typedef unsigned long int pthread_t;
/*
描述
	pthread_create创建新线程
参数
	thread 资源标识符, 实际上, Linux上几乎所有资源的标识符都是一个整型数
	attr 用于设置线程属性, 传NULL代表默认线程属性
	start_routine 指定新线程函数
	arg 指定新线程函数的参数
返回值
	成功返回0
	失败返回错误码
此外
	一个用户可以打开的线程数量不能超过RLIMIT_NPROC软资源限制
	或者线程总数不得超过/proc/sys/kernel/thread-max内核定义的值
*/

void pthread_exit(void* retval);
/*
描述
	pthread_exit函数通过retval参数向线程的回收者传递其推出信息, 线程执行完之后不会返回到调用者, 而且永远不会失败.
参数
	返回值地址
*/
int pthread_join(pthread_t thread, void** retval);
/*
描述
	阻塞等待目标线程返回的退出信息
参数
	thread 等待的线程标识符
	retval 返回值地址
此外
	thread_join函数可能会引发的错误码
		EDEADLK 可能会引起死锁, 例如两个线程相互针对对方调用pthread_join, 或者自身调用pthread_join
		EINVAL 目标线程不可回收, 后者已经有其他线程在回收该目标线程.
		ESRCH 目标线程不存在
*/

int pthread_cancel(pthread_t thread);
/*
描述
	异常终止一个线程, 即取消线程
参数
	thread 取消线程ID
返回值
	成功0, 失败错误码
此外
	接收到取消请求的目标线程可以决定是否允许被取消, 以及如何取消, 由如下两个函数完成
*/

int pthread_setcanselstate(int state, int* oldstate);

/*
描述
	设置取消状态(是否允许取消)
参数
	state 取消状态
		PTHREAD_CANCEL_ENABLE 允许线程被取消, 它是线程被创建时的默认取消状态
		PTHREAD_CANCEL_DISABLE 禁止线程被取消, 这种情况, 如果一个线程收到取消请求, 则它会将请求挂起, 直到线程被允许取消
		PTHREAD_CANCEL_ASYNCHRONOUS
		PTHREAD_CANCEL_DEFERRED 
*/
int pthread_setcanceltype(int type, int* oldtype);
/*
描述
	设置取消类型(如何取消)
参数
	type 可选值
		PTHREAD_CANCEL_ASYNCHRONOUS 线程随时可以被取消, 他将使得接收到取消请求的目标线程立即采取行动
		PTHREAD_CANCEL_DEFERRED , 允许目标线程推迟行动, 直到它调用了下面几个所wide取消点函数中的一个: pthread_join, pthread_testcancel, pthread_cond_wait, pthread_con_timedwait, semwait和sigwait.根据POSIX标准, 其他可能阻塞的系统调用, 比如read, wait, 也可以成为取消点.不过为了安全起见, 我们最好在可能被取消的代码中调用pthread_setcanceltype函数以设置取消点.
返回值
	成功返回0, 失败返回错误码
*/
```



## 14.3线程属性

```c
#include <bits/pthreadtype.h>
#define __SIZEOF_PTHREAD_ATTR_T 36
typedef union
{
    char __size[__SIZEOF_PTHREAD_ATTR_T];
    long int __align;
} pthread_attr_t;
/*
描述
	可见, 各种线程属性包含在一个字符数组中, 线程库定义了一系列的函数来操作pthread_attr_t类型的变量, 以方便我们获取和设置线程属性

*/
// 初始化线程属性对象
int pthread_attr_init(pthread_attr_t *attr);
// 销毁线程属性对象, 直到再次被初始化前都不能用
int pthread_attr_destory(pthread_attr_t *attr)

int pthread_attr_getdetachstate(const pthread_attr_t *attr, int *detachstate);
int pthread_attr_setdetachstate(pthread_attr_t *attr, int detachstate);
int pthread_attr_getstackaddr(const pthread_attr_t *attr, void** stackaddr);
int pthread_attr_setstackaddr(pthread_attr_t *attr, void** stackaddr);
int pthread_attr_getstacksize(const pthread_attr_t *attr, size_t* stacksize);
int pthread_attr_setstacksize(pthread_attr_t *attr, size_t* stacksize);
int pthread_attr_setstack(pthread_attr_t *attr, void** stacksize, size_t* stacksize);
.....
int pthread_detach(pthread_t thread)
```



## 14.4 posix信号量

```c
#include<semaphore>

int sem_init(sem_t* sem, int pshared, unsigned int value)
/*
描述
	初始化一个信号量
参数
	sem 返回信号量
	pshared 指定信号量类型
		=0 信号量是当前进程的局部信号量
		否则 该信号量可以在多个进程间共享
	value 指定信号量的初始值
	
返回值
	成功返回0, 失败-1+errno
此外
	若初始化已经初始化过的信号量, 则结果未定义
*/

int sem_destory(sem_t* sem)
/*
描述
	销毁信号量
参数
	sem 信号量
返回值
	成功返回0, 失败-1+errno
此外
	若销毁一个正被其他线程等待的信号量, 则结果未定义
*/

int sem_wait(sem_t* sem)
/*
描述
	p操作, 若信号量的值为0, 则sem_wait将阻塞, 直到信号量具有非0值
参数
	sem 指定信号量
返回值
	成功返回0, 失败-1+errno
*/

int sem_trywait(sem_t* sem)
/*
描述
	相当于sem_wait的非阻塞版本
	当信号量值非0时, 执行减1操作
	当信号量值为0时, 返回-1, 并设置errno为EAGAIN
参数
	sem 指定信号量
返回值
	成功返回0, 失败-1+errno
*/
int sem_post(sem_t* sem)
/*
描述
	以原子操作执行v操作, 信号量值将被+1
参数
	sem 指定信号量
返回值
		成功返回0, 失败-1+errno
*/
```



## 14.5 互斥锁

互斥锁等价于信号量值为1, 互斥是同步的一种特殊情况.

### 14.5.1互斥锁基础api

```c
#include <pthread.h>
int pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *mutexattr);
/*
描述
	初始化互斥锁, 
	或者 pthread_mutes_t mutex = PTHREAD_MUTEX_INITIALIZER;
	PTHREAD_MUTEX_INITIALIZER 将互斥锁各个字段初始化为0;
参数
	mutex 用于返回互斥锁
	mutexatrr 互斥锁属性, NULL为默认属性
返回值
	成功返回0, 失败返回错误码
	
*/
int pthread_mutex_destory(pthread_mutex_t *mutex);
int pthread_mutex_lock(pthread_mutex_t *mutex);
int pthread_mutex_trylock(pthread_mutex_t *mutex);
int pthread_mutex_unlock(pthread_mutex_t *mutex);
```

### 14.5.2互斥锁属性

```c
#include <pthread.h>
//初始化互斥锁属性对象
int pthread_mutexattr_init(pthread_mutexattr_t *attr);
int pthread_mutexattr_destory(pthread_mutexattr_t *attr);


int pthread_mutexattr_getpshared(const pthread_mutexattr_t *attr, int *pshared);
int pthread_mutexattr_setpshared(const pthread_mutexattr_t *attr, int pshared);
/*
描述
	获取和设置互斥锁的pshared属性
参数
	pshared
		PTHREAD_PROCESS_SHARED 互斥锁可以被跨进程共享
		PTHREAD_PROCESS_PRIVATE 互斥锁只能被和锁定的初始化线程隶属同一个进程的线程共享


*/

int pthread_mutexattr_gettype(const pthread_mutexattr_t *attr, int *type);
int pthread_mutexattr_settype(const pthread_mutexattr_t *attr, int type);

/*
描述
	获取和设置互斥锁的type属性, 指定互斥锁的类型
参数
	type
		PTHREAD＿MUTEX＿NORMAL，普通锁。这是互斥锁默认的类型。当一个线程对一个普通锁加锁以后，其余请求该锁的线程将形成一个等待队列，并在该锁解锁后按优先级获得它。这种锁类型保证了资源分配的公平性。但这种锁也很容易引发问题：一个线程如果对一个已经加锁的普通锁再次加锁，将引发死锁；对一个已经被其他线程加锁的普通锁解锁，或者对一个已经解锁的普通锁再次解锁，将导致不可预期的后果。｜
		PTHREAD＿MUTEX＿ERRORCHECK，检错锁。一个线程如果对一个已经加锁的检错 锁再次加锁，则加锁操作返回EDEADLK。对一个已经被其他线程加锁的检错锁解锁，或者对一个已经解锁的检错锁再次解锁，则解锁操作返回EPERM。
		PTHREAD＿MUTEX＿RECURSIVE，嵌套锁。这种锁允许一个线程在释放锁之前多次 对它加锁而不发生死锁。不过其他线程如果要获得这个锁，则当前锁的拥有者必须执行相应次数的解锁操作。对一个已经被其他线程加锁的嵌套锁解锁，或者对一个已经解锁的嵌套锁再次解锁，则解锁操作返回EPERM。
		PTHREAD＿MUTEX＿DEFAULT，默认锁。一个线程如果对一个已经加锁的默认锁再次加锁，或者对一个已经被其他线程加锁的默认锁解锁，或者对一个已经解锁的默认锁再次解锁，将导致不可预期的后果。这种锁在实现的时候可能被映射为上面三种锁


*/
```



### 14.5.3互斥锁举例

按照不同顺序上锁和解锁

```c
pthread_mutex_t mutex;
int count = 0;
void* t(void *a)
{
    pthread_mutex_lock(&mutex);
    printf("%d\n", count);
    count++;
    pthread_mutex_unlock(&mutex);
}
int main()
{
    pthread_mutex_init(&mutex, nullptr);
    pthread_t thread[10];
    for (int i = 0; i < 10; ++i)
    {
        pthread_create(&thread[i], nullptr, t, nullptr);
    }
    sleep(3);
    pthread_mutex_destroy(&mutex);
}
```



## 14.6条件变量

如果说互斥锁用于对同步线程对共享数据的访问, 那么条件变量则是用于在线程之间同步共享数据的值.

条件变量提供了一种线程间的通信机制: 当某个共享数据达到某个值的时候, 唤醒等待这个共享数据的线程.

```c
#include
int pthread_cond_init(pthread_cond_t *cond, const pthread_condattr *cond_attr);
/*
描述
	初始化条件变量, 
	或者 pthread_cond_init cond = PTHREAD_COND_INITIALIZER;
	PTHREAD_COND_INITIALIZER 将条件变量各个字段初始化为0;
参数
	mutex 用于返回互斥锁
	mutexatrr 互斥锁属性, NULL为默认属性
返回值
	成功返回0, 失败返回错误码
	
*/
int pthread_cont_destory(pthread_cond_t *cond);
int pthread_cont_broadcast(pthread_cond_t *cond);
/*
描述
	唤醒所有等待该条件变量的线程,
	至于唤醒哪一个取决于线程的优先级和调度策略
*/
int pthread_cond_signal(pthread_cond_t *cond);
/*
描述
	唤醒一个等待条件变量的线程
*/
int pthread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex);
/*
描述
	用于等待目标条件变量
参数
	mutex 用于保护条件变量的互斥锁, 以确保pthread_cond_wait操作的原子性
*/
```



## 14.7线程同步机制包装类

将三种线程同步机制分别包装成3个类

```c
//locker.h
#ifndef __LOCKER_H
#define __LOCKER_H

#include <exception>
#include <pthread.h>
#include <semaphore.h>

/* 封装信号量类 */
class sem
{
public:
    /* 创建并初始化信号量 */
    sem()
    {
        if (sem_init(&m_sem, 0, 0) != 0)
        {
            /* 构造函数没有返回值，可以通过抛出异常来报告错误 */
            throw std::exception();
        }
    }
    /* 销毁信号量 */
    ~sem()
    {
        sem_destroy(&m_sem);
    }

    /* 等待信号量 */
    bool wait()
    {
        return sem_wait(&m_sem) == 0;
    }

    /* 增加信号量 */
    bool post()
    {
        return sem_post(&m_sem) == 0;
    }

private:
    sem_t m_sem;
};

/* 封装互斥锁的类 */
class locker
{
public:
    /* 创建并初始化互斥锁 */
    locker()
    {
        if (pthread_mutex_init(&m_mutex, NULL) != 0)
        {
            throw std::exception();
        }
    }
    /* 销毁互斥锁 */
    ~locker()
    {
        pthread_mutex_destroy(&m_mutex);
    }
    /* 获取互斥锁 */
    bool lock()
    {
        return pthread_mutex_lock(&m_mutex) == 0;
    }

    /* 释放互斥锁 */
    bool unlock()
    {
        return pthread_mutex_unlock(&m_mutex) == 0;
    }
private:
    pthread_mutex_t m_mutex;
};

/* 封装条件变量的类 */
class cond
{
public:
    /* 创建并初始化条件变量 */
    cond()
    {
        if (pthread_mutex_init(&m_mutex, NULL) != 0)
        {
            throw std::exception();
        }
        if (pthread_cond_init(&m_cond, NULL) != 0)
        {
            /* 构造函数中一旦出现问题，就应该立即释放已经成功分配了的资源 */
            pthread_mutex_destroy(&m_mutex);
            throw std::exception();
        }
    }
    /* 销毁条件变量 */
    ~cond()
    {
        pthread_mutex_destroy(&m_mutex);
        pthread_cond_destroy(&m_cond);
    }

    /* 等待条件变量 */
    bool wait()
    {
        int ret = 0;
        pthread_mutex_lock(&m_mutex);
        ret = pthread_cond_wait(&m_cond, &m_mutex);
        pthread_mutex_unlock(&m_mutex);
        return ret == 0;
    }

    /* 唤醒等待条件变量的线程 */
    bool signal()
    {
        return pthread_cond_signal(&m_cond) == 0;
    }
private:
    pthread_mutex_t m_mutex;
    pthread_cond_t m_cond;
};

#endif //__LOCKER_H
```



## 14.8多线程环境

### 14.8.1 可重入函数

Linux只有少部分系统调用是不可重入函数.

### 14.8.2 线程和进程

### 14.8.3 线程和信号
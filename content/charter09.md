# 第9章 IO复用

IO复用可以使程序同时监听多个文件描述符.

下列情况会使用IO复用技术:

1. 客户端程序需要同时处理多个socket, 例如非阻塞connect
2. 客户端需要同时处理用户输入和网络连接, 例如聊天室
3. TCP服务器要同时处理监听socket和连接socket
4. 服务器同时处理TCP和UDP请求, 例如回射服务器
5. 服务器要同时监听多个端口, 或者处理多种服务.

但是, IO复用虽然可以同时监听多个文件描述符, 但是本身使组设的.

## 9.1 select

### 基本使用

select系统调用的用途: 在指定时间内, 监听用户感兴趣的文件你描述符上的可读 可写 和 异常事件.

```c
#include <sys/select.h>
int select(int nfds, fd_set* readfds, fd_set* writefds, fd_set* exceptfds, struct timeval* timeout);
/*
描述
	将感兴趣的文件描述符上的事件集中查询后, 返回修改后的文件描述符集合
参数
	nfds 指定被监听的文件描述符的总数, 通常被设置为监听所有的文件描述符+1(fd自0开始)
	readfds writefds execptfds 分别指向可读 可写 和 异常事件对应的fd集合
	timeout 超时时间大小
	
返回值
	成功返回就绪文件描述符总数, 且将文件描述符集合设置为感兴趣的文件描述符集合
	失败返回-1, +errno=EINTR
*/

struct timeval
{
    long tv_sec;	//seconds
    long tv_usec;	//微秒数
}

#include <typesizes.h>
#define __FD_SETSIZE 1024

#include <sys/select.h>
#define FD_SETSIZE __FD_SETSIZE
typedef long int __fd_mask;
#undef __NFDBITS
#define __NFDBITS (8*(int)sizeof(__fd_mask))

typedef struct
{
#ifdef __USE_XOPEN
    __fd_mask fds_bits[__FD_SETSIZE / __NFDBITS];
#define __FDS_BITS(set) ((set)->fds_bits)
#else
    __fd_mask __fds_bit[__FD_SETSIZE / __NFDBITS];
#define __FDS_BITS(set) ((set)->__fds_bits)
#endif
}fd_set;

/*

可知fd_set是一个整形数组,其大小由FD_SETSIZE指定, 这就限制了select能同时处理的文件描述符总量, 每一个位代表一个文件描述符
*/

#include <sys/select.h>
FD_ZERO(fd_set* fdset);				//清除fdset所有位
FD_SET(int fd, fd_set* fdset);		//设置fdset的位fd
FD_CLR(int fd, fd_set* fdset);		//清除fdset的位fd
int FD_ISSET(int fd, fd_set* fdset);//测试fdset的位是否被设置
```

### 9.1.2 文件描述符就绪条件

在哪些情况下文件描述符可以被认为是可读, 可写, 或者出现异常

可读:

1. socket内核接收缓冲区中的字节数大于或等于其低水位标记`SO_RCVLOWAT`.此时可无阻塞读取该socket, 并且操作返回的字节数大于0.
2. socket通信的对方关闭连接, 此时socket的读操作返回0.
3. 监听socket上有新的连接请求.
4. socket上有未处理的错误, 此时可用getsockopt来读取和清除该错误.

可写:

1. socket内核可写缓冲区中的字节数大于或等于其低水位标记`SO_SNDLOWAT`.此时可无阻塞写该socket, 并且操作返回的字节数大于0.
2. socket的写操作被关闭, 对写操作被关闭的socket执行写操作将触发SIGPIPE信号.
3. socket使用非阻塞connect连接成功或者失败(超时)之后.
4. socket上有未处理的错误, 此时我们可以使用getsockopt来读取和清楚该错误.

异常:

1. socket接收到带外数据.

### 处理带外数据

```c
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>

int main( int argc, char* argv[] )
{
	if( argc <= 2 )
	{
		printf( "usage: %s ip_address port_number\n", basename( argv[0] ) );
		return 1;
	}
	const char* ip = argv[1];
	int port = atoi( argv[2] );
	printf( "ip is %s and port is %d\n", ip, port );

	int ret = 0;
    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

	int listenfd = socket( PF_INET, SOCK_STREAM, 0 );
	assert( listenfd >= 0 );

    ret = bind( listenfd, ( struct sockaddr* )&address, sizeof(address ) );
	assert( ret != -1 );

	ret = listen( listenfd, 5 );
	assert( ret != -1 );

	struct sockaddr_in client_address;
    socklen_t client_addrlength = sizeof( client_address );
	int connfd = accept( listenfd, ( struct sockaddr*)&client_address, &client_addrlength );
	if ( connfd < 0 )
	{
		printf( "errno is: %d\n", errno );
		close( listenfd );
	}

	char remote_addr[INET_ADDRSTRLEN];
	printf( "connected with ip: %s and port: %d\n", inet_ntop( AF_INET, &client_address.sin_addr, remote_addr, INET_ADDRSTRLEN ), ntohs( client_address.sin_port ) );

	char buf[1024];
    fd_set read_fds;
    fd_set exception_fds;

    FD_ZERO( &read_fds );
    FD_ZERO( &exception_fds );

    int nReuseAddr = 1;
	setsockopt( connfd, SOL_SOCKET, SO_OOBINLINE,&nReuseAddr,sizeof( nReuseAddr ) );
	while( 1 )
	{
        //每次调用select需恢复文件描述符集合
		memset( buf, '\0', sizeof( buf ) );
		FD_SET( connfd, &read_fds );
		FD_SET( connfd, &exception_fds );

        ret = select( connfd + 1, &read_fds, NULL, &exception_fds, NULL );
		printf( "select one\n" );
        if ( ret < 0 )
        {
            printf( "selection failure\n" );
            break;
        }
		//普通可读事件
        if ( FD_ISSET( connfd, &read_fds ) )
		{
        	ret = recv( connfd, buf, sizeof( buf )-1, 0 );
			if( ret <= 0 )
			{
				break;
			}
			printf( "get %d bytes of normal data: %s\n", ret, buf );
		}
        //对于异常事件, 采用MSG_OOB标志的recv函数读取带外数据
		else if( FD_ISSET( connfd, &exception_fds ) )
        {
        	ret = recv( connfd, buf, sizeof( buf )-1, MSG_OOB );
			if( ret <= 0 )
			{
				break;
			}
			printf( "get %d bytes of oob data: %s\n", ret, buf );
        }

	}

	close( connfd );
	close( listenfd );
	return 0;
}
```

## 9.2 poll

#### 基本使用

Poll与select类似.

```c
#include <poll.h>
int poll(struct pollfd* fds, nfds_t nfds, int timeout);
/*
描述
	
参数
	fds pollfd结构体数组, 即文件描述符+ 注册事件(可读, 可写, 异常)
	nfds 监听fd的集合大小, 
	timeout 单位毫秒, long int大小, 
		timeout = -1, poll一直阻塞, 直到有事件触发
		timeout = 0, poll检测到无事件触发, 直接返回.
返回值
*/
typeof unsigned long int nfds_t;

struct pollfd
{
    int fd;	//文件描述符
    short events;	//注册事件
    short revents;	//实际发送事件, 由内核填充
}
```

#### Poll事件

| 事件      | 描述                                                      | 是否可作为输入 | 是否可作为输出 |
| --------- | --------------------------------------------------------- | -------------- | -------------- |
| POLLIN    | 数据可读, 包含普通数据和优先数据                          | 是             | 是             |
| POLLPRI   | 高优先数据可读, 比如TCP带外数据                           | 是             | 是             |
| POLLOUT   | 数据可写, 包含普通数据和优先数据                          | 是             | 是             |
| POLLRDHUP | TCP连接被对方关闭, 或者对方关闭了写操作, 它由GNU引入      | 是             | 是             |
| POLLERR   | 错误                                                      | 否             | 是             |
| POLLHUP   | 挂起, 比如管道的写端被关闭后, 该描述符上将收到POLLHUP事件 | 否             | 是             |
| POLLNVAL  | 文件描述符未打开                                          | 否             | 是             |

```
在Linux kernel 2.6.17之前, 无POLLRDHUP事件, 需通过recv返回值来判断接收到的是有效数据还是对方关闭连接的请求, 并做相应处理.
之后, 引入POLLRDHUP事件, 不过使用前需定义 _GNU_SOURCE
```



## 9.3 epoll

### 基本内容

epoll不像select和poll每次传参需要拷贝代价, 内核区和用户区数据交换位拷贝, 且前两者大小受限.

epoll在内核去建立事件表, 由一组函数支持它来控制这个事件表, 事件表都有其属性.

```c
#include <sys/epoll.h>
int epoll_create(int size);
/*
描述
	在内核创建一个表
参数
	size给内核提示, 需要多大
返回值
	创建的epoll的句柄, 由文件描述符充当, 即在一个进程中唯一标识该epoll创建的内核epoll事件表

*/

int epoll_ctl(int epfd, int op, int fd, struct epoll_event* event);
/*
描述
	控制内核表内数据
参数
	epfd 需修改的已创建的epoll的文件描述
	fd 需要操作的文件描述符
	op操作类型, EPOLL_CTL_ADD	EPOLL_CTL_MOD	EPOLL_CTL_DEL
返回值
	成功 0, 失败 -1+errno
*/
struct epoll_event
{
    __uint32_t events;	//epoll事件
    epoll_data_t data;	//用户数据, 联合体, 一般只存该fd
};
/*
poll_event和epoll_event支持事件类型基本相同, poll事件宏加上E就为epoll的事件, 但epoll多了两个额外事件类型, EPOLLET和 EPOLLONESHOT
*/
typedef union epoll_data
{
	void* ptr;
    int fd;
    uint32_t u32;
    uint64_t u64;
}epoll_data_t;

int epoll_wait(int epfd, struct epoll_event* events, int maxevents, int timeout);
/*
描述
	查询感兴趣的事件
参数
	epfd 感兴趣的事件
	events 结果, 数组, 将epoll内核事件表上的就绪事件, 的fd及其事件存于events
	maxevents 最多监听多少个事件
	timeout 等待时间, 与poll的含义相同
返回值
	成功返回就绪事件个数, 失败 -1+errno
*/
```



poll和epoll在使用上的差别:

```c
int ret = poll(fds, MAX_EVENTS_NUMBER, -1);
for(int i = 0; i < MAX_EVENTS_NUMBER;i++)
{
	if(fds[i].revents & POLLIN)
	{
		int sockfd = fd[i].fd;
		...
	}
}

int ret = epoll(epollfd, events, MAX_EVENTS_NUMBER, -1);
for(int i = 0; i < ret;i++)
{
	
	int sockfd = events[i].data.fd;
	...
}
```



### LT和ET模式

epoll对文件描述符的操作模式

1. Level Trigger, 电平触发, 默认模式,此时相当于一个效率较高的poll.
   - epoll_wait后可以不立即处理, 当应用程序下次调用还会再次通知.

2. Edge Trigger, 边缘触发, 注册EPOLLET事件, epoll将以ET模式操作, 高效工作模式.
   - epoll_wait后要立即处理, 且下次不再触发, 降低了同一个epoll事件触发次数, 因此提高了工作效率.

```c
//测试LT ET
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <pthread.h>

#define MAX_EVENT_NUMBER 1024
#define BUFFER_SIZE 10

//设置文件描述符非阻塞状态
int setnonblocking( int fd )
{
    int old_option = fcntl( fd, F_GETFL );
    int new_option = old_option | O_NONBLOCK;
    fcntl( fd, F_SETFL, new_option );
    return old_option;
}

//将文件描述符fd上的EPOLLIN注册到epollfd指示的epoll内核事件表上, 参数eable_et指定是否对fd启用ET模式
void addfd( int epollfd, int fd, bool enable_et )
{
    epoll_event event;
    event.data.fd = fd;
    event.events = EPOLLIN;
    if( enable_et )
    {
        event.events |= EPOLLET;
    }
    epoll_ctl( epollfd, EPOLL_CTL_ADD, fd, &event );
    setnonblocking( fd );
}

// LT模式的工作流程
void lt( epoll_event* events, int number, int epollfd, int listenfd )
{
    char buf[ BUFFER_SIZE ];
    for ( int i = 0; i < number; i++ )
    {
        int sockfd = events[i].data.fd;
        if ( sockfd == listenfd )
        {
            struct sockaddr_in client_address;
            socklen_t client_addrlength = sizeof( client_address );
            int connfd = accept( listenfd, ( struct sockaddr* )&client_address, &client_addrlength );
            addfd( epollfd, connfd, false );
        }
        else if ( events[i].events & EPOLLIN )
        {
        	//只要socket上的都缓冲还有未读出数据, 这段代码就会被触发
            printf( "event trigger once\n" );
            memset( buf, '\0', BUFFER_SIZE );
            int ret = recv( sockfd, buf, BUFFER_SIZE-1, 0 );
            if( ret <= 0 )
            {
                close( sockfd );
                continue;
            }
            printf( "get %d bytes of content: %s\n", ret, buf );
        }
        else
        {
            printf( "something else happened \n" );
        }
    }
}

// ET模式工作流程
void et( epoll_event* events, int number, int epollfd, int listenfd )
{
    char buf[ BUFFER_SIZE ];
    for ( int i = 0; i < number; i++ )
    {
        int sockfd = events[i].data.fd;
        if ( sockfd == listenfd )
        {
            struct sockaddr_in client_address;
            socklen_t client_addrlength = sizeof( client_address );
            int connfd = accept( listenfd, ( struct sockaddr* )&client_address, &client_addrlength );
            addfd( epollfd, connfd, true );
        }
        else if ( events[i].events & EPOLLIN )
        {
        	//这段代码不会被重复触发
            printf( "event trigger once\n" );
            while( 1 )
            {
                memset( buf, '\0', BUFFER_SIZE );
                int ret = recv( sockfd, buf, BUFFER_SIZE-1, 0 );
                if( ret < 0 )
                {
                	//对于非阻塞IO, 下面的条件成立表述数据以及全部读取完毕, 此后, epoll就能再次触发sockfd上的EPOLLIN事件, 以驱动下一次读操作
                    if( ( errno == EAGAIN ) || ( errno == EWOULDBLOCK ) )
                    {
                        printf( "read later\n" );
                        break;
                    }
                    close( sockfd );
                    break;
                }
                else if( ret == 0 )
                {
                    close( sockfd );
                }
                else
                {
                    printf( "get %d bytes of content: %s\n", ret, buf );
                }
            }
        }
        else
        {
            printf( "something else happened \n" );
        }
    }
}

int main( int argc, char* argv[] )
{
    if( argc <= 2 )
    {
        printf( "usage: %s ip_address port_number\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );

    int ret = 0;
    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int listenfd = socket( PF_INET, SOCK_STREAM, 0 );
    assert( listenfd >= 0 );

    ret = bind( listenfd, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );

    ret = listen( listenfd, 5 );
    assert( ret != -1 );

    epoll_event events[ MAX_EVENT_NUMBER ];
    int epollfd = epoll_create( 5 );
    assert( epollfd != -1 );
    addfd( epollfd, listenfd, true );

    while( 1 )
    {
        int ret = epoll_wait( epollfd, events, MAX_EVENT_NUMBER, -1 );
        if ( ret < 0 )
        {
            printf( "epoll failure\n" );
            break;
        }
    
        lt( events, ret, epollfd, listenfd );
        //et( events, ret, epollfd, listenfd );
    }

    close( listenfd );
    return 0;
}
```

### 9.3.4 POLLONESHOT事件

即使我们使用ET模式, 一个socket上的事件还是可能被多次触发, 在并发程序中, 一个线程在处理socket数据, 突然该socket新数据到来, 另一个线程就会处理数据, 造成脏读.

我们可以通过epoll的EPOLLONESHOT事件实现这个问题.

即注册事件时要加上EPOLLONESHOT事件, 使用结束要重置EPOLLONESHOT事件, 确保下一次可读时, 其EPOLLIN事件能被触发.

```c
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <pthread.h>

#define MAX_EVENT_NUMBER 1024
#define BUFFER_SIZE 1024
struct fds
{
   int epollfd;
   int sockfd;
};

int setnonblocking( int fd )
{
    int old_option = fcntl( fd, F_GETFL );
    int new_option = old_option | O_NONBLOCK;
    fcntl( fd, F_SETFL, new_option );
    return old_option;
}
/*
将fd上的EPOLLIN和EPOLLET注册到epollfd指示的epoll内核事件表中, 参数oneshot指定是否组测fd的EPOLLONESHOT事件
*/
void addfd( int epollfd, int fd, bool oneshot )
{
    epoll_event event;
    event.data.fd = fd;
    event.events = EPOLLIN | EPOLLET;
    if( oneshot )
    {
        event.events |= EPOLLONESHOT;
    }
    epoll_ctl( epollfd, EPOLL_CTL_ADD, fd, &event );
    setnonblocking( fd );
}
/*
重置fd上的事件, 这样操作之后, 尽管fd上的EPOLLONESHOT事件被注册了, 但是操作系统仍然会触发fd上的EPOLLIN事件, 且只触发一次
*/
void reset_oneshot( int epollfd, int fd )
{
    epoll_event event;
    event.data.fd = fd;
    event.events = EPOLLIN | EPOLLET | EPOLLONESHOT;
    epoll_ctl( epollfd, EPOLL_CTL_MOD, fd, &event );
}
//工作线程
void* worker( void* arg )
{
    int sockfd = ( (fds*)arg )->sockfd;
    int epollfd = ( (fds*)arg )->epollfd;
    printf( "start new thread to receive data on fd: %d\n", sockfd );
    char buf[ BUFFER_SIZE ];
    memset( buf, '\0', BUFFER_SIZE );
    //循环读取sockfd上的数据, 直到遇到EAGAIN错误
    while( 1 )
    {
        int ret = recv( sockfd, buf, BUFFER_SIZE-1, 0 );
        if( ret == 0 )
        {
            close( sockfd );
            printf( "foreiner closed the connection\n" );
            break;
        }
        else if( ret < 0 )
        {
            if( errno == EAGAIN )
            {
                reset_oneshot( epollfd, sockfd );
                printf( "read later\n" );
                break;
            }
        }
        else
        {
            printf( "get content: %s\n", buf );
            //休息5秒模拟处理工作
            sleep( 5 );
        }
    }
    printf( "end thread receiving data on fd: %d\n", sockfd );
}

int main( int argc, char* argv[] )
{
    if( argc <= 2 )
    {
        printf( "usage: %s ip_address port_number\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );

    int ret = 0;
    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int listenfd = socket( PF_INET, SOCK_STREAM, 0 );
    assert( listenfd >= 0 );

    ret = bind( listenfd, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );

    ret = listen( listenfd, 5 );
    assert( ret != -1 );

    epoll_event events[ MAX_EVENT_NUMBER ];
    int epollfd = epoll_create( 5 );
    assert( epollfd != -1 );
    /* 注意, 监听socket listenfd上是不能注册EPOLLONESHOT事件的, 否则应用程序之恶能处理一个客户连接, 因为后续的客户连接请求不再触发listenfd上的EPOLLIN事件 */
    addfd( epollfd, listenfd, false );

    while( 1 )
    {
        int ret = epoll_wait( epollfd, events, MAX_EVENT_NUMBER, -1 );
        if ( ret < 0 )
        {
            printf( "epoll failure\n" );
            break;
        }
    
        for ( int i = 0; i < ret; i++ )
        {
            int sockfd = events[i].data.fd;
            if ( sockfd == listenfd )
            {
                struct sockaddr_in client_address;
                socklen_t client_addrlength = sizeof( client_address );
                int connfd = accept( listenfd, ( struct sockaddr* )&client_address, &client_addrlength );
                //对每个非监听文件描述符半丁EPOLLONSHOT事件
                addfd( epollfd, connfd, true );
            }
            else if ( events[i].events & EPOLLIN )
            {
                pthread_t thread;
                fds fds_for_new_worker;
                fds_for_new_worker.epollfd = epollfd;
                fds_for_new_worker.sockfd = sockfd;
                //新启动一个工作线程位sockfd服务
                pthread_create( &thread, NULL, worker, ( void* )&fds_for_new_worker );
            }
            else
            {
                printf( "something else happened \n" );
            }
        }
    }

    close( listenfd );
    return 0;
}
```

![image-20230206165202725](./charter09.assets/IO复用对比.png)

## 9.5非阻塞connect

```
EINPROGRESS
	The socket is nonblocking and the connection cannot be completed immediately.  It is possible to select(2) or poll(2) for completion by selecting the socket for writing.  After select(2) indicates writability, use getsockopt(2) to read the SO_ERROR option at  level  SOL_SOCKET  to  determine whether connect() completed successfully (SO_ERROR is zero) or unsuccessfully  (SO_ERROR  is one  of the usual error codes listed here, explaining the reason for the failure).
```

这段话描述了connect出错时的一种errno值: EINPROGRESS. 这种错误发生在对非阻塞的socket 调用 connect，而连接又没有立即建立时。根据man文档的解释，在这种情况下我们可以调用select、poll等函数来监听这个连接失败的socket上的可写事件。当select、poll等函数返回后，再利用getsockopt来读取错误码并清除该socket上的错误。如果错误码是0，表示连接成功建立，否则连接失败。

通过上面描述的非阻塞connect方式，我们就能同时发起多个连接并一起等待。下面看看非阻塞connect的一种实现（2），如代码清单9—5所示。



## 9.6聊天室程序

客户功能:

1. 从标准输入终端读入用户数据, 并将用户数据发送至服务器.
2. 往标准输出终端打印服务器发送给它的数据.

服务器功能:

1. 接收客户数据, 并把客户数据发送给每一个登录到服务器上的客户端(数据发送者除外).

```c
//client
#include <sys/socket.h>
#include <sys/types.h>
#include <cassert>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <poll.h>
#include <fcntl.h>

#define BUFFER_SIZE 64

int main(int argc, char* argv[]) {

	if(argc <= 2) {
		printf("Wrong number of parameters \n");
		return 1;
	}
	// 输入ip+port
	int port = atoi(argv[2]);
	char* ip = argv[1];
	
    // sockaddr   [af, ip,port]
	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_port = htons(port);
	inet_pton(AF_INET, ip, &address.sin_addr);

    // socket 申请
	int sockfd = socket(PF_INET, SOCK_STREAM, 0);
	assert(sockfd >= 0);

    // 发起连接
	if(connect(sockfd, (struct sockaddr*)&address, sizeof(address)) < 0) {
		printf("Connect error\n");
		close(sockfd);
		return 1;
	}
	// 注册文件描述符STDIN_FILENO和sockfd上的可读事件
	pollfd fds[2];
	fds[0].fd = 0;
	fds[0].events = POLLIN;
	fds[0].revents = 0;
	fds[1].fd = sockfd;
	fds[1].events = POLLIN | POLLRDHUP;
	fds[1].revents = 0;

    // 数据接收缓冲区
	char read_buf[BUFFER_SIZE];
	int pipefd[2];
	int ret = pipe(pipefd);
	assert(ret != -1);

	while (1) {

		ret = poll(fds, 2, -1);
		if(ret < 0) {
			printf("poll failure\n");
			break;
		}
        
		// sockfd断开连接
		if(fds[1].revents & POLLRDHUP) {
			printf("server close the connection\n");
			break;
		}
        
        // sockfd上有服务器数据
		if(fds[1].revents & POLLIN) {
			memset(read_buf, '\0', BUFFER_SIZE);
			recv(fds[1].fd, read_buf, BUFFER_SIZE - 1, 0);
			printf("%s\n", read_buf);
		}
		
        // 本进程的标准输入数据可读, 转发给服务器
        // POLLIN 文件描述符上数据可读
		if(fds[0].revents & POLLIN) {
            // splice 两个文件描述符必须有一个时管道文件描述符
            // STDIN_FILENO -> pipefd[1]	移动数据
			ret = splice(0, nullptr, pipefd[1], NULL, 32768, SPLICE_F_MORE | SPLICE_F_MOVE);
            
            //pipefd[0] -> sockfd	数据移动
			ret = splice(pipefd[0], nullptr, sockfd, NULL, 32768, SPLICE_F_MORE | SPLICE_F_MOVE);
		}
	}

	close(sockfd);
	return 0;
}
```

```c
// server
#include <sys/socket.h>
#include <sys/types.h>
#include <cassert>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <poll.h>
#include <fcntl.h>
#include <cerrno>

#define USER_LIMIT 5		// 最大用户数量
#define BUFFER_SIZE 64		// 读缓冲区的大小
#define FD_LIMIT 65535		// 文件描述符数量限制


struct client_data {
    sockaddr_in address;
    char* write_buff;			// 可写地址
    char buf[BUFFER_SIZE];		// 接收缓冲区总大小
};

int setnonblocking(int fd) {
    int old_option = fcntl(fd, F_GETFL);
    int new_option = old_option | O_NONBLOCK;
    fcntl(fd, F_SETFL, new_option);
    return old_option;
}

int main(int argc, char* argv[]) {

    if(argc <= 2) {
        printf("Wrong number of parameters \n");
        return 1;
    }
	// 接收输入IP+PORT
    int port = atoi(argv[2]);
    char* ip = argv[1];
	
    // sockaddr [IP + PORT]
    struct sockaddr_in address;
    memset(&address, 0, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    inet_pton(AF_INET, ip, &address.sin_addr);

    // 申请sockfd
    int sockfd = socket(PF_INET, SOCK_STREAM, 0);
    assert(sockfd >= 0);
    
    // sockfd命名
    int ret = bind(sockfd, (struct sockaddr*)&address, sizeof(address));
    assert(ret != -1);
	
    // 监听sockfd
    ret = listen(sockfd, 5);
    assert(ret != -1);


	// 创建users数组, 分配 FD_LIMIT个client_data对象, (接受缓冲区)
    // 这样每个socket连接都可以获得一个对象
	// 并且可以根据socket的值直接索引对应的对象 
    client_data* users = new client_data[FD_LIMIT];
    pollfd fds[USER_LIMIT + 1];
    int user_count = 0;
    for(int i = 0; i <= USER_LIMIT; i++) {
        fds[i].fd = -1;
        fds[i].events = 0;
    }
	
    // fds[0] 是服务器sockfd
    fds[0].fd = sockfd;
    fds[0].events = POLLIN | POLLERR;
    fds[0].revents = 0;

    while(1) {
		
        // poll 阻塞轮询
        ret = poll(fds, user_count + 1, -1);
        if(ret < 0) {
            printf("poll failure\n");
            break;
        }
		
        // 处理poll结果
        for(int i =0; i < user_count + 1; i++) {
            // 1. 有sockfd可读, 即有新用户
            if((fds[i].fd == sockfd) && (fds[i].revents & POLLIN)) {
                struct sockaddr_in client_address;
                socklen_t client_addrlength = sizeof(client_address);
                int connfd = accept(sockfd, (struct sockaddr *) &client_address, &client_addrlength);

                if (connfd < 0) {
                    printf("errno is: %d\n", errno);
                    continue;
                }

                if (user_count >= USER_LIMIT) {
                    const char *info = "to many users\n";
                    printf("%s", info);
                    send(connfd, info, strlen(info), 0);
                    close(connfd);
                    continue;
                }

                user_count++;
                users[connfd].address = client_address;
                setnonblocking(connfd);
                fds[user_count].fd = connfd;
                fds[user_count].events = POLLIN | POLLRDHUP | POLLERR;
                fds[user_count].revents = 0;
                printf("come a new client, now hava %d users\n", user_count);

            } 
            // 客户端 fd 发生错误
            else if(fds[i].revents & POLLERR) {
                printf("get an error from %d\n", fds[i].fd);
                char errors[100];
                memset(errors, '\0', 100);
                socklen_t length = sizeof(errors);
				
                // 接受错误信息
                if(getsockopt(fds[i].fd, SOL_SOCKET, SO_ERROR, &errors, &length) < 0) {
                    printf("get socketopt failed\n");
                }
                continue;
            }
            // 客户端断开连接
            else if(fds[i].revents & POLLRDHUP) {
                users[fds[i].fd] = users[fds[user_count].fd];
                close(fds[i].fd);
                fds[i] = fds[user_count];
                i--;
                user_count--;
                printf("a client left\n");
            } 
            // 客户有数据输入
            else if(fds[i].revents & POLLIN) {
				
                // 接受数据
                int connfd = fds[i].fd;
                memset(users[connfd].buf, '\0', BUFFER_SIZE);
                ret = recv(connfd, users[connfd].buf, BUFFER_SIZE - 1, 0);
                printf("get %d bytes of client data %s from %d\n", ret, users[connfd].buf, connfd);
				
                // 客户fd出错
                if(ret < 0) {

                    if(errno != EAGAIN) {
                        close(connfd);
                        users[fds[i].fd] = users[fds[user_count].fd];
                        fds[i] = fds[user_count];
                        i--;
                        user_count--;
                    }
                } 
                // 客户端无数据可读
                else if(ret == 0) {

                } 
                // 取数据并且重置poll监听事件
                else {
                    for(int j = 1; j <= user_count; j++) {
                        if(fds[j].fd == connfd) {
                            continue;
                        }
						
                        // 这里就要监听客户fds可写事件
                        fds[j].events |= ~POLLIN;
                        fds[j].events |= POLLOUT;
                        // 每次来新的连接, 需要将新连接的write_buff其实位置指定
                        users[fds[j].fd].write_buff = users[connfd].buf;
                    }
                }
            } 
            // 客户fd可写, 转发消息
            else if(fds[i].revents & POLLOUT) {
                int connfd = fds[i].fd;
                if(!users[connfd].write_buff) {
                    continue;
                }
                ret =send(connfd, users[connfd].write_buff, strlen(users[connfd].write_buff), 0);

				// 写完后重新注册fds[i]的可读事件(完成一个大循环) 
                users[connfd].write_buff = nullptr;
                fds[i].events |= ~POLLOUT;
                fds[i].events |= POLLIN;

            }
        }
    }

    delete[] users;
    close(sockfd);
    return 0;
}
```



## 9.7 同时处理TCP和UDP服务

从bind系统调用的参数来看，一个socket只能与一个socket地址绑定，即一个socket只能用来监听一个端口。因此，服务器如果要同时监听多个端口，就必须创建多个socket，并将它们分别绑定到各个端口上。这样一来，服务器程序就需要同时管理多个监听socket，I/O复用技术就有了用武之地。另外，即使是同一个端口，如果服务器要同时处理该端口上的TCP和UDP请求，则也需要创建两个不同的socket：一个是流 socket，另一个是数据报socket，并将它们都绑定到该端口上。比如代码清单9—8所示的回射服务器就能同时处理一个端口上的TCP和UDP请求。

```c
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <pthread.h>

#define MAX_EVENT_NUMBER 1024
#define TCP_BUFFER_SIZE 512
#define UDP_BUFFER_SIZE 1024

int setnonblocking( int fd )
{
    int old_option = fcntl( fd, F_GETFL );
    int new_option = old_option | O_NONBLOCK;
    fcntl( fd, F_SETFL, new_option );
    return old_option;
}

void addfd( int epollfd, int fd )
{
    epoll_event event;
    event.data.fd = fd;
    //event.events = EPOLLIN | EPOLLET;
    event.events = EPOLLIN;
    epoll_ctl( epollfd, EPOLL_CTL_ADD, fd, &event );
    setnonblocking( fd );
}

int main( int argc, char* argv[] )
{
    if( argc <= 2 )
    {
        printf( "usage: %s ip_address port_number\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );

    int ret = 0;
    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int listenfd = socket( PF_INET, SOCK_STREAM, 0 );
    assert( listenfd >= 0 );
	
    // tcpfd 命名
    ret = bind( listenfd, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );

    // tcpfd 监听
    ret = listen( listenfd, 5 );
    assert( ret != -1 );

    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );
    
    int udpfd = socket( PF_INET, SOCK_DGRAM, 0 );
    assert( udpfd >= 0 );
	
    // udpfd 命名
    ret = bind( udpfd, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );


    epoll_event events[ MAX_EVENT_NUMBER ];
    int epollfd = epoll_create( 5 );
    assert( epollfd != -1 );
    
    // 注册 udpfd+tcpfd 到epoll上
    addfd( epollfd, listenfd );
    addfd( epollfd, udpfd );

    while( 1 )
    {
        // 1. 查询
        int number = epoll_wait( epollfd, events, MAX_EVENT_NUMBER, -1 );
        if ( number < 0 )
        {
            printf( "epoll failure\n" );
            break;
        }
    	// 2. 处理
        for ( int i = 0; i < number; i++ )
        {
            int sockfd = events[i].data.fd;
            // 2.1 tcpfd 新连接
            if ( sockfd == listenfd )
            {
                struct sockaddr_in client_address;
                socklen_t client_addrlength = sizeof( client_address );
                int connfd = accept( listenfd, ( struct sockaddr* )&client_address, &client_addrlength );
                addfd( epollfd, connfd );
            }
            // 2.2 udpfd 新连接
            else if ( sockfd == udpfd )
            {
                char buf[ UDP_BUFFER_SIZE ];
                memset( buf, '\0', UDP_BUFFER_SIZE );
                struct sockaddr_in client_address;
                socklen_t client_addrlength = sizeof( client_address );

                ret = recvfrom( udpfd, buf, UDP_BUFFER_SIZE-1, 0, ( struct sockaddr* )&client_address, &client_addrlength );
                if( ret > 0 )
                {
                    sendto( udpfd, buf, UDP_BUFFER_SIZE-1, 0, ( struct sockaddr* )&client_address, client_addrlength );
                }
            }
            
            // 2.3.1 fd读事件
            else if ( events[i].events & EPOLLIN )
            {
                char buf[ TCP_BUFFER_SIZE ];
                while( 1 )
                {
                    memset( buf, '\0', TCP_BUFFER_SIZE );
                    ret = recv( sockfd, buf, TCP_BUFFER_SIZE-1, 0 );
                    if( ret < 0 )
                    {
                        if( ( errno == EAGAIN ) || ( errno == EWOULDBLOCK ) )
                        {
                            break;
                        }
                        close( sockfd );
                        break;
                    }
                    else if( ret == 0 )
                    {
                        close( sockfd );
                    }
                    else
                    {
                        send( sockfd, buf, ret, 0 );
                    }
                }
            }
            // 2.3.2 fd发生其他事件
            else
            {
                printf( "something else happened \n" );
            }
        }
    }

    close( listenfd );
    return 0;
}
```


# 第10章 信号

## 10.1 Linux信号概述

信号是用户, 系统或者进程发给目标进程的信息, 以通知目标进程某个状态的改变 或者 系统异常.

Linux信号可由如下条件产生:

1. 对于前台进程, 用户可以通过输入特殊字符, Ctrl+C.
2. 系统异常, 浮点数异常和非法内存段访问.
3. 系统状态变化, 比如alarm定时器到期引起SIGALRM.
4. kill命令或者kill函数调用.

先讨论如何发送和处理, 再讨论Linux支持的信号总类, 最后讨论和网络编程密切相关的信号

### 10.1.1发送信号

```c
#include <sys/types.h>
#include <signal.h>
int kill(pid_t pid, int sig);
/*
描述
	将sig发给特定的进程pid
参数
	pid
		pid>0	发给进程
		pid=0	发给本地进程组内其他进程
		pid=-1	发给除init进程外的所有进程, 但发送者需要有对目标进程发送信号的权限
		pid< -1	信号发送给组ID为-pid的进程组中的所有成员
	sig
		sig>0	发送信号
		sig=0	kill不发送任何信号, 但可以检测目标进程是否存在, 因为检查操作早于发送, 但也不可靠, 因为是非原子操作.
返回值
	成功0, 失败-1+errno
	EINVAL	无效信号
	EPERM	该进程无权限发信号给任何一个目标进程
	ESRCH	目标进程或进程组不存在
*/
```



### 10.1.2信号处理方式

```c
#include <signal.h>
typedef void (*__sighandler_t)(int sig);
/*
描述
	目标进程收到信号,需定义一个接收函数来处理, 会自动调用.
	处理函数要求可重入, 否则引发竞态条件, 所以信号中严禁调用不安全的函数
参数
	sig	接收信号类型

*/

//提供默认信号处理函数
//
#include <bit/signum.h>
#define SIG_DFL((__sighandler) 0);
/*
描述
	采用默认方式:
		结束进程(Term), 
		忽略信号(Ign), 
		结束+核心转储(Core), 
		暂停进程(Stop), 
		继续进程(Cont)

*/
#define SIG_IGN((__sighandler_t) 1);
/*
描述
	采用忽略信号

*/
```



### 10.1.3Linux信号

Linux信号都定义再`bit/signum.h`, 并不需要全会, 主要关注:`SIGHUP`,  `SIGPIPE`,`SIGURG`.后续添加`SIGALARM`, `SIGCHILD`.

![image-20230209145515294](./charter10.assets/signals.png)

### 10.1.4 中断系统调用

如果程序在执行处于阻塞状态的系统调用时接收信号, 并且我们为该信号设置了信号处理函数, 则默认情况下, 系统调用将被中断, 并且`errno`被设置为`EINTR`.我们可以使用sigaction函数, 为信号设置`SA_RESTART`标志以自动重启被该信号中断的系统调用.

对于默认行为是暂停进程的信号(如SIGSTOP SIGTTIN), 如果我们没有为他们设置信号处理函数, 则他们也可以中断某些系统调用(如connect epoll_wait). posix没有规定这种行为, 这是Linux独有的.

简而言之, 程序在执行系统调用处于阻塞状态时, 信号可进入使进程处于中断状态, 转而执行其他函数. 中断之后为信号设置`SA_RESTART`标志恢复现场.

## 10.2信号函数

### 10.2.1 signal系统调用

```c
#include <signal.h>
_sighandler_t signal(int sig, _sighander_t handler);
/*
描述
	为本进程sig信号设置处理函数.
参数
	sig 要捕获的信号
	handler 处理函数
返回值
	成功返回前一次调用signal函数使传入的函数指针, 或者是信号sig默认处理函数指针SIG_DEF
	失败返回 SIG_ERR 并设置errno
*/
```



### 10.2.2sigaction系统调用

```c
#include <signal.h>
int sigaction(int sig, const struct sigaction* act, struct* sigaction oact);
/*
描述
	比signal函数更健壮的接口
参数
	sig 要捕获的信号
	act 新的信号处理方式
	oact 旧的信号处理方式 
返回值
	成功0
	失败-1+errno
*/

struct sigation
{
#ifdef __USE_POSIX199309    
    union
    {
        _sigaction_t sa_handler;
        void (*sa_sigaction) (int, siginfo_t*, void*);
    }
    _sigaction_handler;
#define sa_handler		__sigaction_handler.sa_handler
#define sa_sigaction	__sigaction_handler.sa_sigaction
#else
    _sighandler_t sa_handler;
#endif
    _sigset_t sa_mask;
    int sa_flags;
    void (*sa_restorer) (void);    
};
/*
sa_handler
	该成员指定信号处理函数
sa_mask
	该成员设置进程信号掩码, 进程原有信号掩码基础上增加新的信号掩码
	sa_mask是信号集_sigset_t的同义词, 同类型
sa_flags
	该指定程序收到信号的行为

sa_restorer
	该成员已过时, 最好不要使用
*/
```

![image-20230209152451659](./charter10.assets/sig_flags.png)

## 10.3信号集

### 10.3.1信号集函数

前文提到, Linux使用数据结构sigset_t来表示一组信号

```c
#include <bits/sigset.h>
#define _SIGSET_NWORDS (1024/(8*sizeof(unsigned long int)))
typedef struct
{
	unsigned long int __val[_SIGSET_NWORDS];
}__sigset_t;
/*
描述
	可知sigset_t为长整型数组, 每个元素的每个位表示一个信号
	这种定义方式和文件描述符fd_set类似, Linux提供了如下一组函数来设置, 修改, 删除和查询信号集
*/
#include <signal.h>
int sigemptyset(sigset_t* set);		//清空信号集
int sigfillset(sigset_t* set);		//在信号集设置所有信号, 监听所有信号
int sigaddset(sigset_t* set, int _signo);//将信号_signo添加至信号集中
int sigdelset(sigset_t* set, int _signo);//将信号_signo从信号集删除
int sigismember(sigset_t* set, _signo);	//测试_signo是否在信号集中
i
```



### 10.3.2进程信号掩码

可以利用sigaction结构体中sa_mask成员设置进程信号掩码.

```c
#include <signal.h>
int sigprocmask(int _how, const sigset_t* _set. sigset_t* _oset);
/*
描述
	设置进程信号掩码.
参数
	_set参数指定新的信号掩码
	_oset返回原来的信号掩码, 如果不为NULL
	_how 控制_set信号掩码方式, 如果_set非空
返回值
	成功0, 失败-1+errno
*/
```

![image-20230209154042597](./charter10.assets/_how.png)

### 10.3.3被挂起的信号

设置进程信号掩码后，被屏蔽的信号将不能被进程接收。如果给进程发送一个被屏蔽的信号，则操作系统将该信号设置为进程的一个被挂起的信号。如果我们取消对被挂起信号的屏蔽，则它能立即被进程接收到。

```c
#include <signal.h>

int sigpending(sigset_t* set);
/*
描述
	获得进程当前被挂起的信号集.
参数
	set 保存被挂起的信号集, 进程多次接收同一个被挂起信号, sigpending函数只能反映一次, 并且当我们再次使用sigprocmask使能该挂起信号时, 该信号的处理函数也只能被触发一次
返回值
	成功 0 , 失败-1+errno
*/
```

关于信号和信号集, 关键点在: 始终清楚进程每个运行时刻的信号掩码, 以及如何适当处理捕获到的信号.

在多进程和多线程环境中, 我们要以进程, 线程为单位来处理信号和信号掩码. 不能设想 新创建的进程和线程 具有同 父进程和主线程 完全相同的信号特征. 比如fork调用产生的子进程将继承父进程的信号掩码, 但是具有一个空的挂起信号集.



简单来说, 设置sigprocmask之后, 进程还是能接收信号, 信号多次触发只会接收一次, 存在sigpending中, 子进程和父进程只继承信号掩码集合(sigprocmask)而不包含挂起信号集合(sigpending).

## 10.4统一事件源

信号是一种异步事件：信号处理函数和程序的主循环是两条不同的执行路线。很显然，信号处理函数需要尽可能快地执行完毕，以确保该信号不被屏蔽（前面提到过，为了避免一些竞态条件，信号在处理期间，系统不会再次触发它）太久。

一种典型的解决方案是：

1. 把信号的主要处理逻辑放到程序的主循环中，当信号处理函数被触发时，它只是简单地通知主循环程序接收到信号，并把信号值传递给主循环，主循环再根据接收到的信号值执行目标信号对应的逻辑代码。
2. 信号处理函数通常使用管道来将信号“传递”给主循环：信号处理函数往管道的写端写入信号值，主循环则从管道的读端读出该信号值。那么主循环怎么知道管道上何时有数据可读呢？这很简单，我们只需要使用I/O复用系统调用来监听管道的读端文件描述符上的可读事件。如此一来，信号事件就能和其他I/O事件一样被处理，即统一事件源。

简而言之, 将信号作为事件交由主循环, 主循环触发, 交给其他处理, 方法就是使用管道充当信号, 主循环(epoll_wait)监听管道可读与否.

```c
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <pthread.h>

#define MAX_EVENT_NUMBER 1024
static int pipefd[2];

int setnonblocking( int fd )
{
    int old_option = fcntl( fd, F_GETFL );
    int new_option = old_option | O_NONBLOCK;
    fcntl( fd, F_SETFL, new_option );
    return old_option;
}

void addfd( int epollfd, int fd )
{
    epoll_event event;
    event.data.fd = fd;
    // 只监听可读事件和水平触发
    event.events = EPOLLIN | EPOLLET;
    epoll_ctl( epollfd, EPOLL_CTL_ADD, fd, &event );
    setnonblocking( fd );
}

// 信号处理函数
void sig_handler( int sig )
{
    // 保留原来的errno, 在函数最后恢复, 以保证函数的可重入性
    int save_errno = errno;
    int msg = sig;
    // 将信号写入管道, 以通知主循环
    send( pipefd[1], ( char* )&msg, 1, 0 );
    errno = save_errno;
}

// 设置信号处理函数
void addsig( int sig )
{
    struct sigaction sa;
    memset( &sa, '\0', sizeof( sa ) );
    sa.sa_handler = sig_handler;
    sa.sa_flags |= SA_RESTART;
    sigfillset( &sa.sa_mask );
    assert( sigaction( sig, &sa, NULL ) != -1 );
}

int main( int argc, char* argv[] )
{
    if( argc <= 2 )
    {
        printf( "usage: %s ip_address port_number\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );

    int ret = 0;
    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int listenfd = socket( PF_INET, SOCK_STREAM, 0 );
    assert( listenfd >= 0 );


    ret = bind( listenfd, ( struct sockaddr* )&address, sizeof( address ) );
    if( ret == -1 )
    {
        printf( "errno is %d\n", errno );
        return 1;
    }
    //assert( ret != -1 );

    ret = listen( listenfd, 5 );
    assert( ret != -1 );

    epoll_event events[ MAX_EVENT_NUMBER ];
    int epollfd = epoll_create( 5 );
    assert( epollfd != -1 );
    addfd( epollfd, listenfd );

    // 使用sockpair创建管道
    ret = socketpair( PF_UNIX, SOCK_STREAM, 0, pipefd );
    assert( ret != -1 );
    setnonblocking( pipefd[1] );
    // 注册管道 addfd函数只监听可读事件
    addfd( epollfd, pipefd[0] );

    // 设置信号的处理函数
    addsig( SIGHUP );
    addsig( SIGCHLD );
    addsig( SIGTERM );
    addsig( SIGINT );
    bool stop_server = false;

    while( !stop_server )
    {
        int number = epoll_wait( epollfd, events, MAX_EVENT_NUMBER, -1 );
        if ( ( number < 0 ) && ( errno != EINTR ) )
        {
            printf( "epoll failure\n" );
            break;
        }
    
        for ( int i = 0; i < number; i++ )
        {
            int sockfd = events[i].data.fd;
            // 新连接
            if( sockfd == listenfd )
            {
                struct sockaddr_in client_address;
                socklen_t client_addrlength = sizeof( client_address );
                int connfd = accept( listenfd, ( struct sockaddr* )&client_address, &client_addrlength );
                addfd( epollfd, connfd );
            }
            // 触发信号
            else if( ( sockfd == pipefd[0] ) && ( events[i].events & EPOLLIN ) )
            {
                int sig;
                char signals[1024];
                ret = recv( pipefd[0], signals, sizeof( signals ), 0 );
                if( ret == -1 )
                {
                    continue;
                }
                else if( ret == 0 )
                {
                    continue;
                }
                else
                {
                    for( int i = 0; i < ret; ++i )
                    {
                        //printf( "I caugh the signal %d\n", signals[i] );
                        switch( signals[i] )
                        {
                            case SIGCHLD:
                            case SIGHUP:
                            {
                                continue;
                            }
                            case SIGTERM:
                            case SIGINT:
                            {
                                stop_server = true;
                            }
                        }
                    }
                }
            }
            else
            {
            }
        }
    }

    printf( "close fds\n" );
    close( listenfd );
    close( pipefd[1] );
    close( pipefd[0] );
    return 0;
}
```



## 10.5网络编程相关信号

本节探讨三个和网络编程密切相关的信号.

### 10.5.1 sighup

当挂起进程的控制终端(控制IO操作, 一般一个会话有)时, SIGHUP将被触发, 如果没有控制终端(服务器), 通常会利用SIGHUP信号强制服务器重读配置文件.

### 10.5.2 sigpipe

默认情况下, 往一个读端关闭的管道或者socket连接中写入数据, 将引发`SIGPIPE`信号.我们需要在代码中捕获并处理该信号, 或者至少忽略它, 因为进程接收到`SIGPIPE`信号的默认行为是结束进程, 而我们绝不能因为错误的写操作而导致进程关闭.引起`SIGPIPE`信号的写操作将`errno`设置为`EPIPE`.

第五章我们提到, 可以用send函数的`MSG_NOSIGNAL`标志来禁止写操作触发`SIGPIPE`信号, 在这种情况下, 我们应该使用send函数来反馈errno值来判断管道或者socket连接的读端是否以及关闭.

此外, 也可用利用IO复用系统调用来检测管道和socket连接的读端是否已经关闭, 以poll为例, 当管道的读端关闭时, 写端文件描述上的`POLLHUP`事件将被触发; 当socket连接被对方关闭时, socket上的`POLLRDHUP`事件将被触发.

### 10.5.3 sigurg

Linux环境下, 内核通知应用程序带外数据到达的主要方式有两种: 一种是IO复用技术, select等系统调用在接收到带外数据时将返回, 并向进程报告socket上的异常事件(9-1代码).另一个就是利用SIGURG信号(10-3代码)

3.8 TCP带外数据引入.

5.3.1 带外标记, 如何检测有带外数据到来, `sockatmark`系统调用.

5.8.1 send/recv中使用MSG_OOB标志接收/发送带外数据

9.1.3 IO复用系统调用报告的异常事件, 检测带外数据到达

10.5.3 SIGURG, 检测带外数据到达

5.9 sockatmark系统调用解决, 即判断socket是否处于带外标记, 即socket上下一个被读取到的数据是否携带带外数据.

```c
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>

#define BUF_SIZE 1024

static int connfd;

// SIGURG信号的处理函数
void sig_urg( int sig )
{
    int save_errno = errno;
    
    char buffer[ BUF_SIZE ];
    memset( buffer, '\0', BUF_SIZE );
    int ret = recv( connfd, buffer, BUF_SIZE-1, MSG_OOB );
    printf( "got %d bytes of oob data '%s'\n", ret, buffer );

    errno = save_errno;
}

// 添加信号处理函数
void addsig( int sig, void ( *sig_handler )( int ) )
{
    struct sigaction sa;
    memset( &sa, '\0', sizeof( sa ) );
    sa.sa_handler = sig_handler;
    sa.sa_flags |= SA_RESTART;
    sigfillset( &sa.sa_mask );
    assert( sigaction( sig, &sa, NULL ) != -1 );
}

int main( int argc, char* argv[] )
{
    if( argc <= 2 )
    {
        printf( "usage: %s ip_address port_number\n", basename( argv[0] ) );
        return 1;
    }
    const char* ip = argv[1];
    int port = atoi( argv[2] );

    struct sockaddr_in address;
    bzero( &address, sizeof( address ) );
    address.sin_family = AF_INET;
    inet_pton( AF_INET, ip, &address.sin_addr );
    address.sin_port = htons( port );

    int sock = socket( PF_INET, SOCK_STREAM, 0 );
    assert( sock >= 0 );

    int ret = bind( sock, ( struct sockaddr* )&address, sizeof( address ) );
    assert( ret != -1 );

    ret = listen( sock, 5 );
    assert( ret != -1 );

    struct sockaddr_in client;
    socklen_t client_addrlength = sizeof( client );
    connfd = accept( sock, ( struct sockaddr* )&client, &client_addrlength );
    if ( connfd < 0 )
    {
        printf( "errno is: %d\n", errno );
    }
    else
    {
        addsig( SIGURG, sig_urg );
        // 使用SIGURG信号之前, 我们必须设置socket的宿主主进程或者进程组
        fcntl( connfd, F_SETOWN, getpid() );

        char buffer[ BUF_SIZE ];
        // 循环接收普通数据
        while( 1 )
        {
            memset( buffer, '\0', BUF_SIZE );
            ret = recv( connfd, buffer, BUF_SIZE-1, 0 );
            if( ret <= 0 )
            {
                break;
            }
            printf( "got %d bytes of normal data '%s'\n", ret, buffer );
        }

        close( connfd );
    }

    close( sock );
    return 0;
}
```

